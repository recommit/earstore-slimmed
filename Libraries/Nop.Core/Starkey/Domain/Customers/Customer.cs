﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customers
{
    public partial class Customer : BaseEntity
    {        
        public string SocialNumber { get; set; }
        public string CustomerNumber { get; set; }
        public string DefaultAdress { get; set; } //In Earstore, a customer has a specific adress chosen as a default adress, instead of having a equal 1 -> N adress relationship as in original NC.
        public Boolean HasExpiredInvoices { get; set; }
        public Boolean AcceptNewsletters { get; set; } //This flag has completely overridden the built in system for newsletter.
        //Instead to determine which customers should receive letters, only this flag is checked,
        //not actual subscription-class objects, which are no longer used at all.        
    }
}
