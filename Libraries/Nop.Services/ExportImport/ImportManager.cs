using System;
using System.IO;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Media;
using Nop.Services.Seo;
using OfficeOpenXml;
using Nop.Core.Domain.Customers;
using Nop.Services.Customers;
using Nop.Core.Domain.Common;
using Nop.Services.Common;
using System.Collections.Generic;
using Nop.Services.Messages;
using Nop.Core.Domain.Messages;
using NLog;
using NLog.Config;
using NLog.Targets;
using Nop.Services.Logging;
using System.Text;
using System.Diagnostics;


namespace Nop.Services.ExportImport
{
    /// <summary>
    /// Import manager
    /// </summary>
    public partial class ImportManager : IImportManager
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IPictureService _pictureService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ICustomerService _customerService;
        private readonly IAddressService _addressService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly CustomerSettings _customerSettings;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly ILogger _logger;

        //public NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public ImportManager(ILogger logger, IWorkContext workContext, IProductService productService, ICategoryService categoryService,
            IManufacturerService manufacturerService, IPictureService pictureService,
            IUrlRecordService urlRecordService, ICustomerService customerService,IAddressService addressService,
            IGenericAttributeService genericAttributeService, CustomerSettings customerSettings, INewsLetterSubscriptionService newsLetterSubscriptionService)
        {
            this._productService = productService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._pictureService = pictureService;
            this._urlRecordService = urlRecordService;
            this._customerService = customerService;
            this._addressService = addressService;
            this._genericAttributeService = genericAttributeService;
            this._customerSettings = customerSettings;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._logger = logger;
        }

        protected virtual int GetColumnIndex(string[] properties, string columnName)
        {
            if (properties == null)
                throw new ArgumentNullException("properties");

            if (columnName == null)
                throw new ArgumentNullException("columnName");

            for (int i = 0; i < properties.Length; i++)
                if (properties[i].Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return i + 1; //excel indexes start from 1
            return 0;
        }

        public static DateTime FromUnix(string stringUnixTimestamp)
        {
            double unixTimeStamp = double.Parse(stringUnixTimestamp);
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }


        /// <summary>
        /// Import customers from XLSX file
        /// </summary>
        /// <param name="stream">Stream</param>
        public virtual void ImportAllCustomersEarstore(Stream stream)
        {
            using (var xlPackage = new ExcelPackage(stream))
            {
                // get the first worksheet in the workbook
                var worksheet = xlPackage.Workbook.Worksheets.FirstOrDefault();
                if (worksheet == null)
                    throw new NopException("No worksheet found");

                //the columns
                var properties = new string[]
                {
                    "CustomerNumber",
                    "FirstName",
                    "LastName",
                    "Adress1",
                    "Adress2",
                    "PostalCode",
                    "City",
                    "Email",
                    "Phone",
                    "DeliveryAdress1",
                    "DeliveryAdress2",
                    "DeliveryPostalCode",
                    "DeliveryCity",
                    "DeliveryName",
                    "CreatedDate",
                    "LastLogin",
                    "Password",
                    "Hash",
                    "PasswordTimestamp",
                    "Guid",
                    "Marketing",
                    "SocialNumber",
                    "Blacklisted",
                    "UnpaidOrders",
                    "Premium",
                };

                bool only1time = true;
                int iRow = 1;
                int endRow = iRow + 150;
                int nrOfImportedCustomers = 0;

                //----------Initiate variables instead of new ones in loop, for memory management--------

                Customer cust2 = null;
                Customer customer = null;
                string modifiedSocialNumber = "";
                Address defaultAddress = null;
                Customer customerWithAdressId = null;
                String firstName = "";
                String lastName = "";
                Address SecondaryAdress = null;
                string[] array = null;
                int iz = 0;
                int length = 0;


                    //--in above--


                String addCustomerStatus = "Okay";
                DateTime timeStampNow = DateTime.UtcNow;
                string FirstName = "";
                string LastName = "";
                string Adress1 = "";
                string Adress2 = "";
                string PostalCode = "";
                string City = "";
                string Email = "";
                string Phone = "";
                string DeliveryAdress1 = "";
                string DeliveryAdress2 = "";
                string DeliveryPostalCode = "";
                string DeliveryCity = "";
                string DeliveryName = "";
                string CreatedDate = "";
                string LastLogin = "";
                string Marketing = "";
                string SocialNumber = "";
                string Blacklisted = "";
                string UnpaidOrders = "";
                string Premium = "";
                int CustomerNumber = 0;

                //---No need to reset---
                DateTime gregorianCreated = new DateTime();
                DateTime gregorianLastActivity = new DateTime();
                string sPattern = "^((\\d{6})[-]?\\d{4})$";
                var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
                var newCustomerRoles = new List<CustomerRole>();
                //---No need to reset---
                              

                //----------Initiate variables instead of new ones in loop, for memory management--------

                Stopwatch totalTime = new Stopwatch();
                totalTime.Start();
                _logger.Debug("Customer Import started.");

                while (true)
                {
                    try
                    {
                        Stopwatch sw0 = new Stopwatch();
                        Stopwatch sw1 = new Stopwatch();
                        Stopwatch sw2 = new Stopwatch();
                        Stopwatch sw3 = new Stopwatch();
                        Stopwatch sw4 = new Stopwatch();
                        Stopwatch sw5 = new Stopwatch();
                        Stopwatch sw6 = new Stopwatch();
                        Stopwatch sw7 = new Stopwatch();
                        Stopwatch sw8 = new Stopwatch();
                       
                        //------------------<<<<<<<<<<<<<< sw 1 start >>>>>>>>>>>>>>>>------------------------                       

                        sw0.Start(); 
                        bool allColumnsAreEmpty = true;
                        for (var i = 1; i <= properties.Length; i++)
                            if (worksheet.Cells[iRow, i].Value != null && !String.IsNullOrEmpty(worksheet.Cells[iRow, i].Value.ToString()))
                            {
                                allColumnsAreEmpty = false;
                                break;
                            }

                        if (allColumnsAreEmpty)
                            break;

                        sw0.Stop(); 
                        sw1.Start(); 

                        //-------reset some variables, null, "" etc are needed for logic-------------
                         cust2 = null;
                         customer = null;
                         modifiedSocialNumber = "";
                         defaultAddress = null;
                         customerWithAdressId = null;
                         firstName = "";
                         lastName = "";
                         SecondaryAdress = null;
                         array = null;
                         iz = 0;
                         length = 0;

                         //--in above--

                         addCustomerStatus = "Okay";
                         timeStampNow = DateTime.UtcNow;                    
                         FirstName = "";
                         LastName = "";
                         Adress1 = "";
                         Adress2 = "";
                         PostalCode = "";
                         City = "";
                         Email = "";
                         Phone = "";
                         DeliveryAdress1 = "";
                         DeliveryAdress2 = "";
                         DeliveryPostalCode = "";
                         DeliveryCity = "";
                         DeliveryName = "";
                         CreatedDate = "";
                         LastLogin = "";
                         Marketing = "";
                         SocialNumber = "";
                         Blacklisted = "";
                         UnpaidOrders = "";
                         Premium = "";
                         CustomerNumber = 0;

                         //-------reset some variables, null, "" etc are needed for logic-------------

                    try
                    {
                        try
                        {
                            //CustomerNumber = Encoding.Convert(Encoding.A,
                            CustomerNumber = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "CustomerNumber")].Value);
                            if (CustomerNumber == null) { CustomerNumber = (Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "CustomerNumber")].Value as String)); }
                        }
                        catch (Exception e) { addCustomerStatus = "Invalid CustomerNumber";  }
                        FirstName = worksheet.Cells[iRow, GetColumnIndex(properties, "FirstName")].Value as string;
                        if (FirstName == null) { FirstName = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "FirstName")].Value)).ToString(); }

                        LastName = worksheet.Cells[iRow, GetColumnIndex(properties, "LastName")].Value as string;
                        if (LastName == null) { LastName = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "LastName")].Value)) + ""; }

                        Adress1 = worksheet.Cells[iRow, GetColumnIndex(properties, "Adress1")].Value as string;
                        if (Adress1 == null) { Adress1 = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "Adress1")].Value)) + ""; }

                        Adress2 = worksheet.Cells[iRow, GetColumnIndex(properties, "Adress2")].Value as string;
                        if (Adress2 == null) { Adress2 = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "Adress2")].Value)) + ""; }

                        PostalCode = worksheet.Cells[iRow, GetColumnIndex(properties, "PostalCode")].Value as string;
                        if (PostalCode == null) { PostalCode = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "PostalCode")].Value)) + ""; }

                        City = worksheet.Cells[iRow, GetColumnIndex(properties, "City")].Value as string;
                        if (City == null) { City = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "City")].Value)) + ""; }

                        Email = worksheet.Cells[iRow, GetColumnIndex(properties, "Email")].Value as string;
                        if (Email == null) { Email = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "Email")].Value)) + ""; }

                        Phone = worksheet.Cells[iRow, GetColumnIndex(properties, "Phone")].Value as string;
                        if (Phone == null) { Phone = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "Phone")].Value)) + ""; }

                        DeliveryAdress1 = worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryAdress1")].Value as string;
                        if (DeliveryAdress1 == null) { DeliveryAdress1 = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryAdress1")].Value)) + ""; }

                        DeliveryAdress2 = worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryAdress2")].Value as string;
                        if (DeliveryAdress2 == null) { DeliveryAdress2 = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryAdress2")].Value)) + ""; }

                        DeliveryPostalCode = worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryPostalCode")].Value as string;
                        if (DeliveryPostalCode == null) { DeliveryPostalCode = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryPostalCode")].Value)) + ""; }

                        DeliveryCity = worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryCity")].Value as string;
                        if (DeliveryCity == null) { DeliveryCity = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryCity")].Value)) + ""; }

                        DeliveryName = worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryName")].Value as string;
                        if (DeliveryName == null) { DeliveryName = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "DeliveryName")].Value)) + ""; }

                        CreatedDate = worksheet.Cells[iRow, GetColumnIndex(properties, "CreatedDate")].Value as string;
                        if (CreatedDate == null) { CreatedDate = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "CreatedDate")].Value)) + ""; }

                        LastLogin = worksheet.Cells[iRow, GetColumnIndex(properties, "LastLogin")].Value as string;
                        if (LastLogin == null) { LastLogin = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "LastLogin")].Value)) + ""; }

                        // string Password = worksheet.Cells[iRow, GetColumnIndex(properties, "Password")].Value as string;//Anv�nds ej
                        // string Hash = worksheet.Cells[iRow, GetColumnIndex(properties, "Hash")].Value as string;//Anv�nds ej
                        // string PasswordTimestamp = worksheet.Cells[iRow, GetColumnIndex(properties, "PasswordTimestamp")].Value as string;//Anv�nds ej
                        //string Guid = worksheet.Cells[iRow, GetColumnIndex(properties, "Guid")].Value as string;//Generera ny?

                        Marketing = worksheet.Cells[iRow, GetColumnIndex(properties, "Marketing")].Value as string;
                        if (Marketing == null) { Marketing = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "Marketing")].Value)) + ""; }

                        SocialNumber = worksheet.Cells[iRow, GetColumnIndex(properties, "SocialNumber")].Value as string;
                        if (SocialNumber == null) { SocialNumber = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "SocialNumber")].Value)) + ""; }

                        Blacklisted = worksheet.Cells[iRow, GetColumnIndex(properties, "Blacklisted")].Value as string;
                        if (Blacklisted == null) { Blacklisted = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "Blacklisted")].Value)) + ""; }

                        UnpaidOrders = worksheet.Cells[iRow, GetColumnIndex(properties, "UnpaidOrders")].Value as string;
                        if (UnpaidOrders == null) { UnpaidOrders = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "UnpaidOrders")].Value)) + ""; }

                        Premium = worksheet.Cells[iRow, GetColumnIndex(properties, "Premium")].Value as string; //Nullhantering
                        if (Premium == null) { Premium = (Convert.ToInt64(worksheet.Cells[iRow, GetColumnIndex(properties, "Premium")].Value)) + ""; }
                    }
                    catch (Exception e) {
                        addCustomerStatus = "General exception reading Customer Data";
                        _logger.Debug("Row: " + iRow + " CustomerNumber: " + CustomerNumber + " Error parsing import file: " + e.Message + ", " + e.InnerException);
                    }



                    //Set all different versions of missing, unset or 0 to NULL
                    if (FirstName == "0" || FirstName == "" || FirstName == "NULL") { FirstName = null; }
                    if (LastName == "0" || LastName == "" || LastName == "NULL") { LastName = null; }
                    if (Adress1 == "0" || Adress1 == "" || Adress1 == "NULL") { Adress1 = null; }
                    if (Adress2 == "0" || Adress2 == "" || Adress2 == "NULL") { Adress2 = null; }
                    if (PostalCode == "0" || PostalCode == "" || PostalCode == "NULL") { PostalCode = null; }
                    if (City == "0" || City == "" || City == "NULL") { City = null; }
                    if (Email == "0" || Email == "" || Email == "NULL") { Email = null; }
                    if (Phone == "0" || Phone == "" || Phone == "NULL") { Phone = null; }
                    if (DeliveryAdress1 == "0" || DeliveryAdress1 == "" || DeliveryAdress1 == "NULL") { DeliveryAdress1 = null; }
                    if (DeliveryAdress2 == "0" || DeliveryAdress2 == "" || DeliveryAdress2 == "NULL") { DeliveryAdress2 = null; }
                    if (DeliveryPostalCode == "0" || DeliveryPostalCode == "" || DeliveryPostalCode == "NULL") { DeliveryPostalCode = null; }
                    if (DeliveryCity == "0" || DeliveryCity == "" || DeliveryCity == "NULL") { DeliveryCity = null; }
                    if (DeliveryName == "0" || DeliveryName == "" || DeliveryName == "NULL") { DeliveryName = null; }
                    if (CreatedDate == "0" || CreatedDate == "" || CreatedDate == "NULL") { CreatedDate = null; }
                    if (LastLogin == "0" || LastLogin == "" || LastLogin == "NULL") { LastLogin = null; }
                    if (SocialNumber == "0" || SocialNumber == "" || SocialNumber == "NULL") { SocialNumber = null; }
                    

                    //--------------------------Errorhandling Importdata--------------------------- 

                    if (CustomerNumber == 0 || CustomerNumber == null)
                    {
                        _logger.Debug("Invalid CustomerNumber, Row: " + iRow);
                        addCustomerStatus = "Invalid CustomerNumber"; 
                    }

                    try
                    {
                        if (!String.IsNullOrWhiteSpace(Email))
                        {
                            if (!CommonHelper.IsValidEmail(Email))
                            {
                                Email = CustomerNumber + "felaktigt.format@tabort.com";
                            }
                            else
                            { //If a double email has occured, add unique identifier and "ImpDublett" to flag for manual handling after import
                                cust2 = _customerService.GetCustomerByEmail(Email);
                                if (cust2 != null)
                                    Email = "ImpDublett" + "["+CustomerNumber+"][" +Email+"]";
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.Debug("Exception parsing Email, Row: " + iRow + " : "+e.Message);
                    }
                    //--------------------------Errorhandling Importdata---------------------------                   

                    if (addCustomerStatus.Equals("Okay") == false) {

                        _logger.Debug("Aborting import, Row: " + iRow + "Status: " + addCustomerStatus);
                    }
                    else
                    {
                        customer = new Customer()
                       {
                           CustomerGuid = System.Guid.NewGuid(),
                           Email = Email,
                           Username = Email,
                           VendorId = 0,
                           AdminComment = null,
                           IsTaxExempt = false,
                           Active = true,
                           CustomerNumber = CustomerNumber + ""
                       };

                        customer.CreatedOnUtc = timeStampNow;
                        customer.LastActivityDateUtc = timeStampNow;

                        gregorianCreated = new DateTime();
                        gregorianLastActivity = new DateTime();

                        try
                        {
                            if (CreatedDate != null)
                            {
                                gregorianCreated = FromUnix(CreatedDate);
                                customer.CreatedOnUtc = gregorianCreated;
                            }
                            if (LastLogin != null)
                            {
                                gregorianLastActivity = FromUnix(LastLogin);
                                customer.LastActivityDateUtc = gregorianLastActivity;
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Debug("Invalid DateTimes, defaulting to current DateTime, Row: " + iRow);
                            customer.CreatedOnUtc = timeStampNow;
                            customer.LastActivityDateUtc = timeStampNow;
                        
                        }

                        //-------------------<<<<<<<<<<<<<< sw 1 stop >>>>>>>>>>>>>>>>------------------------
                        sw1.Stop();

                        //------------------<<<<<<<<<<<<<< sw 2 start >>>>>>>>>>>>>>>>------------------------                       
                        sw2.Start();
                        //-------------------<<<<<<<<<<<<<< sw 2 stop >>>>>>>>>>>>>>>>------------------------
                        _customerService.InsertCustomer(customer);
                        sw2.Stop();

                        //------------------<<<<<<<<<<<<<< sw 3 start >>>>>>>>>>>>>>>>------------------------                       
                        sw3.Start();                        

                        if (SocialNumber != null)
                        {                            
                            if (System.Text.RegularExpressions.Regex.IsMatch(SocialNumber, sPattern))
                            {
                                customer.SocialNumber = SocialNumber;
                            }
                            else
                            {
                                modifiedSocialNumber = "";
                                modifiedSocialNumber = SocialNumber.Substring(2); //If validate failed first time, it may help to shorten the number with the optional two first letters YYYYMMDD-XXXX -> YYMMDD-XXXX
                                if (System.Text.RegularExpressions.Regex.IsMatch(modifiedSocialNumber, sPattern))
                                {
                                    customer.SocialNumber = modifiedSocialNumber;
                                }
                            }
                        }                      

                        if (UnpaidOrders == "1")
                        {
                            customer.HasExpiredInvoices = true;
                        }
                        if (Marketing == "1")
                        {
                            customer.AcceptNewsletters = true;   
                        }
                        //_customerService.UpdateCustomer(customer);
                        //-------------------<<<<<<<<<<<<<< sw 3 stop >>>>>>>>>>>>>>>>------------------------
                        sw3.Stop();
                        //------------------<<<<<<<<<<<<<< sw 4 start >>>>>>>>>>>>>>>>------------------------                       
                        sw4.Start();  
                        _genericAttributeService.SaveAttributeOptimized(customer, SystemCustomerAttributeNames.FirstName, FirstName);
                        _genericAttributeService.SaveAttributeOptimized(customer, SystemCustomerAttributeNames.LastName, LastName);

                        if (_customerSettings.StreetAddressEnabled)
                            _genericAttributeService.SaveAttributeOptimized(customer, SystemCustomerAttributeNames.StreetAddress, Adress1);
                        if (_customerSettings.StreetAddress2Enabled)
                            _genericAttributeService.SaveAttributeOptimized(customer, SystemCustomerAttributeNames.StreetAddress2, Adress2);
                        if (_customerSettings.ZipPostalCodeEnabled)
                            _genericAttributeService.SaveAttributeOptimized(customer, SystemCustomerAttributeNames.ZipPostalCode, PostalCode);
                        if (_customerSettings.CityEnabled)
                            _genericAttributeService.SaveAttributeOptimized(customer, SystemCustomerAttributeNames.City, City);
                        if (_customerSettings.CountryEnabled)
                            _genericAttributeService.SaveAttributeOptimized(customer, SystemCustomerAttributeNames.CountryId, 73);   //-------Hardcode sweden default            

                        if (_customerSettings.PhoneEnabled)
                            _genericAttributeService.SaveAttributeOptimized(customer, SystemCustomerAttributeNames.Phone, Phone);
                         //-------------------<<<<<<<<<<<<<< sw 4 stop >>>>>>>>>>>>>>>>------------------------
                        sw4.Stop();
                        //------------------<<<<<<<<<<<<<< sw 5 start >>>>>>>>>>>>>>>>------------------------                       
                        sw5.Start();
                        foreach (var customerRole in allCustomerRoles)
                            if (customerRole.SystemName == SystemCustomerRoleNames.Registered || (customerRole.SystemName == "Premium" && Premium =="1"))
                            {
                                customer.CustomerRoles.Add(customerRole);
                            }


                        //_customerService.UpdateCustomer(customer);


                 
                        
                        //-----------------------DEFAULT ADRESS---------------------------
                            defaultAddress = new Address()
                           {
                               FirstName = FirstName,
                               LastName = LastName,
                               Email = Email,
                               Company = null,
                               CountryId = 73,
                               StateProvinceId = null,
                               City = City,
                               Address1 = Adress1,
                               Address2 = Adress2,
                               ZipPostalCode = PostalCode,
                               PhoneNumber = Phone,
                               FaxNumber = null,
                               CreatedOnUtc = customer.CreatedOnUtc
                           };

                        //if (this._addressService.IsAddressValidWithoutEmail(defaultAddress))
                        //{
                            //some validation
                            if (defaultAddress.CountryId == 0)
                                defaultAddress.CountryId = null;
                            if (defaultAddress.StateProvinceId == 0)
                                defaultAddress.StateProvinceId = null;
                            //set default address
                            customer.Addresses.Add(defaultAddress);
                            customer.BillingAddress = defaultAddress;
                            customer.ShippingAddress = defaultAddress;
                            //-------------------<<<<<<<<<<<<<< sw 6 stop >>>>>>>>>>>>>>>>------------------------
                            sw5.Stop();
                            //------------------<<<<<<<<<<<<<< sw 6 start >>>>>>>>>>>>>>>>------------------------                       
                            sw6.Start();   
                        
                            _customerService.UpdateCustomer(customer);
                            customerWithAdressId = _customerService.GetCustomerById(customer.Id);
                            //-------------------<<<<<<<<<<<<<< sw 6 stop >>>>>>>>>>>>>>>>------------------------
                            sw6.Stop();
                            //------------------<<<<<<<<<<<<<< sw 6 start >>>>>>>>>>>>>>>>------------------------                       
                            sw7.Start(); 

                            foreach (Address add in customerWithAdressId.Addresses) //There is never going to be more than 1 address at this point.
                            {
                                customer.DefaultAdress = add.Id + ""; // Default adress auto-add
                              
                            }
                            //_customerService.UpdateCustomer(customer);
                            //-------------------<<<<<<<<<<<<<< sw 6 stop >>>>>>>>>>>>>>>>------------------------
                            sw7.Stop();
                        //}
                            //------------------<<<<<<<<<<<<<< sw 6 start >>>>>>>>>>>>>>>>------------------------                       
                            sw8.Start(); 

                        //-----------------------DEFAULT ADRESS---------------------------

                    if (DeliveryAdress1 != null && DeliveryPostalCode != null && DeliveryCity != null && DeliveryName != null) //Requirements to even trying to add the alternative adress
                    {

                        //Name is recieved as one string, try to split by using first space.
                        firstName = "";
                        lastName = "";

                        if(DeliveryName.Contains(" ")){
                                array = DeliveryName.Split(' ');
                                 firstName = array[0];
                                 lastName = array[1];
                                 iz = 2;
                            length = array.Length;
                            while (iz < length)
                            {
                                if (array[iz] != null)
                                {
                                    lastName = lastName + " " + array[iz];
                                    iz++;
                                }
                            }
                        }

                        SecondaryAdress = new Address()
                        {
                            FirstName = firstName,
                            LastName = lastName,
                            Email = Email,
                            Company = null,
                            CountryId = 73,
                            StateProvinceId = null,
                            City = DeliveryCity,
                            Address1 = DeliveryAdress1,
                            Address2 = DeliveryAdress2,
                            ZipPostalCode = DeliveryPostalCode,
                            PhoneNumber = Phone,
                            FaxNumber = null,
                            CreatedOnUtc = customer.CreatedOnUtc
                        };                        
                      
                        customer.Addresses.Add(SecondaryAdress);  
                        //_customerService.UpdateCustomer(customer);
                     
                    }


                    if (Blacklisted == "1")
                    {
                        customer.Active = false;
                    }

                    _customerService.UpdateCustomer(customer);
                    //-------------------<<<<<<<<<<<<<< sw 6 stop >>>>>>>>>>>>>>>>------------------------
                    sw8.Stop();

                    }

                    nrOfImportedCustomers++;

                    if (iRow % 50 == 0)
                    {                     
                     
                        //_logger.Debug("Import Status. Current Row: " + iRow + "   Number of imported customers: " + nrOfImportedCustomers);
                        _logger.Debug(" Row: " + iRow + "     sw0: " + sw0.Elapsed.Milliseconds + "     sw2: " + sw2.Elapsed.Milliseconds + "     sw3: " + sw3.Elapsed.Milliseconds + "     sw4: " + sw4.Elapsed.Milliseconds + "     sw5: " + sw5.Elapsed.Milliseconds + "     sw6: " + sw6.Elapsed.Milliseconds + "     sw7: " + sw7.Elapsed.Milliseconds + "     sw8: " + sw8.Elapsed.Milliseconds);
                    }
                }catch(Exception e){
                    _logger.Debug("Row: " + iRow + " CustomerNumber: " + CustomerNumber + " << FATAL >> Unhandled exception " + e.Message + ", " + e.InnerException);
                 
                }
                    if (customer != null)
                    {
                        _customerService.DetachCustomer(customer);

                        if (defaultAddress != null)
                        {
                            _addressService.DetachCustomerAdress(defaultAddress);
                        }
                        if (SecondaryAdress != null)
                        {
                            _addressService.DetachCustomerAdress(SecondaryAdress);
                        }    
                    
                        
                      
                    }
                    iRow++;
                }
                totalTime.Stop();
                    //next customer
                _logger.Debug("Customer Import End. Total time: "+ totalTime.Elapsed);
                //return iRow;
                }            
            }
        



        /// <summary>
        /// Import products from XLSX file
        /// </summary>
        /// <param name="stream">Stream</param>
        public virtual void ImportProductsFromXlsx(Stream stream)
        {
            // ok, we can run the real code of the sample now
            using (var xlPackage = new ExcelPackage(stream))
            {
                // get the first worksheet in the workbook
                var worksheet = xlPackage.Workbook.Worksheets.FirstOrDefault();
                if (worksheet == null)
                    throw new NopException("No worksheet found");

                //the columns
                var properties = new string[]
                {
                    "ProductTypeId",
                    "ParentGroupedProductId",
                    "VisibleIndividually",
                    "Name",
                    "ShortDescription",
                    "FullDescription",
                    "VendorId",
                    "ProductTemplateId",
                    "ShowOnHomePage",
                    "MetaKeywords",
                    "MetaDescription",
                    "MetaTitle",
                    "SeName",
                    "AllowCustomerReviews",
                    "Published",
                    "SKU",
                    "ManufacturerPartNumber",
                    "Gtin",
                    "IsGiftCard",
                    "GiftCardTypeId",
                    "RequireOtherProducts",
                    "RequiredProductIds",
                    "AutomaticallyAddRequiredProducts",
                    "IsDownload",
                    "DownloadId",
                    "UnlimitedDownloads",
                    "MaxNumberOfDownloads",
                    "DownloadActivationTypeId",
                    "HasSampleDownload",
                    "SampleDownloadId",
                    "HasUserAgreement",
                    "UserAgreementText",
                    "IsRecurring",
                    "RecurringCycleLength",
                    "RecurringCyclePeriodId",
                    "RecurringTotalCycles",
                    "IsShipEnabled",
                    "IsFreeShipping",
                    "AdditionalShippingCharge",
                    "IsTaxExempt",
                    "TaxCategoryId",
                    "ManageInventoryMethodId",
                    "StockQuantity",
                    "DisplayStockAvailability",
                    "DisplayStockQuantity",
                    "MinStockQuantity",
                    "LowStockActivityId",
                    "NotifyAdminForQuantityBelow",
                    "BackorderModeId",
                    "AllowBackInStockSubscriptions",
                    "OrderMinimumQuantity",
                    "OrderMaximumQuantity",
                    "AllowedQuantities",
                    "DisableBuyButton",
                    "DisableWishlistButton",
                    "CallForPrice",
                    "Price",
                    "OldPrice",
                    "ProductCost",
                    "SpecialPrice",
                    "SpecialPriceStartDateTimeUtc",
                    "SpecialPriceEndDateTimeUtc",
                    "CustomerEntersPrice",
                    "MinimumCustomerEnteredPrice",
                    "MaximumCustomerEnteredPrice",
                    "Weight",
                    "Length",
                    "Width",
                    "Height",
                    "CreatedOnUtc",
                    "CategoryIds",
                    "ManufacturerIds",
                    "Picture1",
                    "Picture2",
                    "Picture3",
                     //------------------------------------EDITED-----------------------------
                    "isRequireSerialNumber",
                    "isPackageArticle",
                     //------------------------------------EDITED-----------------------------
                       
                };


                int iRow = 2;
                while (true)
                {
                    bool allColumnsAreEmpty = true;
                    for (var i = 1; i <= properties.Length; i++)
                        if (worksheet.Cells[iRow, i].Value != null && !String.IsNullOrEmpty(worksheet.Cells[iRow, i].Value.ToString()))
                        {
                            allColumnsAreEmpty = false;
                            break;
                        }
                    if (allColumnsAreEmpty)
                        break;

                    int productTypeId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "ProductTypeId")].Value);
                    int parentGroupedProductId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "ParentGroupedProductId")].Value);
                    bool visibleIndividually = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "VisibleIndividually")].Value);
                    string name = worksheet.Cells[iRow, GetColumnIndex(properties, "Name")].Value as string;
                    string shortDescription = worksheet.Cells[iRow, GetColumnIndex(properties, "ShortDescription")].Value as string;
                    string fullDescription = worksheet.Cells[iRow, GetColumnIndex(properties, "FullDescription")].Value as string;
                    int vendorId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "VendorId")].Value);
                    int productTemplateId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "ProductTemplateId")].Value);
                    bool showOnHomePage = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "ShowOnHomePage")].Value);
                    string metaKeywords = worksheet.Cells[iRow, GetColumnIndex(properties, "MetaKeywords")].Value as string;
                    string metaDescription = worksheet.Cells[iRow, GetColumnIndex(properties, "MetaDescription")].Value as string;
                    string metaTitle = worksheet.Cells[iRow, GetColumnIndex(properties, "MetaTitle")].Value as string;
                    string seName = worksheet.Cells[iRow, GetColumnIndex(properties, "SeName")].Value as string;
                    bool allowCustomerReviews = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "AllowCustomerReviews")].Value);
                    bool published = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "Published")].Value);
                    
                    string sku = worksheet.Cells[iRow, GetColumnIndex(properties, "SKU")].Value as string;
                    //--------------------------EDITED---------------------------------
                    if (sku == null)
                    {
                        sku = ""+worksheet.Cells[iRow, GetColumnIndex(properties, "SKU")].Value;
                    }
                    string manufacturerPartNumber = worksheet.Cells[iRow, GetColumnIndex(properties, "ManufacturerPartNumber")].Value as string;
                    string gtin = worksheet.Cells[iRow, GetColumnIndex(properties, "Gtin")].Value as string;
                    bool isGiftCard = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "IsGiftCard")].Value);
                    int giftCardTypeId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "GiftCardTypeId")].Value);
                    bool requireOtherProducts = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "RequireOtherProducts")].Value);
                    string requiredProductIds = worksheet.Cells[iRow, GetColumnIndex(properties, "RequiredProductIds")].Value as string;
                    bool automaticallyAddRequiredProducts = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "AutomaticallyAddRequiredProducts")].Value);
                    bool isDownload = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "IsDownload")].Value);
                    int downloadId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "DownloadId")].Value);
                    bool unlimitedDownloads = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "UnlimitedDownloads")].Value);
                    int maxNumberOfDownloads = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "MaxNumberOfDownloads")].Value);
                    int downloadActivationTypeId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "DownloadActivationTypeId")].Value);
                    bool hasSampleDownload = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "HasSampleDownload")].Value);
                    int sampleDownloadId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "SampleDownloadId")].Value);
                    bool hasUserAgreement = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "HasUserAgreement")].Value);
                    string userAgreementText = worksheet.Cells[iRow, GetColumnIndex(properties, "UserAgreementText")].Value as string;
                    bool isRecurring = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "IsRecurring")].Value);
                    int recurringCycleLength = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "RecurringCycleLength")].Value);
                    int recurringCyclePeriodId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "RecurringCyclePeriodId")].Value);
                    int recurringTotalCycles = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "RecurringTotalCycles")].Value);
                    bool isShipEnabled = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "IsShipEnabled")].Value);
                    bool isFreeShipping = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "IsFreeShipping")].Value);
                    decimal additionalShippingCharge = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "AdditionalShippingCharge")].Value);
                    bool isTaxExempt = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "IsTaxExempt")].Value);
                    int taxCategoryId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "TaxCategoryId")].Value);
                    int manageInventoryMethodId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "ManageInventoryMethodId")].Value);
                    int stockQuantity = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "StockQuantity")].Value);
                    bool displayStockAvailability = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "DisplayStockAvailability")].Value);
                    bool displayStockQuantity = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "DisplayStockQuantity")].Value);
                    int minStockQuantity = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "MinStockQuantity")].Value);
                    int lowStockActivityId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "LowStockActivityId")].Value);
                    int notifyAdminForQuantityBelow = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "NotifyAdminForQuantityBelow")].Value);
                    int backorderModeId = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "BackorderModeId")].Value);
                    bool allowBackInStockSubscriptions = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "AllowBackInStockSubscriptions")].Value);
                    int orderMinimumQuantity = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "OrderMinimumQuantity")].Value);
                    int orderMaximumQuantity = Convert.ToInt32(worksheet.Cells[iRow, GetColumnIndex(properties, "OrderMaximumQuantity")].Value);
                    string allowedQuantities = worksheet.Cells[iRow, GetColumnIndex(properties, "AllowedQuantities")].Value as string;
                    bool disableBuyButton = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "DisableBuyButton")].Value);
                    bool disableWishlistButton = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "DisableWishlistButton")].Value);
                    bool callForPrice = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "CallForPrice")].Value);
                    decimal price = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "Price")].Value);
                    decimal oldPrice = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "OldPrice")].Value);
                    decimal productCost = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "ProductCost")].Value);
                    decimal? specialPrice = null;
                    var specialPriceExcel = worksheet.Cells[iRow, GetColumnIndex(properties, "SpecialPrice")].Value;
                    if (specialPriceExcel != null)
                        specialPrice = Convert.ToDecimal(specialPriceExcel);
                    DateTime? specialPriceStartDateTimeUtc = null;
                    var specialPriceStartDateTimeUtcExcel = worksheet.Cells[iRow, GetColumnIndex(properties, "SpecialPriceStartDateTimeUtc")].Value;
                    if (specialPriceStartDateTimeUtcExcel != null)
                        specialPriceStartDateTimeUtc = DateTime.FromOADate(Convert.ToDouble(specialPriceStartDateTimeUtcExcel));
                    DateTime? specialPriceEndDateTimeUtc = null;
                    var specialPriceEndDateTimeUtcExcel = worksheet.Cells[iRow, GetColumnIndex(properties, "SpecialPriceEndDateTimeUtc")].Value;
                    if (specialPriceEndDateTimeUtcExcel != null)
                        specialPriceEndDateTimeUtc = DateTime.FromOADate(Convert.ToDouble(specialPriceEndDateTimeUtcExcel));

                    bool customerEntersPrice = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "CustomerEntersPrice")].Value);
                    decimal minimumCustomerEnteredPrice = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "MinimumCustomerEnteredPrice")].Value);
                    decimal maximumCustomerEnteredPrice = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "MaximumCustomerEnteredPrice")].Value);
                    decimal weight = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "Weight")].Value);
                    decimal length = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "Length")].Value);
                    decimal width = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "Width")].Value);
                    decimal height = Convert.ToDecimal(worksheet.Cells[iRow, GetColumnIndex(properties, "Height")].Value);
                    DateTime createdOnUtc = DateTime.FromOADate(Convert.ToDouble(worksheet.Cells[iRow, GetColumnIndex(properties, "CreatedOnUtc")].Value));
                    string categoryIds = worksheet.Cells[iRow, GetColumnIndex(properties, "CategoryIds")].Value as string;
                    string manufacturerIds = worksheet.Cells[iRow, GetColumnIndex(properties, "ManufacturerIds")].Value as string;
                    string picture1 = worksheet.Cells[iRow, GetColumnIndex(properties, "Picture1")].Value as string;
                    string picture2 = worksheet.Cells[iRow, GetColumnIndex(properties, "Picture2")].Value as string;
                    string picture3 = worksheet.Cells[iRow, GetColumnIndex(properties, "Picture3")].Value as string;

                    //------------------------------------EDITED-----------------------------
                    bool isRequireSerialNumber = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "isRequireSerialNumber")].Value);
                   
                    bool isPackageArticle = Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "isPackageArticle")].Value);
                    //------------------------------------EDITED-----------------------------

                    //var readIn = worksheet.Cells[iRow, GetColumnIndex(properties, "isPackageArticle")].Value;


                    //if(typeof(readIn) ==  bool){}

                    //if(readIn is bool){
                    //Convert.ToBoolean(worksheet.Cells[iRow, GetColumnIndex(properties, "isPackageArticle")].Value);
                    
                    //}

                    var product = _productService.GetProductBySku(sku);
                    bool newProduct = false;
                    if (product == null)
                    {
                        product = new Product();
                        newProduct = true;
                    }
                    product.ProductTypeId = productTypeId;
                    product.ParentGroupedProductId = parentGroupedProductId;
                    product.VisibleIndividually = visibleIndividually;
                    product.Name = name;
                    product.ShortDescription = shortDescription;
                    product.FullDescription = fullDescription;
                    product.VendorId = vendorId;
                    product.ProductTemplateId = productTemplateId;
                    product.ShowOnHomePage = showOnHomePage;
                    product.MetaKeywords = metaKeywords;
                    product.MetaDescription = metaDescription;
                    product.MetaTitle = metaTitle;
                    product.AllowCustomerReviews = allowCustomerReviews;
                    product.Sku = sku;
                    product.ManufacturerPartNumber = manufacturerPartNumber;
                    product.Gtin = gtin;
                    product.IsGiftCard = isGiftCard;
                    product.GiftCardTypeId = giftCardTypeId;
                    product.RequireOtherProducts = requireOtherProducts;
                    product.RequiredProductIds = requiredProductIds;
                    product.AutomaticallyAddRequiredProducts = automaticallyAddRequiredProducts;
                    product.IsDownload = isDownload;
                    product.DownloadId = downloadId;
                    product.UnlimitedDownloads = unlimitedDownloads;
                    product.MaxNumberOfDownloads = maxNumberOfDownloads;
                    product.DownloadActivationTypeId = downloadActivationTypeId;
                    product.HasSampleDownload = hasSampleDownload;
                    product.SampleDownloadId = sampleDownloadId;
                    product.HasUserAgreement = hasUserAgreement;
                    product.UserAgreementText = userAgreementText;
                    product.IsRecurring = isRecurring;
                    product.RecurringCycleLength = recurringCycleLength;
                    product.RecurringCyclePeriodId = recurringCyclePeriodId;
                    product.RecurringTotalCycles = recurringTotalCycles;
                    product.IsShipEnabled = isShipEnabled;
                    product.IsFreeShipping = isFreeShipping;
                    product.AdditionalShippingCharge = additionalShippingCharge;
                    product.IsTaxExempt = isTaxExempt;
                    product.TaxCategoryId = taxCategoryId;
                    product.ManageInventoryMethodId = manageInventoryMethodId;
                    product.StockQuantity = stockQuantity;
                    product.DisplayStockAvailability = displayStockAvailability;
                    product.DisplayStockQuantity = displayStockQuantity;
                    product.MinStockQuantity = minStockQuantity;
                    product.LowStockActivityId = lowStockActivityId;
                    product.NotifyAdminForQuantityBelow = notifyAdminForQuantityBelow;
                    product.BackorderModeId = backorderModeId;
                    product.AllowBackInStockSubscriptions = allowBackInStockSubscriptions;
                    product.OrderMinimumQuantity = orderMinimumQuantity;
                    product.OrderMaximumQuantity = orderMaximumQuantity;
                    product.AllowedQuantities = allowedQuantities;
                    product.DisableBuyButton = disableBuyButton;
                    product.DisableWishlistButton = disableWishlistButton;
                    product.CallForPrice = callForPrice;
                    product.Price = price;
                    product.OldPrice = oldPrice;
                    product.ProductCost = productCost;
                    product.SpecialPrice = specialPrice;
                    product.SpecialPriceStartDateTimeUtc = specialPriceStartDateTimeUtc;
                    product.SpecialPriceEndDateTimeUtc = specialPriceEndDateTimeUtc;
                    product.CustomerEntersPrice = customerEntersPrice;
                    product.MinimumCustomerEnteredPrice = minimumCustomerEnteredPrice;
                    product.MaximumCustomerEnteredPrice = maximumCustomerEnteredPrice;
                    product.Weight = weight;
                    product.Length = length;
                    product.Width = width;
                    product.Height = height;
                    product.Published = published;
                    product.CreatedOnUtc = createdOnUtc;
                    product.UpdatedOnUtc = DateTime.UtcNow;

                    //------------------------------------EDITED-----------------------------
                    product.isRequireSerialNumber = isRequireSerialNumber;
                    product.isPackageArticle = isPackageArticle;
                    //------------------------------------EDITED-----------------------------                   


                    if (newProduct)
                    {
                        _productService.InsertProduct(product);
                    }
                    else
                    {
                        _productService.UpdateProduct(product);
                    }

                    //search engine name
                    _urlRecordService.SaveSlug(product, product.ValidateSeName(seName, product.Name, true), 0);

                    //category mappings
                    if (!String.IsNullOrEmpty(categoryIds))
                    {
                        foreach (var id in categoryIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x.Trim())))
                        {
                            if (product.ProductCategories.FirstOrDefault(x => x.CategoryId == id) == null)
                            {
                                //ensure that category exists
                                var category = _categoryService.GetCategoryById(id);
                                if (category != null)
                                {
                                    var productCategory = new ProductCategory()
                                    {
                                        ProductId = product.Id,
                                        CategoryId = category.Id,
                                        IsFeaturedProduct = false,
                                        DisplayOrder = 1
                                    };
                                    _categoryService.InsertProductCategory(productCategory);
                                }
                            }
                        }
                    }

                    //manufacturer mappings
                    if (!String.IsNullOrEmpty(manufacturerIds))
                    {
                        foreach (var id in manufacturerIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x.Trim())))
                        {
                            if (product.ProductManufacturers.FirstOrDefault(x => x.ManufacturerId == id) == null)
                            {
                                //ensure that manufacturer exists
                                var manufacturer = _manufacturerService.GetManufacturerById(id);
                                if (manufacturer != null)
                                {
                                    var productManufacturer = new ProductManufacturer()
                                    {
                                        ProductId = product.Id,
                                        ManufacturerId = manufacturer.Id,
                                        IsFeaturedProduct = false,
                                        DisplayOrder = 1
                                    };
                                    _manufacturerService.InsertProductManufacturer(productManufacturer);
                                }
                            }
                        }
                    }

                    //pictures
                    foreach (var picture in new string[] { picture1, picture2, picture3 })
                    {
                        if (String.IsNullOrEmpty(picture))
                            continue;

                        product.ProductPictures.Add(new ProductPicture()
                        {
                            Picture = _pictureService.InsertPicture(File.ReadAllBytes(picture), "image/jpeg", _pictureService.GetPictureSeName(name), true),
                            DisplayOrder = 1,
                        });
                        _productService.UpdateProduct(product);
                    }

                    //update "HasTierPrices" and "HasDiscountsApplied" properties
                    _productService.UpdateHasTierPricesProperty(product);
                    _productService.UpdateHasDiscountsApplied(product);



                    //next product
                    iRow++;
                }
            }
        }
    }
}
