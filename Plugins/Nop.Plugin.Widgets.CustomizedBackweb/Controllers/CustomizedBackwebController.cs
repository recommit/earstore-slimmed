﻿/*using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.CustomizedBackweb.Controllers
{
    class CustomizedBackwebController : Controller
    {

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            return View("Nop.Plugin.Widgets.CustomizedBackweb.Views.CustomizedBackweb.Configure");
        }

    }
}*/

using System.Web.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.CustomizedBackweb.Models;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Admin.Controllers;
using Nop.Core.Domain.Orders;
using System;
using Nop.Services.Orders;
using Nop.Services.Catalog;
using Nop.Core.Domain.Catalog;
using System.Text;

namespace Nop.Plugin.Widgets.CustomizedBackweb.Controllers
{
    public class CustomizedBackwebController : Controller
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;

        private readonly IOrderService _orderService;
        private readonly IProductService _productService;



        public CustomizedBackwebController(IWorkContext workContext,
            IStoreContext storeContext, IStoreService storeService,
            IPictureService pictureService, ISettingService settingService, IOrderService orderService,
            IProductService productService)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._pictureService = pictureService;
            this._settingService = settingService;

            this._orderService = orderService;
            this._productService = productService;
        }

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
           // var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(storeScope);
           // var model = new ConfigurationModel();

            var model = new ConfigurationModel();
            model.Text1 = "Någon inställning";
            return View("Nop.Plugin.Widgets.CustomizedBackweb.Views.CustomizedBackweb.Configure", model);
        }

        public ActionResult PlaceNewOrder()
        {
         
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);        

            var order = new Order();
            Guid g = new Guid();
            order.OrderGuid = g;
            order.CustomerId = 1;
            order.StoreId = 1;
            order.BillingAddressId = 1;
            order.ShippingAddressId = 1;
            order.ShippingStatusId = 20;
            order.PaymentStatusId = 10;
            order.OrderStatusId = 10;
            order.PaymentMethodSystemName = "Payments.CashOnDelivery";
            order.CustomerCurrencyCode = "USD";
            order.CreatedOnUtc = DateTime.Now.ToUniversalTime();

            // here I am giving these values hard coded. 
            // Is there any way to get all these details by passing product id and quantity             
            order.OrderTotal = 100.10m;
            order.OrderSubtotalInclTax = 100.40m;
            order.OrderSubtotalExclTax = 50.40m;

            order.OrderItems.Add(new OrderItem()
            {
                ProductId = 1,
                PriceInclTax = 10.35m,
                UnitPriceExclTax = 20.65m,
                Quantity = 3
            });

            order.OrderTotal = 500m;
            _orderService.InsertOrder(order);
            // Insert Order in NopCommerce.        

            var model = new ConfigurationModel();
            model.Text1 = "Någon inställning";
            return View("Nop.Plugin.Widgets.CustomizedBackweb.Views.CustomizedBackweb.Configure", model);
        }


        public ActionResult GetProductByID(ConfigurationModel model)
        {
          //var model = new ConfigurationModel();
           
            //Product p = _productService.GetProductById(model.SearchString);

           // model.ProductInfo = p.Name;
           // model.Text1 = "Någon inställning";
        
            return View("Nop.Plugin.Widgets.CustomizedBackweb.Views.CustomizedBackweb.Configure", model);
        }
              
        public ActionResult BackwebOrder()
        {
            BackwebOrder model = new BackwebOrder();
            return View("Nop.Plugin.Widgets.CustomizedBackweb.Views.CustomizedBackweb.BackwebOrder", model);
        }

        [HttpPost]
        public ActionResult BackwebOrderAction(BackwebOrder model)
        {
            decimal simpleInteresrt = (model.Amount * model.Year * model.Rate) / 100;
            StringBuilder sbInterest = new StringBuilder();
            sbInterest.Append("<b>Amount :</b> " + model.Amount + "<br/>");
            sbInterest.Append("<b>Rate :</b> " + model.Rate + "<br/>");
            sbInterest.Append("<b>Time(year) :</b> " + model.Year + "<br/>");
            sbInterest.Append("<b>Interest :</b> " + simpleInteresrt);
            return Content(sbInterest.ToString());
        }
                    
    }
}
