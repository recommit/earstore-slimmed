using Nop.Plugin.Other.ProductViewTracker.Domain;

namespace Nop.Plugin.Other.ProductViewTracker.Services
{
    public interface IViewTrackingService
    {
        /// <summary>
        /// Logs the specified record.
        /// </summary>
        /// <param name="record">The record.</param>
        void Log(TrackingRecord record);
    }
}