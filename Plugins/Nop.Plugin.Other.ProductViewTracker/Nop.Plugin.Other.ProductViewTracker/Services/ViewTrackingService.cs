using Nop.Core.Data;
using Nop.Plugin.Other.ProductViewTracker.Domain;

namespace Nop.Plugin.Other.ProductViewTracker.Services
{
    public class ViewTrackingService : IViewTrackingService
    {
        private readonly IRepository<TrackingRecord> _trackingRecordRepository;

        public ViewTrackingService(IRepository<TrackingRecord> trackingRecordRepository)
        {
            _trackingRecordRepository = trackingRecordRepository;
        }

        #region Implementation of IViewTrackingService

        /// <summary>
        /// Logs the specified record.
        /// </summary>
        /// <param name="record">The record.</param>
        public void Log(TrackingRecord record)
        {
            _trackingRecordRepository.Insert(record);
        }

        #endregion
    }
}