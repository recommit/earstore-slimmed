
using Nop.Core;

namespace Nop.Plugin.Other.ProductViewTracker.Domain
{
    public class TrackingRecord : BaseEntity
    {
        public virtual int ProductId { get; set; }
        public virtual string ProductName { get; set; }
        public virtual int CustomerId { get; set; }
        public virtual string IpAddress { get; set; }
        public virtual bool IsRegistered { get; set; }
    }
}
