﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Other.ProductViewTracker.Domain;
using Nop.Plugin.Other.ProductViewTracker.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Core.Plugins;

namespace Nop.Plugin.Other.ProductViewTracker.Controllers
{
    public class TrackingController : Controller
    {
        private readonly IProductService _productService;
        private readonly IViewTrackingService _viewTrackingService;
        private readonly IWorkContext _workContext;

        public TrackingController(IWorkContext workContext, 
            IViewTrackingService viewTrackingService, 
            IProductService productService,
            IPluginFinder pluginFinder)
        {
            _workContext = workContext;
            _viewTrackingService = viewTrackingService;
            _productService = productService;
        }

        [ChildActionOnly]
        public ActionResult Index(int productId)
        {
            //Read from the product service
            Product productById = _productService.GetProductById(productId);

            //If the product exists we will log it
            if (productById != null)
            {
                //Setup the product to save
                var record = new TrackingRecord();
                record.ProductId = productId;
                record.ProductName = productById.Name;
                record.CustomerId = _workContext.CurrentCustomer.Id;
                record.IpAddress = _workContext.CurrentCustomer.LastIpAddress;
                record.IsRegistered = _workContext.CurrentCustomer.IsRegistered();

                //Map the values we're interested in to our new entity
                _viewTrackingService.Log(record);
            }

            //Return the view, it doesn't need a model
            return Content("-------------------------- <br><br> --------------------------------------------------");
        }
    }
}