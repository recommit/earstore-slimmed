using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Other.ProductViewTracker.Domain;

namespace Nop.Plugin.Other.ProductViewTracker.Data
{
    public class TrackingRecordMap : EntityTypeConfiguration<TrackingRecord>
    {
        public TrackingRecordMap()
        {
            ToTable("ProductViewTracking");

            //Map the primary key
            HasKey(m => m.Id);
            //Map the additional properties
            Property(m => m.ProductId);
            //Avoiding truncation/failure 
            //so we set the same max length used in the product tame
            Property(m => m.ProductName).HasMaxLength(400);
            Property(m => m.IpAddress);
            Property(m => m.CustomerId);
            Property(m => m.IsRegistered);
        }
    }

}
