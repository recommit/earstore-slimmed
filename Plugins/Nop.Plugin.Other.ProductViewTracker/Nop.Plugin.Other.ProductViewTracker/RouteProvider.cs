﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Other.ProductViewTracker
{
    public class ProductViewTrackerRouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Nop.Plugin.Other.ProductViewTracker.Log", "tracking/productviews/{productId}", new { controller = "Tracking", action = "Index" }, new[] { "Nop.Plugin.Other.ProductViewTracker.Controllers" });
        }

        public int Priority
        {
            get { return 0; }
        }
    }
}