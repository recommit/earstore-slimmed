using Nop.Core.Plugins;
using Nop.Plugin.Other.ProductViewTracker.Data;

namespace Nop.Plugin.Other.ProductViewTracker
{
    public class ProductViewTrackerPlugin : BasePlugin
    {
        private readonly TrackingRecordObjectContext _context;

        public ProductViewTrackerPlugin(TrackingRecordObjectContext context)
        {
            _context = context;
        }

        public override void Install()
        {
            _context.Install();
            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            _context.Uninstall();
            base.Uninstall();
        }
    }
}