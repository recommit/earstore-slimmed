﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Services.Common;
using System.Web.Routing;
using Nop.Services.Cms;

namespace Nop.Plugin.Widgets.Backweb
{
    public class Backweb : BasePlugin, IMiscPlugin
    {
        public void GetConfigurationRoute(out string actionName,
                   out string controllerName,
                   out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "Backweb";
            routeValues = new RouteValueDictionary()
            {
                { "Namespaces", "Nop.Plugin.Widgets.Backweb.Controllers" }, 
                { "area", null }
            };
        }
    }
}
