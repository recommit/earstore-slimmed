﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.Backweb.Models;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Admin.Controllers;
using Nop.Core.Domain.Orders;
using System;
using Nop.Services.Orders;
using Nop.Services.Catalog;
using Nop.Core.Domain.Catalog;
using System.Text;
using Nop.Services.Customers;

namespace Nop.Plugin.Widgets.Backweb.Controllers
{
    public class BackwebController : Controller
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;

        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly ICustomerService _customerService;



        public BackwebController(IWorkContext workContext,
            IStoreContext storeContext, IStoreService storeService,
            IPictureService pictureService, ISettingService settingService, IOrderService orderService,
            IProductService productService, ICustomerService customerService)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._pictureService = pictureService;
            this._settingService = settingService;

            this._orderService = orderService;
            this._productService = productService;
            this._customerService = customerService;
        }

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            // var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(storeScope);
            // var model = new ConfigurationModel();

            var model = new ConfigurationModel();
            model.Text1 = "Någon inställning";
            return View("Nop.Plugin.Widgets.Backweb.Views.Backweb.Configure", model);
        }

        public ActionResult MakeBackwebOrder()
        {
            BackwebOrderModel model = new BackwebOrderModel();

            /*var order = new Order();
            Guid g = new Guid();
            order.OrderGuid = g;
            order.CustomerId = 1; 
            order.StoreId = 1;
            order.BillingAddressId = 1;
            order.ShippingAddressId = 1;
            order.ShippingStatusId = 20;
            order.PaymentStatusId = 10;
            order.OrderStatusId = 10;
            order.PaymentMethodSystemName = "Payments.CashOnDelivery";
            order.CustomerCurrencyCode = "USD";
            order.CreatedOnUtc = DateTime.Now.ToUniversalTime();

            // here I am giving these values hard coded. 
            // Is there any way to get all these details by passing product id and quantity             
            order.OrderTotal = 100.10m;
            order.OrderSubtotalInclTax = 100.40m;
            order.OrderSubtotalExclTax = 50.40m;

            order.OrderItems.Add(new OrderItem()
            {
                ProductId = 1,
                PriceInclTax = 10.35m,
                UnitPriceExclTax = 20.65m,
                Quantity = 3
            });

            order.OrderTotal = 500m;

            model.NCorder = order;

            //if (order.CustomerId != 0)
            //{
              
            //}

            //_orderService.InsertOrder(order);
            // Insert Order in NopCommerce.  
*/
/*
            model.AddedProducts.Add(new OrderItem()
            {
                ProductId = 1,
                PriceInclTax = 10.35m,
                UnitPriceExclTax = 20.65m,
                Quantity = 3
            });*/
                          
           
            return View("Nop.Plugin.Widgets.Backweb.Views.Backweb.MakeBackwebOrder", model);
        }

        public string test()
        {       
            return "hej";
        }

        [HttpPost]
        public ActionResult SaveChanges(BackwebOrderModel model)
        {
            if (model.customerID != null)
            {
                model.NCcustomer = _customerService.GetCustomerById(model.customerID);                  
            }

  
          
           
            return View("Nop.Plugin.Widgets.Backweb.Views.Backweb.MakeBackwebOrder", model); ;
        }




        //-----------------------------------------------------------------------------------


        //
        // GET: /Backweb/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Backweb/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Backweb/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Backweb/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Backweb/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Backweb/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Backweb/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Backweb/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
