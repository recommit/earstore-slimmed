﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public class ReturnRequestListModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.ReturnRequests.List.searchStatus")]
        public string searchStatus { get; set; }
    }
}