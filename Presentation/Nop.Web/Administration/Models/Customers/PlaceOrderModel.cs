﻿using System;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Admin.Models.ShoppingCart;
using Nop.Core.Domain.Discounts;

//using Nop.Web.Models.ShoppingCart.AdminOrderCart.cs


namespace Nop.Admin.Models.Customers
{
    public class PlaceOrderModel : BaseNopModel
    {
        public PlaceOrderModel()     
        {
            AvailableCategories = new List<SelectListItem>();
            AvailableManufacturers = new List<SelectListItem>();
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableProductTypes = new List<SelectListItem>();

            DiscountsList = new List<Discount>();
            AddedDiscountsList = new List<BackwebDiscount>();

            adminOrderCart = new AdminOrderCart();
            SearchIncludeSubCategories = true;  
        }



        public AdminOrderCart adminOrderCart { get; set; }

        public string customerID { get; set; }
        public string adressBilling { get; set; }
        public string adressBillingShortDesc { get; set; }
        public string adressDelivery { get; set; }
        public string adressDeliveryShortDesc { get; set; }
        public string customerFullName { get; set; }
        public string customerNumber { get; set; }

        public string noteAdmin { get; set; }
        public string noteCustomer { get; set; }

        public string quickAddQuantity { get; set; }
        public string quickAddName { get; set; }
        public string quickAddID { get; set; }

        public string orderMethod { get; set; }

        
        

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchProductName")]
        [AllowHtml]
        public string SearchProductName { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchCategory")]
        public int SearchCategoryId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchIncludeSubCategories")]
        public bool SearchIncludeSubCategories { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchManufacturer")]
        public int SearchManufacturerId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchStore")]
        public int SearchStoreId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchVendor")]
        public int SearchVendorId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchProductType")]
        public int SearchProductTypeId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.NoOfProducts")]
        public int NoOfProducts { get; set; }


        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchProductSKU")]
        public string SearchProductSKU { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.GoDirectlyToSku")]
        [AllowHtml]
        public string GoDirectlyToSku { get; set; }

        public bool DisplayProductPictures { get; set; }

        public bool IsLoggedInAsVendor { get; set; }

        public IList<SelectListItem> AvailableCategories { get; set; }
        public IList<SelectListItem> AvailableManufacturers { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableProductTypes { get; set; }

        public IList<Discount> DiscountsList { get; set; }
        public IList<BackwebDiscount> AddedDiscountsList { get; set; }
        
    }
}