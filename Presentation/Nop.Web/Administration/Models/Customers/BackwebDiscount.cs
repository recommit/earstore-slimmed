﻿using Nop.Core.Domain.Discounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Customers
{
    public class BackwebDiscount //This class is used in the backweb to enable more than 1 discount of a specific type.
    {
        public BackwebDiscount()
        {
            quantity = 1;         
        }
        public Discount discount { get; set; }
        public int quantity { get; set; }
        public int nrOfTimesApplied { get; set; }
    }
}
