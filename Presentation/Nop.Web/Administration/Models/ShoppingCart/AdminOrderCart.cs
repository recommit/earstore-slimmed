﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.ShoppingCart
{
    public class AdminOrderCart
    {
        public AdminOrderCart()
        {
            Items = new List<AdminOrderCartItemModel>();
        }

        public IList<AdminOrderCartItemModel> Items { get; set; }
        public int TotalProducts { get; set; }
        public string SubTotal { get; set; }
        public bool DisplayShoppingCartButton { get; set; }
        public bool DisplayCheckoutButton { get; set; }
        public bool CurrentCustomerIsGuest { get; set; }
        public bool AnonymousCheckoutAllowed { get; set; }
        public bool ShowProductImages { get; set; }

        public string ExistingOrderItems { get; set; }


        #region Nested Classes

        public partial class AdminOrderCartItemModel : BaseNopEntityModel
        {
            public AdminOrderCartItemModel()
            {
                //Picture = new PictureModel();
            }

            public int ProductId { get; set; }

            public string ProductName { get; set; }

            public string ProductSeName { get; set; }

            public int Quantity { get; set; }

            public string UnitPrice { get; set; }

            public string TotalPriceWithQuantity { get; set; }

            public string AttributeInfo { get; set; }

            public Boolean DeactivateAutoCalculatePrice { get; set; }
                      

            //public PictureModel Picture { get; set; }
        }

        #endregion
    }
}