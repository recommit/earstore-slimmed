﻿using Nop.Admin.Models.Customers;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Telerik.Web.Mvc;

namespace Nop.Admin.Models.Messages
{
    public partial class NewsLetterSubscriptionListModel : BaseNopModel
    {

        public NewsLetterSubscriptionListModel()
        {
            AvailableCustomerRoles = new List<CustomerRoleModel>();
        }


        [NopResourceDisplayName("Admin.Customers.Customers.List.SearchEmail")]
        public string SearchEmail { get; set; }


        [NopResourceDisplayName("Admin.Customers.Customers.List.CustomerRoleNames")]
        public string CustomerRoleNames { get; set; }
        public List<CustomerRoleModel> AvailableCustomerRoles { get; set; }
        public int[] SelectedCustomerRoleIds { get; set; }
    }
}