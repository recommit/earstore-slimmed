﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    public class UploadProductDocumentModel
    {

        public IList<SelectListItem> UploadedProductDocuments { get; set; }
        public string message { get; set; }
        public string selectedFile { get; set; }
    }
}