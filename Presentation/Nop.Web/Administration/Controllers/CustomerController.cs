﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.ModelBinding;
using System.Web.Mvc;
using Nop.Admin.Models.Common;
using Nop.Admin.Models.Customers;
using Nop.Admin.Models.ShoppingCart;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Authentication.External;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.ExportImport;
using Nop.Services.Forums;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Telerik.Web.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Services.Discounts;
using System.Collections;
using Nop.Core.Domain.Discounts;
using System.Diagnostics;
using System.Data;
using Nop.Data;
using Nop.Core.Data;
using System.Data.SqlClient;


namespace Nop.Admin.Controllers
{
    [AdminAuthorize]
    public partial class CustomerController : BaseNopController
    {
        #region Fields

        private readonly ICustomerService _customerService;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICustomerReportService _customerReportService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly DateTimeSettings _dateTimeSettings;
        private readonly TaxSettings _taxSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IAddressService _addressService;
        private readonly CustomerSettings _customerSettings;
        private readonly ITaxService _taxService;
        private readonly IWorkContext _workContext;
        private readonly IVendorService _vendorService;
        private readonly IStoreContext _storeContext;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderService _orderService;
        private readonly IExportManager _exportManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPermissionService _permissionService;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IEmailAccountService _emailAccountService;
        private readonly ForumSettings _forumSettings;
        private readonly IForumService _forumService;
        private readonly IOpenAuthenticationService _openAuthenticationService;
        private readonly AddressSettings _addressSettings;
        private readonly IStoreService _storeService;

        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IProductService _productService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDiscountService _discountService;
        private readonly ILogger _logger;

        private readonly IDataProvider _dataProvider;
        private readonly IDbContext _dbContext;

        #endregion

        #region Constructors

        public CustomerController( IDataProvider dataProvider, IDbContext dbContext,ILogger logger, ICustomerService customerService,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IGenericAttributeService genericAttributeService,
            ICustomerRegistrationService customerRegistrationService,
            ICustomerReportService customerReportService, 
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService, 
            DateTimeSettings dateTimeSettings,
            TaxSettings taxSettings, 
            RewardPointsSettings rewardPointsSettings,
            ICountryService countryService, 
            IStateProvinceService stateProvinceService, 
            IAddressService addressService,
            CustomerSettings customerSettings,
            ITaxService taxService, 
            IWorkContext workContext,
            IVendorService vendorService,
            IStoreContext storeContext,
            IPriceFormatter priceFormatter,
            IOrderService orderService, 
            IExportManager exportManager,
            ICustomerActivityService customerActivityService,
            IPriceCalculationService priceCalculationService,
            IPermissionService permissionService, 
            IQueuedEmailService queuedEmailService,
            EmailAccountSettings emailAccountSettings,
            IEmailAccountService emailAccountService, 
            ForumSettings forumSettings,
            IForumService forumService, 
            IOpenAuthenticationService openAuthenticationService,
            AddressSettings addressSettings, 
            IStoreService storeService,
            ICategoryService categoryService,
            AdminAreaSettings adminAreaSettings,
            IManufacturerService manufacturerService,
            IProductService productService,
            IWorkflowMessageService workflowMessageService,
            IDiscountService discountService)
        {
            this._customerService = customerService;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._genericAttributeService = genericAttributeService;
            this._customerRegistrationService = customerRegistrationService;
            this._customerReportService = customerReportService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._dateTimeSettings = dateTimeSettings;
            this._taxSettings = taxSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._addressService = addressService;
            this._customerSettings = customerSettings;
            this._taxService = taxService;
            this._workContext = workContext;
            this._vendorService = vendorService;
            this._storeContext = storeContext;
            this._priceFormatter = priceFormatter;
            this._orderService = orderService;
            this._exportManager = exportManager;
            this._customerActivityService = customerActivityService;
            this._priceCalculationService = priceCalculationService;
            this._permissionService = permissionService;
            this._queuedEmailService = queuedEmailService;
            this._emailAccountSettings = emailAccountSettings;
            this._emailAccountService = emailAccountService;
            this._forumSettings = forumSettings;
            this._forumService = forumService;
            this._openAuthenticationService = openAuthenticationService;
            this._addressSettings = addressSettings;
            this._storeService = storeService;

            this._categoryService = categoryService;
            this._adminAreaSettings = adminAreaSettings;
            this._manufacturerService = manufacturerService;
            this._productService = productService;
            this._workflowMessageService = workflowMessageService;
            this._discountService = discountService;
            this._logger = logger;
            this._dataProvider = dataProvider;
            this._dbContext = dbContext;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected string GetCustomerRolesNames(IList<CustomerRole> customerRoles, string separator = ",")
        {
            var sb = new StringBuilder();
            for (int i = 0; i < customerRoles.Count; i++)
            {
                sb.Append(customerRoles[i].Name);
                if (i != customerRoles.Count - 1)
                {
                    sb.Append(separator);
                    sb.Append(" ");
                }
            }
            return sb.ToString();
        }

        [NonAction]
        protected IList<RegisteredCustomerReportLineModel> GetReportRegisteredCustomersModel()
        {
            var report = new List<RegisteredCustomerReportLineModel>();
            report.Add(new RegisteredCustomerReportLineModel()
            {
                Period = _localizationService.GetResource("Admin.Customers.Reports.RegisteredCustomers.Fields.Period.7days"),
                Customers = _customerReportService.GetRegisteredCustomersReport(7)
            });

            report.Add(new RegisteredCustomerReportLineModel()
            {
                Period = _localizationService.GetResource("Admin.Customers.Reports.RegisteredCustomers.Fields.Period.14days"),
                Customers = _customerReportService.GetRegisteredCustomersReport(14)
            });
            report.Add(new RegisteredCustomerReportLineModel()
            {
                Period = _localizationService.GetResource("Admin.Customers.Reports.RegisteredCustomers.Fields.Period.month"),
                Customers = _customerReportService.GetRegisteredCustomersReport(30)
            });
            report.Add(new RegisteredCustomerReportLineModel()
            {
                Period = _localizationService.GetResource("Admin.Customers.Reports.RegisteredCustomers.Fields.Period.year"),
                Customers = _customerReportService.GetRegisteredCustomersReport(365)
            });

            return report;
        }

        [NonAction]
        protected IList<CustomerModel.AssociatedExternalAuthModel> GetAssociatedExternalAuthRecords(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            var result = new List<CustomerModel.AssociatedExternalAuthModel>();
            foreach (var record in _openAuthenticationService.GetExternalIdentifiersFor(customer))
            {
                var method = _openAuthenticationService.LoadExternalAuthenticationMethodBySystemName(record.ProviderSystemName);
                if (method == null)
                    continue;

                result.Add(new CustomerModel.AssociatedExternalAuthModel()
                {
                    Id = record.Id,
                    Email = record.Email,
                    ExternalIdentifier = record.ExternalIdentifier,
                    AuthMethodName = method.PluginDescriptor.FriendlyName
                });
            }

            return result;
        }

        /// <summary>
        /// Method for preparing a customer to be displayed in the customer list.
        /// This method has been modified to also include two specially created strings to easily display the adresses of a customer in the list.
        /// Without this modification one must enter the edit page for a specific customer to retrieve view any adress-data.
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="customer">Core customer-model object</param>
        /// <returns>Customer-model prepared for a list-view</returns>
        [NonAction]
        protected CustomerModel PrepareCustomerModelForList(Customer customer)
        {

            String s = "";
            String s2 = "";
            int i = 1;

            //Also display address-info in the customer list [Earstore 2.0]
            foreach(Address a in customer.Addresses) {
                if (i == 1)
                {
                    s = "[" + a.Address1 + "], ";
                    if (a.Address2 != null && a.Address2 != "")
                    {
                        s = s + " [(Alternativ adress): " + a.Address2 + "], ";
                    }
                    s = s + "[" + a.ZipPostalCode + "], [" + a.City + "]";
                }
                if (i == 2)
                {
                    s2 = "[" + a.Address1 + "], ";
                    if (a.Address2 != null && a.Address2 != "")
                    {
                        s2 = s2 + " [(Alternativ adress): " + a.Address2 + "], ";
                    }
                    s2 = s2 + "[" + a.ZipPostalCode + "], [" + a.City + "]";
                }
                if (i == 3)
                {
                   s2 = s2 +  "[ERROR: Kunden har fler än 2 adresser, kontakta IT-ansvarig.]";
                }
              
                i++;
            }


            return new CustomerModel()
            {
                HasExpiredInvoices = customer.HasExpiredInvoices,//added [Earstore 2.0]
                CustomerNumber = customer.CustomerNumber,//added
                DefaultAdress = customer.DefaultAdress,//added
                Id = customer.Id,
                StreetAddress = s,//added
                StreetAddress2 = s2,//added
                Email = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest"),
                Username = customer.Username,
                FullName = customer.GetFullName(),
                Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                CustomerRoleNames = GetCustomerRolesNames(customer.CustomerRoles.ToList()),
                Active = customer.Active,
                CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc),
                LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc),
                
            };
        }

        [NonAction]
        protected string ValidateCustomerRoles(IList<CustomerRole> customerRoles)
        {
            if (customerRoles == null)
                throw new ArgumentNullException("customerRoles");

            //ensure a customer is not added to both 'Guests' and 'Registered' customer roles
            //ensure that a customer is in at least one required role ('Guests' and 'Registered')
            bool isInGuestsRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Guests) != null;
            bool isInRegisteredRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Registered) != null;
            if (isInGuestsRole && isInRegisteredRole)
                return "The customer cannot be in both 'Guests' and 'Registered' customer roles";
            if (!isInGuestsRole && !isInRegisteredRole)
                return "Add the customer to 'Guests' or 'Registered' customer role";

            //no errors
            return "";
        }

        [NonAction]
        private void PrepareVendorsModel(CustomerModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AvailableVendors.Add(new SelectListItem()
            {
                Text = _localizationService.GetResource("Admin.Customers.Customers.Fields.Vendor.None"),
                Value = "0"
            });
            var vendors = _vendorService.GetAllVendors(0, int.MaxValue, true);
            foreach (var vendor in vendors)
            {
                model.AvailableVendors.Add(new SelectListItem()
                {
                    Text = vendor.Name,
                    Value = vendor.Id.ToString()
                });
            }
        }

        #endregion

        #region Customers

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        
        public ActionResult UpdateAdminOrderCart(string selectedIds)
        {           
            AdminOrderCart cart = new AdminOrderCart();
            cart.TotalProducts = 4;

            var products = new List<Product>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                products.AddRange(_productService.GetProductsByIds(ids));
            }
           
            return PartialView(cart);
        }          

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            //load registered customers by default
            var defaultRoleIds = new[] {_customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id};
            var listModel = new CustomerListModel()
            {
                UsernamesEnabled = _customerSettings.UsernamesEnabled,
                DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled,
                CompanyEnabled = _customerSettings.CompanyEnabled,
                PhoneEnabled = _customerSettings.PhoneEnabled,
                ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled,
                AvailableCustomerRoles = _customerService.GetAllCustomerRoles(true).Select(cr => cr.ToModel()).ToList(),
                SearchCustomerRoleIds = defaultRoleIds,
            };          
            return View(listModel);
        }

        /// <summary>
        /// Method used for applying search-criterias and filling the customer-list according to these.
        /// The method has been modified to include additional search-parameters needed for Earstore 2.0.
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="command"></param>
        /// <param name="model"></param>
        /// <param name="searchCustomerRoleIds">Chosen customer-roles to include in search. The logic used is ANY of the included roles, not ALL</param>
        /// <returns></returns>
        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult CustomerList(GridCommand command, CustomerListModel model,  
            [ModelBinderAttribute(typeof(CommaSeparatedModelBinder))] int[] searchCustomerRoleIds)
        {
            //we use own own binder for searchCustomerRoleIds property 
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var searchDayOfBirth = 0;
            int searchMonthOfBirth = 0;
            if (!String.IsNullOrWhiteSpace(model.SearchDayOfBirth))
                searchDayOfBirth = Convert.ToInt32(model.SearchDayOfBirth);
            if (!String.IsNullOrWhiteSpace(model.SearchMonthOfBirth))
                searchMonthOfBirth = Convert.ToInt32(model.SearchMonthOfBirth);
            
            var customers = _customerService.GetAllCustomers(
                customerRoleIds: searchCustomerRoleIds,
                email: model.SearchEmail,
                username: model.SearchUsername,
                firstName: model.SearchFirstName,
                lastName: model.SearchLastName,
                dayOfBirth: searchDayOfBirth,
                monthOfBirth: searchMonthOfBirth,
                company: model.SearchCompany,
                phone: model.SearchPhone,
                zipPostalCode: model.SearchZipPostalCode,
                loadOnlyWithShoppingCart: false,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                customerNumber: model.SearchCustomerNumber,//added [Earstore 2.0]
                socialNumber: model.SearchSocialNumber,//added
                searchCity: model.SearchCity,//added
                searchNonActive: model.SearchNonActive,//added
                searchOnlyActive: model.SearchOnlyActive,
                onlyGetDeleted: model.SearchOnlyDeleted); //added
            var gridModel = new GridModel<CustomerModel>
            {
                Data = customers.Select(PrepareCustomerModelForList),
                Total = customers.TotalCount
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }
                               
        public ActionResult Create(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var model = new CustomerModel();
            
            try
            {
                string[] split = id.Split('¤');
                model.FirstName = split[0];
                model.LastName = split[1];
            }
            catch(Exception e) { }

            model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
            model.AllowUsersToChangeUsernames = _customerSettings.AllowUsersToChangeUsernames;
            model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
            foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
                model.AvailableTimeZones.Add(new SelectListItem() { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == _dateTimeHelper.DefaultStoreTimeZone.Id) });
            model.DisplayVatNumber = false;
            //customer roles
            model.AvailableCustomerRoles = _customerService
                .GetAllCustomerRoles(true)
                .Select(cr => cr.ToModel())
                .ToList();
            model.SelectedCustomerRoleIds = new int[0];
            model.AllowManagingCustomerRoles = _permissionService.Authorize(StandardPermissionProvider.ManageCustomerRoles);
            //vendors
            PrepareVendorsModel(model);
            //form fields
            model.GenderEnabled = _customerSettings.GenderEnabled;
            model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
            model.CompanyEnabled = _customerSettings.CompanyEnabled;
            model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
            model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
            model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
            model.CityEnabled = _customerSettings.CityEnabled;
            model.CountryEnabled = _customerSettings.CountryEnabled;
            model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
            model.PhoneEnabled = _customerSettings.PhoneEnabled;
            model.FaxEnabled = _customerSettings.FaxEnabled;

            if (_customerSettings.CountryEnabled)
            {
                model.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
                foreach (var c in _countryService.GetAllCountries())
                {
                    model.AvailableCountries.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString() });
                }

                if (_customerSettings.StateProvinceEnabled)
                {
                    //states
                    var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
                    if (states.Count > 0)
                    {
                        foreach (var s in states)
                            model.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.StateProvinceId) });
                    }
                    else
                        model.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });

                }
            }
            //default value
            model.Active = true;            

           ActionResult a =  Create(model, true);
           return a;
        }

        
        /// <summary>
        /// This method creates a new customer. Workflow has been modified to trigger this method upon entering the customer creation, creating the customer instantly.
        /// A new customer number is generated for the new customer. This is done by searching all existing customers, finding the highest customer-number, and adding 1 to that value. 
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormNameAttribute("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult Create(CustomerModel model, bool continueEditing)
        {

            Stopwatch sw0 = new Stopwatch();
            Stopwatch sw1 = new Stopwatch();
            Stopwatch sw2 = new Stopwatch();


            //------------------<<<<<<<<<<<<<< sw 1 start >>>>>>>>>>>>>>>>------------------------                       

            sw0.Start();
            



            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            if (!String.IsNullOrWhiteSpace(model.Email))
            {
                var cust2 = _customerService.GetCustomerByEmail(model.Email);
                if (cust2 != null)
                    ModelState.AddModelError("", "Email is already registered");
            }
            if (!String.IsNullOrWhiteSpace(model.Username) & _customerSettings.UsernamesEnabled)
            {
                var cust2 = _customerService.GetCustomerByUsername(model.Username);
                if (cust2 != null)
                    ModelState.AddModelError("", "Username is already registered");
            }

            //validate customer roles
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            var newCustomerRoles = new List<CustomerRole>();          
            foreach (var customerRole in allCustomerRoles)
                if ((model.SelectedCustomerRoleIds != null && model.SelectedCustomerRoleIds.Contains(customerRole.Id)) || customerRole.Id == 3)
                    newCustomerRoles.Add(customerRole);           
            var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
            if (!String.IsNullOrEmpty(customerRolesError))
            {
                ModelState.AddModelError("", customerRolesError);
                ErrorNotification(customerRolesError, false);
            }
            bool allowManagingCustomerRoles = _permissionService.Authorize(StandardPermissionProvider.ManageCustomerRoles);

            sw0.Stop();
            sw1.Start();
            if (ModelState.IsValid)
            {
                //---------------------------------New CUSTOMER NUMBER------------------------- [Earstore 2.0]
                /*string nextCustomerNumber;
                int highestCustomerNumber = 0;
                int thisCustomerNumber;
                var customers = _customerService.GetAllCustomers(alsoGetDeleted : true);       
                 
             
                foreach(Customer custom in customers){
                    try{
                            thisCustomerNumber = Int32.Parse(custom.CustomerNumber);
                            if (thisCustomerNumber > highestCustomerNumber)
                            {
                                highestCustomerNumber = thisCustomerNumber;
                            }
                        }catch{
                            //ErrorNotification("Felaktig format på CustomerNumber, Kund ID: " + custom.Id + " " + custom.Username + "Customer Number: " + custom.CustomerNumber);
                        }
                }

                nextCustomerNumber = (highestCustomerNumber + 1) + "";*/

                //---------------------------------New CUSTOMER NUMBER------------------------- [Earstore 2.0]

                string nextCustomerNumber = "";
                try
                {

                    var nextCustomerNumberz = _dbContext.SqlQuery<string>("SELECT TOP 1 CustomerNumber FROM dbo.Customer WHERE CustomerNumber IS NOT NULL ORDER BY CustomerNumber DESC").First();

                    int nextCustomerNumberzz = Int32.Parse(nextCustomerNumberz);
                    nextCustomerNumber = (nextCustomerNumberzz + 1) + "";
                }
                catch (Exception e)
                {
                    _logger.Debug("Something went wrong getting highest customer number in DB");
                }



                            sw1.Stop();
                            sw2.Start();

                var customer = new Customer()           //Customer automatic create [Earstore 2.0]
                {
                    CustomerGuid = Guid.NewGuid(),
                    Email = model.Email,
                    Username = model.Username,
                    VendorId = model.VendorId,
                    AdminComment = model.AdminComment,
                    IsTaxExempt = model.IsTaxExempt,
                    Active = model.Active,
                    CreatedOnUtc = DateTime.UtcNow,
                    LastActivityDateUtc = DateTime.UtcNow,
                    CustomerNumber = nextCustomerNumber,
                    AcceptNewsletters = true
                };             
                _customerService.InsertCustomer(customer);

                //form fields
                /*if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
                if (_customerSettings.GenderEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);*/
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
                /*if (_customerSettings.DateOfBirthEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
                if (_customerSettings.CompanyEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);
                if (_customerSettings.StreetAddressEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
                if (_customerSettings.StreetAddress2Enabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
                if (_customerSettings.ZipPostalCodeEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
                if (_customerSettings.CityEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
                if (_customerSettings.CountryEnabled)*/
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, 73);   //-------Hardcode sweden default     [Earstore 2.0]         
                if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)/*
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
                if (_customerSettings.PhoneEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
                if (_customerSettings.FaxEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);*/


                //password
                if (!String.IsNullOrWhiteSpace(model.Password))
                {
                    var changePassRequest = new ChangePasswordRequest(model.Email, false, _customerSettings.DefaultPasswordFormat, model.Password);
                    var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
                    if (!changePassResult.Success)
                    {
                        foreach (var changePassError in changePassResult.Errors)
                            ErrorNotification(changePassError);
                    }
                }

                //customer roles
                if (allowManagingCustomerRoles)
                {
                    foreach (var customerRole in newCustomerRoles)
                        customer.CustomerRoles.Add(customerRole);
                    _customerService.UpdateCustomer(customer);
                }

                //ensure that a customer with a vendor associated is not in "Administrators" role
                //otherwise, he won't be have access to the other functionality in admin area
                if (customer.IsAdmin() && customer.VendorId > 0)
                {
                    customer.VendorId = 0;
                    _customerService.UpdateCustomer(customer);
                    ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
                }

                //ensure that a customer in the Vendors role has a vendor account associated.
                //otherwise, he will have access to ALL products
                if (customer.IsVendor() && customer.VendorId == 0)
                {
                    var vendorRole = customer
                        .CustomerRoles
                        .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
                    customer.CustomerRoles.Remove(vendorRole);
                    _customerService.UpdateCustomer(customer);
                    ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
                }

                sw2.Stop();

                //activity log
                _customerActivityService.InsertActivity("AddNewCustomer", _localizationService.GetResource("ActivityLog.AddNewCustomer"), customer.Id);

                _logger.Debug("sw0: " + sw0.Elapsed.Milliseconds + "     sw1: " + sw1.Elapsed.Milliseconds + "     sw2: " + sw2.Elapsed.Milliseconds);
                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = customer.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
            model.AllowUsersToChangeUsernames = _customerSettings.AllowUsersToChangeUsernames;
            model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
            foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
                model.AvailableTimeZones.Add(new SelectListItem() { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == model.TimeZoneId) });
            model.DisplayVatNumber = false;
            //customer roles
            model.AvailableCustomerRoles = _customerService
                .GetAllCustomerRoles(true)
                .Select(cr => cr.ToModel())
                .ToList();
            model.AllowManagingCustomerRoles = allowManagingCustomerRoles;
            //vendors
            PrepareVendorsModel(model);
            //form fields            
            model.GenderEnabled = _customerSettings.GenderEnabled;
            model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
            model.CompanyEnabled = _customerSettings.CompanyEnabled;
            model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
            model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
            model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
            model.CityEnabled = _customerSettings.CityEnabled;
            model.CountryEnabled = _customerSettings.CountryEnabled;
            model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
            model.PhoneEnabled = _customerSettings.PhoneEnabled;
            model.FaxEnabled = _customerSettings.FaxEnabled;
            if (_customerSettings.CountryEnabled)
            {
                model.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "3" });
                foreach (var c in _countryService.GetAllCountries())
                {
                    model.AvailableCountries.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == model.CountryId) });
                }


                if (_customerSettings.StateProvinceEnabled)
                {
                    //states
                    var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
                    if (states.Count > 0)
                    {
                        foreach (var s in states)
                            model.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.StateProvinceId) });
                    }
                    else
                        model.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.OtherNonUS"), Value = "0" });

                }
            }
            return View(model);

        }

        public ActionResult EditAndUndelete(int id)
        {
            int Id = id;
            //   ----------------Activate later handleDeletedCustomers-------------------
            //return RedirectToAction("Edit", new { id = Id }); //(Remove)
            //   ----------------Activate later handleDeletedCustomers-------------------

            
            var customer = _customerService.GetCustomerById(id);
            if (customer == null )        
                return RedirectToAction("List");

            if (customer.Deleted == true)
            {
                try
                {
                    customer.Deleted = false;
                    _customerService.UpdateCustomer(customer);
                    SuccessNotification("Successfully recreated customer!");
                }
                catch (Exception e)
                {
                    ErrorNotification("Failed recreating Customer.");
                    return RedirectToAction("List");
                }
            }

            return RedirectToAction("Edit", new { id = Id });
        }


        /// <summary>
        /// See ActionResult Edit(CustomerModel model, bool continueEditing) for description. [Earstore 2.0]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(id);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            var model = new CustomerModel();
            model.Id = customer.Id;
            model.Email = customer.Email;
            model.Username = customer.Username;

            //----------------ADDED FIELDS BELOW---------------- [Earstore 2.0]
            model.SocialNumber = customer.SocialNumber;
            model.CustomerNumber = customer.CustomerNumber;            
            model.DefaultAdress = customer.DefaultAdress;
            model.HasExpiredInvoices = customer.HasExpiredInvoices;
            model.Newsletter = customer.AcceptNewsletters;

            //---------Create the dropdown-list with avaialable addresses to choose for default address----- [Earstore 2.0]
            String a = "";
            String a2 = "";
            String a3 = "";
            int j = 1;

            foreach (Address ad in customer.Addresses)
            {
                if (j == 1)
                {
                    a = ad.Id+"";
                }
                if (j == 2)
                {
                    a2 = ad.Id + "";
                }
                if (j == 3)
                {
                    a3 = ad.Id + "";
                }

                j++;
            }

            if (j == 1)
            {
                model.Items = new[]{
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = "", Text = "Inga adresser tillgängliga" },    
            };
            }

            if (j == 2)
            {
                model.Items = new[]{
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a, Text = a },    
            };
            }
            if (j == 3)
            {
                model.Items = new[]{
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a, Text = a },
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a2, Text = a2 },               
            };
            }
            if (j == 4)
            {
                model.Items = new[]{
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a, Text = a },
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a2, Text = a2 },      
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a3, Text = a3 },
            };
            }
            //---------Create the dropdown-list with avaialable addresses to choose for default address----- [Earstore 2.0]

            //vendors
            model.VendorId = customer.VendorId;
            PrepareVendorsModel(model);
            model.AdminComment = customer.AdminComment;
            model.IsTaxExempt = customer.IsTaxExempt;
            model.Active = customer.Active;
            model.AffiliateId = customer.AffiliateId;
            model.TimeZoneId = customer.GetAttribute<string>(SystemCustomerAttributeNames.TimeZoneId);
            model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
            model.AllowUsersToChangeUsernames = _customerSettings.AllowUsersToChangeUsernames;
            model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
            foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
                model.AvailableTimeZones.Add(new SelectListItem() { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == model.TimeZoneId) });
            model.DisplayVatNumber = _taxSettings.EuVatEnabled;
            model.VatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);
            model.VatNumberStatusNote = ((VatNumberStatus)customer.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId))
                .GetLocalizedEnum(_localizationService, _workContext);
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc);
            model.LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc);
            model.LastIpAddress = customer.LastIpAddress;
            model.LastVisitedPage = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastVisitedPage);
            
            //form fields
            model.FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
            model.LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
            model.Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);
            model.DateOfBirth = customer.GetAttribute<DateTime?>(SystemCustomerAttributeNames.DateOfBirth);
            model.Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
            model.StreetAddress = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress);
            model.StreetAddress2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2);
            model.ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode);
            model.City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City);
            model.CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId);
            model.StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId);
            model.Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
            model.Fax = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax);

            model.GenderEnabled = _customerSettings.GenderEnabled;
            model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
            model.CompanyEnabled = _customerSettings.CompanyEnabled;
            model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
            model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
            model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
            model.CityEnabled = _customerSettings.CityEnabled;
            model.CountryEnabled = _customerSettings.CountryEnabled;
            model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
            model.PhoneEnabled = _customerSettings.PhoneEnabled;
            model.FaxEnabled = _customerSettings.FaxEnabled;

            //countries and states
            if (_customerSettings.CountryEnabled)
            {
                model.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
                foreach (var c in _countryService.GetAllCountries())
                {
                    model.AvailableCountries.Add(new SelectListItem()
                    {
                        Text = c.Name,
                        Value = c.Id.ToString(),
                        Selected = c.Id == model.CountryId
                    });
                }

                if (_customerSettings.StateProvinceEnabled)
                {
                    //states
                    var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
                    if (states.Count > 0)
                    {
                        foreach (var s in states)
                            model.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.StateProvinceId) });
                    }
                    else
                        model.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });

                }
            }

            //customer roles
            model.AvailableCustomerRoles = _customerService
                .GetAllCustomerRoles(true)
                .Select(cr => cr.ToModel())
                .ToList();
            model.SelectedCustomerRoleIds = customer.CustomerRoles.Select(cr => cr.Id).ToArray();
            model.AllowManagingCustomerRoles = _permissionService.Authorize(StandardPermissionProvider.ManageCustomerRoles);
            //reward points gistory
            model.DisplayRewardPointsHistory = _rewardPointsSettings.Enabled;
            model.AddRewardPointsValue = 0;
            model.AddRewardPointsMessage = "Some comment here...";
            //external authentication records
            model.AssociatedExternalAuthRecords = GetAssociatedExternalAuthRecords(customer);                
                       
            return View(model);
        }




        //-------------------Section with methods related to the customized backweb for order-placing START (All code in this section is developed by Recommit)---------------- 
        /// <summary>
        /// This method is called once, the first time the admin enters place-order in backweb. All page-loads after this will be using the UpdateCartAll method.
        /// The main purpose of this method is to receive the customer ID, and set the customer info of the site. 
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="id">Id of the customer and the adress, separated by ","</param>
        /// <returns>The admin place-order view</returns>
        public ActionResult GoToPlaceOrder(string id) //[Earstore 2.0]
        {
            string deliveryAdressID = "";
            string customerID = "";
            PlaceOrderModel model = new PlaceOrderModel();
            try
            {
                string[] array = id.Split(',');
                deliveryAdressID = array[1];
                customerID = array[0];

               

                //   ----------------Activate later handleDeletedCustomers-------------------
                
                Customer c = _customerService.GetCustomerById(Int32.Parse(customerID));
                if (c.Deleted == true)
                {
                    ErrorNotification("Kunden Är borttagen, var god återskapa kunden först innan orderläggning");
                    return RedirectToAction("List");
                }

                if (deliveryAdressID == "" || deliveryAdressID == null || deliveryAdressID == "Borttagen")
                {
                    ErrorNotification("Kunden har ingen giltig Default-adress, kontrollera adress-info innan orderläggning.");
                    return RedirectToAction("Edit/" + customerID);
                }

            }
            catch(Exception e)
            {
                ErrorNotification("KundID samt Adress har inte tagits emot korrekt, Kontakta IT-ansvarig.");                
                return RedirectToAction("List");
            }

            try
            {

                Customer cu = _customerService.GetCustomerById(Int32.Parse(customerID));
                Address a = _addressService.GetAddressById(Int32.Parse(deliveryAdressID));
                Address defaultAdress = _addressService.GetAddressById(Int32.Parse(cu.DefaultAdress));
                
                model.customerID = customerID;

                string adressDeliveryShortDesc = "";
                string adressBillingShortDesc = "";


                //Build adressDeliveryShortDesc string
                adressDeliveryShortDesc = a.FirstName + " " + a.LastName + ", [" + a.Address1 + "], ";
                if (a.Address2 != null && a.Address2 != "")
                {
                    adressDeliveryShortDesc = adressDeliveryShortDesc + " [(Alternativ adress): " + a.Address2 + "], ";
                }
                adressDeliveryShortDesc = adressDeliveryShortDesc + "[" + a.ZipPostalCode + "], [" + a.City + "]";

                //Build adressBillingShortDesc string
                adressBillingShortDesc = defaultAdress.FirstName + " " + defaultAdress.LastName + ", [" + defaultAdress.Address1 + "], ";
                if (defaultAdress.Address2 != null && defaultAdress.Address2 != "")
                {
                    adressBillingShortDesc = adressBillingShortDesc + " [(Alternativ adress): " + defaultAdress.Address2 + "], ";
                }
                adressBillingShortDesc = adressBillingShortDesc + "[" + defaultAdress.ZipPostalCode + "], [" + defaultAdress.City + "]";


                model.adressDelivery = deliveryAdressID;
                model.adressDeliveryShortDesc = adressDeliveryShortDesc;
                model.adressBilling = cu.DefaultAdress;
                model.adressBillingShortDesc = adressBillingShortDesc;
                model.customerFullName = cu.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) + " " + cu.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
                model.customerNumber = cu.CustomerNumber;

                model.quickAddName = "Ingen snabbartikel vald";

                AdminOrderCart cart = new AdminOrderCart();
                model.adminOrderCart = cart;

                model.orderMethod = "Telefon";

                //----------------------PREPARE THE VIEWMODEL DROPDOWNS---------------------------------
                model.DisplayProductPictures = _adminAreaSettings.DisplayProductPictures;
                //a vendor should have access only to his products
                model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

                //categories
                model.AvailableCategories.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
                foreach (var c in _categoryService.GetAllCategories(showHidden: true))
                    model.AvailableCategories.Add(new SelectListItem() { Text = c.GetFormattedBreadCrumb(_categoryService), Value = c.Id.ToString() });

                //manufacturers
                model.AvailableManufacturers.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
                foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                    model.AvailableManufacturers.Add(new SelectListItem() { Text = m.Name, Value = m.Id.ToString() });

                //stores
                model.AvailableStores.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
                foreach (var s in _storeService.GetAllStores())
                    model.AvailableStores.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString() });

                //vendors
                model.AvailableVendors.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
                foreach (var v in _vendorService.GetAllVendors(0, int.MaxValue, true))
                    model.AvailableVendors.Add(new SelectListItem() { Text = v.Name, Value = v.Id.ToString() });

                //product types
                model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
                model.AvailableProductTypes.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
                //----------------------PREPARE THE VIEWMODEL DROPDOWNS--------------------------------


                IList<Discount> allDiscounts = _discountService.GetAllDiscounts(null);
                List<Discount> relevantDiscounts = new List<Discount>();
                foreach (Discount disc in allDiscounts)
                {
                    if (disc.DiscountType.Equals(DiscountType.AssignedToOrderSubTotal) || disc.DiscountType.Equals(DiscountType.AssignedToCategories)) //Get the discounts that should be available
                    {
                        relevantDiscounts.Add(disc);
                    }
                }
                model.DiscountsList = relevantDiscounts;

                //----------Add premiun disc to premium customers----------

                Customer customer = _customerService.GetCustomerById(Int32.Parse(customerID));
                Boolean isPremium = customer.IsInCustomerRole("Premium", true);
                if (isPremium)
                {
                    foreach (Discount disc in model.DiscountsList)
                    {
                        if (disc.Name.ToLower() == "premium")
                        {
                            BackwebDiscount bw = new BackwebDiscount();
                            bw.discount = disc;
                            bw.quantity = 1;
                            model.AddedDiscountsList.Add(bw);
                            SuccessNotification("Premiumrabatt har automatiskt lagts till.");
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ErrorNotification("Fel uppstod vid dirigering till lägg order. kontrollera kundens data (adresser, default-adress etc)");
                return RedirectToAction("List");
            }
            
            //----------Add premiun disc to premium customers----------   

            return View("PlaceOrder", model);  
}

        /// <summary>
        /// This method handles all requests made by the admin. Data is received usinga form. this method then performs the appropiate action depending on many factors of the form.
        /// The GUI of the place-order view for admins is advanced and has many unique needs, and the amount of pageloads must be kept at a minimum. This is solved best by using the form.
        /// This method performs all the actions possible on the admin place-order page.
        /// View the code for details and comments about this methods functionality.
        /// </summary>
        /// <param name="form">The form containing all data of the admin place-order page</param>
        /// <returns>The updated view of the admin place-order page</returns>  
        [HttpPost]
        public ActionResult UpdateCartAll(FormCollection form)
        {  
            AdminOrderCart cart = new AdminOrderCart();
            cart.SubTotal = "0";

            string action = form["whichButtonIsClicked"]; //Which operations to perform is determined by the clicked button.
            if (action == null) { action = ""; }          

            string customerID = form["customerID"];
            string adressBilling = form["adressBilling"];
            string adressBillingShortDesc = form["adressBillingShortDesc"];
            string adressDelivery = form["adressDelivery"];
            string adressDeliveryShortDesc = form["adressDeliveryShortDesc"];
            string adminNote = form["NoteAdminField"];
            string customerNote = form["NoteCustomerField"];
            string customerFullName = form["customerFullName"];           
            string customerNumber = form["customerNumber"];
            string quickAddQuantity = form["quickAddQuantity"];
            string quickAddID = form["quickAddID"];

            //string ordMethod = form["orderMethod"]; TEMP REMOVE ALWAYS TELEFON
           

            PlaceOrderModel model = new PlaceOrderModel(); //Create a new viewModel
            model.customerID = customerID;   
            model.adressBilling = adressBilling;
            model.adressDelivery = adressDelivery;
            model.adressBillingShortDesc = adressBillingShortDesc;
            model.adressDeliveryShortDesc = adressDeliveryShortDesc;
            model.customerNumber = customerNumber;
            model.customerFullName = customerFullName;    
            model.noteAdmin = adminNote;
            model.noteCustomer = customerNote;
            model.quickAddName = "Ingen snabbartikel vald";

            //model.orderMethod = ordMethod; TEMP REMOVE ALWAYS TELEFON
           /* if (action.Equals("Change Ordermethod"))
            {
                SuccessNotification("OrderMetod har ändrats till: " + model.orderMethod);
            }*/

            IList<Discount> allDiscounts = _discountService.GetAllDiscounts(null);
            List<Discount> relevantDiscounts = new List<Discount>();
            foreach (Discount disc in allDiscounts)
            {
                if (disc.DiscountType.Equals(DiscountType.AssignedToOrderSubTotal) || disc.DiscountType.Equals(DiscountType.AssignedToCategories )) //Get the discounts that should be available
                {
                    relevantDiscounts.Add(disc);
                }
            }

            model.DiscountsList = relevantDiscounts;    

            //---------------------------------PLACE LAST ORDER---------------------------------------------
            Boolean hasOrders = false;

            if (action.Equals("Get Latest Order")) 
            {
                Order latestOrder = null;
                var orders = _orderService.SearchOrders(_storeContext.CurrentStore.Id, 0, Int32.Parse(customerID),
            null, null, null, null, null, null, null, 0, int.MaxValue, null, null, null);              

                foreach (var order in orders) //Get the last order
                {
                    hasOrders = true;
                    if (latestOrder == null) { latestOrder = order; }
                    if (order.Id > latestOrder.Id)
                    {
                        latestOrder = order;
                    }
                }

                if (hasOrders)
                {
                    //-------------------------ADD serialSeparated Items to same orderItem row---------------------
                    //Items requiring serialnumbers are split to separate orderItems always using quantity 1 when the order is placed, because each product requires a unique serialnumber.
                    //When being read from an existing order in backweb we need to "unsplit" them to one orderItem and instead increase quantity.
                    bool alreadyExistInCart = false;

                    foreach (OrderItem orderItem in latestOrder.OrderItems)
                    {
                        alreadyExistInCart = false;
                        int prodId = orderItem.ProductId;
                        foreach (AdminOrderCart.AdminOrderCartItemModel cartItem in cart.Items)
                        {
                            if (cartItem.Id == prodId)
                            {
                                cartItem.Quantity = cartItem.Quantity + 1;
                                alreadyExistInCart = true;
                            }
                        }
                       
                        if (alreadyExistInCart == false)
                        {
                            Nop.Admin.Models.ShoppingCart.AdminOrderCart.AdminOrderCartItemModel item = new Nop.Admin.Models.ShoppingCart.AdminOrderCart.AdminOrderCartItemModel();
                            item.Id = orderItem.ProductId;
                            item.ProductId = orderItem.ProductId;
                            //item.UnitPrice = Convert.ToInt32(orderItem.UnitPriceInclTax) + "";
                            item.UnitPrice = Convert.ToDouble(orderItem.UnitPriceInclTax) + "";
                            item.Quantity = orderItem.Quantity;
                            item.ProductName = orderItem.Product.Name;
                            item.TotalPriceWithQuantity = double.Parse(item.UnitPrice) * item.Quantity + ""; //Calculate TotalPriceWithQuantity
                            cart.Items.Add(item);
                        }
                        //-------------------------ADD serialSeparated Items to same orderItem row---------------------
                    }
                    SuccessNotification("Varor från senaste order har hämtats.");

                    if (model.adressDelivery != latestOrder.ShippingAddress.Id + "")
                    {
                        ErrorNotification("Nuvarande adress är ej den som användes vid förra ordern.");
                    }

                    //---------------------------------POSSIBLE FUNCTION TO BE USED LATER-------------------------------------
                    //This code can be uncommented to enable setting the place-order address to the one used in the last order.
                    //If this code is not enabled, the admin will ONLY be notified if the address differs

                    //Address a = _addressService.GetAddressById(latestOrder.ShippingAddress.Id);

                    //string newAdressDeliveryShortDesc = "";
                    ////---------------------------------------------------------------------------
                    ////Build newAdressDeliveryShortDesc string
                    //newAdressDeliveryShortDesc = a.FirstName + " " + a.LastName + ", [" + a.Address1 + "], ";
                    //if (a.Address2 != null && a.Address2 != "")
                    //{
                    //    newAdressDeliveryShortDesc = newAdressDeliveryShortDesc + " [(Alternativ adress): " + a.Address2 + "], ";
                    //}
                    //newAdressDeliveryShortDesc = newAdressDeliveryShortDesc + "[" + a.ZipPostalCode + "], [" + a.City + "]";

                    //model.adressDeliveryShortDesc = newAdressDeliveryShortDesc;
                    //model.adressDelivery = latestOrder.ShippingAddress.Id + "";
                    //SuccessNotification("Leveransadress har satts till samma som senaste order.");

                    //---------------------------------POSSIBLE FUNCTION TO BE USED LATER-------------------------------------
                }
                else
                {
                    ErrorNotification("Kunden har inga tidigare ordrar.");
                }
            }
            if (action.Equals("Get Latest Order") == false || hasOrders == false)   //---------------------------------PLACE LAST ORDER---------------------------------------------
            {       //-------------------DO NOT GET OLD CART ITEMS IF PLACE LAST ORDER---------------------------

                //------------------------------------SECTION UPDATE CART--------------------------            
                string value = null;
                string id = null;
                var allIdsToRemove = form["removefromcart"] != null ? form["removefromcart"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();
                var allIdsToDeactivateDefaultPrice = form["DeactivateAutoCalculatePrice"] != null ? form["DeactivateAutoCalculatePrice"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();
                var allDiscountsToRemove = form["removeDiscountCheckbox"] != null ? form["removeDiscountCheckbox"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();
                
                //------------Get the existing items in cart and add to the new cart------------
                try
                {
                    foreach (string formKey in form.AllKeys)
                    {
                        value = form.Get(formKey);
                        if (value != null)
                        {
                            if (formKey.Contains("itemunitprice") == true)
                            {
                                id = formKey.Substring(13);
                                bool remove = allIdsToRemove.Contains(Int32.Parse(id));
                                if (remove == false) //If remove we just simply don't add the product to the cart.
                                {
                                    Nop.Admin.Models.ShoppingCart.AdminOrderCart.AdminOrderCartItemModel itemz = new Nop.Admin.Models.ShoppingCart.AdminOrderCart.AdminOrderCartItemModel();

                                    if (allIdsToDeactivateDefaultPrice.Contains(Int32.Parse(id))){
                                        itemz.DeactivateAutoCalculatePrice = true;
                                    }

                                    itemz.Id = Int32.Parse(id);
                                    itemz.ProductId = Int32.Parse(id);
                                    itemz.UnitPrice = form.Get("itemunitprice" + id);
                                    itemz.Quantity = Int32.Parse(form.Get("itemquantity" + id));
                                    itemz.TotalPriceWithQuantity = double.Parse(itemz.UnitPrice) * itemz.Quantity + ""; //Calculate TotalPriceWithQuantity
                                    itemz.ProductName = form.Get("productName" + id);
                                    cart.Items.Add(itemz);
                                }
                            }
                            if (formKey.Contains("discountId") == true)
                            {
                                id = formKey.Substring(10);
                                bool remove = allDiscountsToRemove.Contains(Int32.Parse(id));
                                if (remove == false) //If remove we just simply don't add the discount to the cart.
                                {
                                   Discount d = _discountService.GetDiscountById(Int32.Parse(id));
                                   BackwebDiscount bw = new BackwebDiscount();
                                   bw.discount = d;
                                   bw.quantity = Int32.Parse(form.Get("discountQuantity" + id));
                                   model.AddedDiscountsList.Add(bw);
                                }
                            }
                        }
                    }
                    SuccessNotification("Varukorgen har uppdaterats");
                }
                catch
                {
                    ErrorNotification("Det gick inte att uppdatera varukorgen, kontakta IT-ansvarig");
                    Exception e;
                }
                model.adminOrderCart = cart; //Add the cart to the viewModel
                //------------------------------------SECTION UPDATE CART--------------------------

            }

            //------------------------------------SECTION ADD NEW PRODUCTS--------------------------
            if (action.Equals("Add Items") == true || action.Equals("Quick Add Items") == true){            
                try
                {
                    //-----------------------------Get product-models using the ID:s-------------------------
                    var products = new List<Product>();


                    if (action.Equals("Add Items") == true){ //-------------If regular add                              
                        if (form.Get("NewOrderItemIds") != null)
                            {
                                var ids = form.Get("NewOrderItemIds")
                                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(x => Convert.ToInt32(x))
                                    .ToArray();
                                products.AddRange(_productService.GetProductsByIds(ids));
                            }                     
                    }

                    if (action.Equals("Quick Add Items") == true)  { //-------------If quick add                           
                            int i = 0;
                            while(i < Int32.Parse(quickAddQuantity)){
                                products.Add(_productService.GetProductById(Int32.Parse(quickAddID)));
                                i++;
                            }   
                 }
                    //---------Convert newly added products to adminCartItemModels and add to cart-------------
                   bool alreadyExistInCart = false;
                    foreach (Product p in products)
                    {
                        alreadyExistInCart = false;

                        //----------If product with this ID already exists, only increase quantity------
                        int idz = p.Id;
                        
                            foreach (AdminOrderCart.AdminOrderCartItemModel cartItem in cart.Items)
                            {
                                if (cartItem.Id == idz)
                                {
                                    cartItem.Quantity = cartItem.Quantity + 1;
                                    alreadyExistInCart = true;
                                }
                            }
                        //}
                       // ----------If product with this ID already exists, only increase quantity------
                        
                        if (alreadyExistInCart == false)
                        {
                            Nop.Admin.Models.ShoppingCart.AdminOrderCart.AdminOrderCartItemModel item = new Nop.Admin.Models.ShoppingCart.AdminOrderCart.AdminOrderCartItemModel();
                            item.Id = p.Id;
                            item.ProductId = p.Id;
                            //item.UnitPrice = Convert.ToInt32(p.Price).ToString();
                            item.UnitPrice = Convert.ToDouble(p.Price).ToString();
                            item.Quantity = 1; //-----------------Default 1---------------------
                            item.ProductName = p.Name;
                            item.TotalPriceWithQuantity = double.Parse(item.UnitPrice) * item.Quantity + ""; //Calculate TotalPriceWithQuantity
                            cart.Items.Add(item);
                        }
                    }
                    SuccessNotification("Varor har lagts till i varukorgen");
                }
                catch
                {
                    ErrorNotification("Det gick inte att lägga till varor korrekt, kontakta IT-ansvarig");
                    Exception e;
                }
            }
             model.adminOrderCart = cart; //Add the cart to the viewModel
             //------------------------------------SECTION ADD NEW PRODUCTS--------------------------

            //--------------------------------------HANDLE TIER PRICES-------------------------------------
            foreach(AdminOrderCart.AdminOrderCartItemModel cartItem in cart.Items){

                if (cartItem.DeactivateAutoCalculatePrice == true){ //Admin must have checked this box to be able to set a custom price to the product.
                    //cart.SubTotal = Double.Parse(cart.SubTotal) + Double.Parse(cartItem.TotalPriceWithQuantity) + "";
                    cart.SubTotal = Double.Parse(cart.SubTotal) + (Double.Parse(cartItem.UnitPrice))*cartItem.Quantity + "";
                
                }
                else //If not checked, use NC method to calculate price (includes tier-price discounts)
                {
                    decimal newUnitPrice = 0;
                    Product p = _productService.GetProductById(cartItem.ProductId);
                    Customer c = _customerService.GetCustomerById(Int32.Parse(customerID));
                 
                        newUnitPrice = _priceCalculationService.GetFinalPrice(p, c, 0, false, cartItem.Quantity);
                        cartItem.UnitPrice = ((Double)newUnitPrice) + "";
                        cartItem.TotalPriceWithQuantity = Double.Parse(cartItem.UnitPrice) * cartItem.Quantity + "";

                        cart.SubTotal = Double.Parse(cart.SubTotal) + Double.Parse(cartItem.TotalPriceWithQuantity) + "";       
                
                }
                //{
                //    decimal newUnitPrice = 0;
                //    Product p = _productService.GetProductById(cartItem.ProductId);
                //    Customer c = _customerService.GetCustomerById(Int32.Parse(customerID));

                //    newUnitPrice = _priceCalculationService.GetFinalPrice(p, c, 0, false, cartItem.Quantity);
                //    cartItem.UnitPrice = ((int)newUnitPrice) + "";
                //    cartItem.TotalPriceWithQuantity = double.Parse(cartItem.UnitPrice) * cartItem.Quantity + "";

                //    cart.SubTotal = Int32.Parse(cart.SubTotal) + Int32.Parse(cartItem.TotalPriceWithQuantity) + "";

                //}
            }      

            //--------------------------------------Handle Discounts -------------------------------------
            if (action.Equals("Add Discount") == true)
            {
                String addedDiscount = form["selectedDiscount"];
                Discount theAddedDiscount;
                bool alreadyExistInCart = false;
                if (addedDiscount != "Ingen")
                {
                    theAddedDiscount = _discountService.GetDiscountById(Int32.Parse(addedDiscount));

                    alreadyExistInCart = false;

                    int idz = theAddedDiscount.Id;
                    
                    foreach (BackwebDiscount bw in model.AddedDiscountsList)
                    {
                        if (bw.discount.Id == idz)
                        {
                            bw.quantity = bw.quantity + 1;
                            alreadyExistInCart = true;
                        }
                    }

                    if (alreadyExistInCart == false)
                    {
                        BackwebDiscount bw = new BackwebDiscount();
                        bw.discount = theAddedDiscount;
                        bw.quantity = 1;
                        model.AddedDiscountsList.Add(bw);
                    }
                }
            }

            //----------------------------CALCULATE DISCOUNTS TO THE CART----------------------------
            //There is no logic to determine how the discounts should be applied, percentage before or after set amount discounts etc.
            //----------------------------INSERT LOGIC----------------------------
            foreach (BackwebDiscount disc in model.AddedDiscountsList)
            {
                int i = 0;
                while (disc.quantity > i)
                {
                    if(disc.discount.DiscountType.Equals(DiscountType.AssignedToCategories)){

                        foreach (AdminOrderCart.AdminOrderCartItemModel item in cart.Items)
                        {
                            Product p = _productService.GetProductById(item.ProductId);

                            foreach (ProductCategory category in p.ProductCategories)
                            {

                                if (disc.discount.AppliedToCategories.Contains(category.Category))
                                {                                
                                    item.TotalPriceWithQuantity = (Double)(Decimal.Parse(item.TotalPriceWithQuantity)) - (Double)(item.Quantity * disc.discount.DiscountAmount) + "";
                                    item.UnitPrice = (Double)(Decimal.Parse(item.UnitPrice)) - (Double)(disc.discount.DiscountAmount) + "";
                                    cart.SubTotal = (Double)(Decimal.Parse(cart.SubTotal)) - (Double)(item.Quantity * disc.discount.DiscountAmount) + "";

                                    disc.nrOfTimesApplied = disc.nrOfTimesApplied + item.Quantity;
                                }
                            }                            
                        }
                    }
                    else{                   


                        if (disc.discount.UsePercentage == true)
                        {
                            cart.SubTotal = (Double)(Decimal.Parse(cart.SubTotal) - (Decimal.Parse(cart.SubTotal) * disc.discount.DiscountPercentage) / 100) + "";
                        }
                        else
                        {
                            cart.SubTotal = (Double)(Decimal.Parse(cart.SubTotal) - disc.discount.DiscountAmount) + "";
                        }
                   
                    }
             i++;
                }
            }

           //--------------------------------------Handle Discounts -------------------------------------
            


            //------------------------------------SECTION PLACE ORDER--------------------------------------------
             if (action.Equals("Place Order") == true)
                 {              
                var order = new Order();
                Boolean overrideThisStatus = false;
                Guid g = Guid.NewGuid();
                // order.OrderMethod = model.orderMethod; TEMP REMOVE ALWAYS TELEFON
                order.OrderMethod = "Telefon";
                order.OrderGuid = g;
                order.CustomerId = Int32.Parse(customerID); 
                order.StoreId = 1;
                order.BillingAddressId = Int32.Parse(adressBilling);
                order.ShippingAddressId = Int32.Parse(adressDelivery);
                order.ShippingStatusId = 20;
                order.PaymentStatusId = 10;
                order.OrderStatusId = 10;
                order.PaymentMethodSystemName = "Payments.CashOnDelivery";
                order.CustomerCurrencyCode = "SEK";
                order.CreatedOnUtc = DateTime.Now.ToUniversalTime();  

                decimal orderTotalCost = 0;

                foreach (Nop.Admin.Models.ShoppingCart.AdminOrderCart.AdminOrderCartItemModel item in model.adminOrderCart.Items)
                {
                    Product p = _productService.GetProductById(item.Id);

                    //---------------------HANDLE CUSTOMIZEDSTATUS----------------------------
                    if (p.StockQuantity - item.Quantity< 0)
                    {
                        order.CustomizedStatus = "RESTNOTERAD";                       
                        overrideThisStatus = true;
                    }
                    //---------------------HANDLE CUSTOMIZEDSTATUS----------------------------

                    if (p.isRequireSerialNumber == false)//-------IF NOTREQUIRESERIALNUMBER WE CAN ADD MULTIPLE QUANTITY--------------
                    {
                        order.OrderItems.Add(new OrderItem()
                        {
                            ProductId = item.Id,
                            PriceInclTax = decimal.Parse(item.UnitPrice) * item.Quantity,
                            PriceExclTax = decimal.Parse(item.UnitPrice) * item.Quantity,
                            UnitPriceInclTax = decimal.Parse(item.UnitPrice),
                            UnitPriceExclTax = decimal.Parse(item.UnitPrice),
                            Quantity = item.Quantity
                        });
                        orderTotalCost = orderTotalCost + (decimal.Parse(item.UnitPrice) * item.Quantity);
                    }
                    else //--------IF ISREQUIRESERIALNUMBER WE CAN NOT ADD MULTIPLE QUANTITY, SEPARATE TO DIFFERENT CART-ITEMS------
                    {
                        //---------------------HANDLE CUSTOMIZEDSTATUS----------------------------
                        if (overrideThisStatus == false)
                        {
                            order.CustomizedStatus = "SERIENUMMER";
                            //_orderService.UpdateOrder(order);
                        }
                        //---------------------HANDLE CUSTOMIZEDSTATUS----------------------------

                        int i = 0;
                        while (i < item.Quantity)
                        {
                    
                            order.OrderItems.Add(new OrderItem()
                            {
                                ProductId = item.Id,
                                PriceInclTax = decimal.Parse(item.UnitPrice),
                                PriceExclTax = decimal.Parse(item.UnitPrice),
                                UnitPriceInclTax = decimal.Parse(item.UnitPrice),
                                UnitPriceExclTax = decimal.Parse(item.UnitPrice),
                                Quantity = 1,
                                SerialNumber = "Inväntar Serienummer"
                            });
                            orderTotalCost = orderTotalCost + (decimal.Parse(item.UnitPrice));
                            i++;
                        }
                    }
                }


                if (order.CustomizedStatus == null)
                {
                    order.CustomizedStatus = "BESTÄLLD";
                }


                order.ShippingMethod = "Posten Brev (kostnadsfritt)"; //-----------Needs to be fixed----------

                decimal priceWithoutDiscount = orderTotalCost;
                decimal categoryDiscAmount = 0;

                 //------------------------ADD  DISCOUNT TO TOTAL PRICE------------------
                foreach (BackwebDiscount disc in model.AddedDiscountsList)                  
                {
                    int i = 0;
                    while (disc.quantity > i)
                    {
                        if (disc.discount.DiscountType.Equals(DiscountType.AssignedToCategories))
                        {
                            //categoryDiscAmount = categoryDiscAmount + disc.discount.DiscountAmount * disc.nrOfTimesApplied;                      
                            //category wide discounts has already been applied to each row, 
                        }
                        else
                        {
                            if (disc.discount.UsePercentage == true)
                            {
                                orderTotalCost = (decimal)orderTotalCost - (orderTotalCost * disc.discount.DiscountPercentage) / 100;
                            }
                            else
                            {
                                orderTotalCost = (decimal)orderTotalCost - disc.discount.DiscountAmount;
                            }
                        }
                       
                        i++;
                    }
                }
                //------------------------ADD  DISCOUNT TO TOTAL PRICE------------------


                 //-----------------------This section needs to be controlled, make sure tax-amounts is right-------------------
                decimal totalDiscount = priceWithoutDiscount - orderTotalCost;     

                order.OrderSubTotalDiscountInclTax = totalDiscount;

                order.OrderTotal = orderTotalCost;
                order.OrderSubtotalInclTax = orderTotalCost;
                order.OrderSubtotalExclTax = orderTotalCost;

                order.CurrencyRate = 1;
                order.CustomerTaxDisplayTypeId = 10;
                order.TaxRates = "0:0;";
                //-----------------------This section needs to be controlled, make sure tax-amounts is right-------------------

                                  //-------------------------ORDERNOTES----------------------------
                if (model.noteAdmin != null && model.noteAdmin != "")
                {

                    var orderNoteAdmin = new OrderNote() //This note is added as a regular note, not visible to customer
                    {
                        DisplayToCustomer = false,
                        Note = model.noteAdmin,
                        CreatedOnUtc = DateTime.UtcNow,
                    };
                    order.OrderNotes.Add(orderNoteAdmin);

                }

                if (model.noteCustomer != null && model.noteCustomer != "") //This note is added as a regular note, and as TheOrderNote, which is a new 1:1 comment on the order, unlike the regular note which is 1:n.
                {

                    var orderNoteCustomer = new OrderNote()
                    {
                        DisplayToCustomer = true,
                        Note = model.noteCustomer,
                        CreatedOnUtc = DateTime.UtcNow,
                    };
                    order.OrderNotes.Add(orderNoteCustomer);
                    order.TheOrderNote = model.noteCustomer;
                }                 

                 //------------------Discount history-------------------
                 foreach(BackwebDiscount bw in model.AddedDiscountsList){

                     int i = 0;
                     while (i < bw.quantity)
                     {
                         DiscountUsageHistory duh = new DiscountUsageHistory();
                         
                         duh.CreatedOnUtc = DateTime.UtcNow;                         
                         duh.Discount = bw.discount;
                         duh.DiscountId = bw.discount.Id;
                         duh.Discount.DiscountAmount = duh.Discount.DiscountAmount;
                         duh.Order = order;
                         duh.OrderId = order.Id;
                         order.DiscountUsageHistory.Add(duh);
                         i++;
                     }
                 }
                 //------------------Discount history-------------------

             

                    _orderService.InsertOrder(order);      //--------------- Insert Order in NopCommerce.----------------
                    SuccessNotification("Ordern har skapats");
                        
                 Product pr;

                 foreach(OrderItem item in order.OrderItems){  //------------------------------Decrease stock-------------------------
                     pr = _productService.GetProductById(item.ProductId);
                     _productService.AdjustInventory(pr, true, item.Quantity, "");
                 }   
                return RedirectToAction("List");
             }

            //----------------------PREPARE THE VIEWMODEL DROPDOWNS---------------------------------
            model.DisplayProductPictures = _adminAreaSettings.DisplayProductPictures;
            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            //categories
            model.AvailableCategories.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var c in _categoryService.GetAllCategories(showHidden: true))
                model.AvailableCategories.Add(new SelectListItem() { Text = c.GetFormattedBreadCrumb(_categoryService), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem() { Text = m.Name, Value = m.Id.ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(0, int.MaxValue, true))
                model.AvailableVendors.Add(new SelectListItem() { Text = v.Name, Value = v.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            //----------------------PREPARE THE VIEWMODEL DROPDOWNS--------------------------------

            return View("PlaceOrder", model); 
        }
        //-------------------Section with methods related to the customized backweb for order-placing END----------------                           

        /// <summary>
        /// This method is used to update information on a customer. It has been edited to include the new fields used in Earstore 2.0.
        /// To be able to choose a default address, we must populate a dropdown list, using the addresses bound to the customer.
        /// Additionally, functionality to creating a new address has been added. If a customer is saved, with enough fields to create a new address, and a default-adress does not exist, it will be created automatically.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormNameAttribute("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult Edit(CustomerModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            //validate customer roles
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            var newCustomerRoles = new List<CustomerRole>();
            foreach (var customerRole in allCustomerRoles)
                if (model.SelectedCustomerRoleIds != null && model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                    newCustomerRoles.Add(customerRole);
            var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
            if (!String.IsNullOrEmpty(customerRolesError))
            {
                ModelState.AddModelError("", customerRolesError);
                ErrorNotification(customerRolesError, false);
            }
            bool allowManagingCustomerRoles = _permissionService.Authorize(StandardPermissionProvider.ManageCustomerRoles);
            
            if (ModelState.IsValid)
            {
                try
                {
                    //---------------------------EDITED--------------------------------------------- [Earstore 2.0]
                    customer.SocialNumber = model.SocialNumber;//lookhere socialnumber
                    customer.CustomerNumber = model.CustomerNumber;
                    customer.DefaultAdress = model.DefaultAdress;
                    customer.HasExpiredInvoices = model.HasExpiredInvoices;
                    customer.AcceptNewsletters = model.Newsletter;

                    //---------Create the dropdown-list with avaialable addresses to choose for default address START----- [Earstore 2.0]

                    String a = "";
                    String a2 = "";
                    String a3 = "";
                    int j = 1;

                    foreach (Address ad in customer.Addresses)
                    {
                        if (j == 1)
                        {
                            a = ad.Id + "";
                        }
                        if (j == 2)
                        {
                            a2 = ad.Id + "";
                        }
                        if (j == 3)
                        {
                            a3 = ad.Id + "";
                        }

                        j++;
                    }

                    if (j == 1)
                    {
                        model.Items = new[]{
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = "", Text = "Inga adresser tillgängliga" },    
            };
                    }

                    if (j == 2)
                    {
                        model.Items = new[]{
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a, Text = a },    
            };
                    }
                    if (j == 3)
                    {
                        model.Items = new[]{
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a, Text = a },
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a2, Text = a2 },               
            };
                    }
                    if (j == 4)
                    {
                        model.Items = new[]{
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a, Text = a },
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a2, Text = a2 },      
                new Nop.Admin.Models.Customers.CustomerModel.Item { Value = a3, Text = a3 },
            };
                    }
                    //---------Create the dropdown-list with avaialable addresses to choose for default address END-----   [Earstore 2.0]
                    
                    
                    customer.AdminComment = model.AdminComment;
                    customer.IsTaxExempt = model.IsTaxExempt;
                    customer.Active = model.Active;
                    //email
                    if (!String.IsNullOrWhiteSpace(model.Email))
                    {
                        _customerRegistrationService.SetEmail(customer, model.Email);
                    }
                    else
                    {
                        customer.Email = model.Email;
                    }

                    //username
                    if (_customerSettings.UsernamesEnabled && _customerSettings.AllowUsersToChangeUsernames)
                    {
                        if (!String.IsNullOrWhiteSpace(model.Username))
                        {
                            _customerRegistrationService.SetUsername(customer, model.Username);
                        }
                        else
                        {
                            customer.Username = model.Username;
                        }
                    }

                    //VAT number
                    if (_taxSettings.EuVatEnabled)
                    {
                        string prevVatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);

                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumber, model.VatNumber);
                        //set VAT number status
                        if (!String.IsNullOrEmpty(model.VatNumber))
                        {
                            if (!model.VatNumber.Equals(prevVatNumber, StringComparison.InvariantCultureIgnoreCase))
                            {
                                _genericAttributeService.SaveAttribute(customer, 
                                    SystemCustomerAttributeNames.VatNumberStatusId, 
                                    (int)_taxService.GetVatNumberStatus(model.VatNumber));
                            }
                        }
                        else
                        {
                            _genericAttributeService.SaveAttribute(customer,
                                SystemCustomerAttributeNames.VatNumberStatusId, 
                                (int)VatNumberStatus.Empty);
                        }
                    }

                    //vendor
                    customer.VendorId = model.VendorId;

                    //form fields
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
                    if (_customerSettings.GenderEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
                    if (_customerSettings.DateOfBirthEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
                    if (_customerSettings.FaxEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);


                                       // SECTION ADD UPDATED DATA TO DEFAULT ADDRESS ADMIN
                    foreach (Address adr in customer.Addresses)
                    {
                        if (adr.Id == Int32.Parse(customer.DefaultAdress))
                        {
                            adr.FirstName = model.FirstName;
                            adr.LastName = model.LastName;
                            adr.Email = model.Email; 
                            if (_customerSettings.CompanyEnabled)
                            {
                                adr.Company = model.Company;
                            }

                            if (_customerSettings.CountryEnabled)
                            {
                                adr.CountryId = model.CountryId;
                            }

                            if (_customerSettings.CityEnabled)
                            {
                                adr.City = model.City;
                            }

                            if (_customerSettings.StreetAddressEnabled)
                            {
                                adr.Address1 = model.StreetAddress;
                            }

                            if (_customerSettings.StreetAddress2Enabled)
                            {
                                adr.Address2 = model.StreetAddress2;
                            }

                            if (_customerSettings.ZipPostalCodeEnabled)
                            {
                                adr.ZipPostalCode = model.ZipPostalCode;
                            }
                            if (_customerSettings.PhoneEnabled)
                            {
                                adr.PhoneNumber = model.Phone;
                            }
                            
                        }
                    }


                    //customer roles
                    if (allowManagingCustomerRoles)
                    {
                        foreach (var customerRole in allCustomerRoles)
                        {
                            if (model.SelectedCustomerRoleIds != null && model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                            {
                                //new role
                                if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) == 0)
                                    customer.CustomerRoles.Add(customerRole);
                            }
                            else
                            {
                                //removed role
                                if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) > 0)
                                    customer.CustomerRoles.Remove(customerRole);
                            }
                        }
                        _customerService.UpdateCustomer(customer);
                    }

                    //ensure that a customer with a vendor associated is not in "Administrators" role
                    //otherwise, he won't be have access to the other functionality in admin area
                    if (customer.IsAdmin() && customer.VendorId > 0)
                    {
                        customer.VendorId = 0;
                        _customerService.UpdateCustomer(customer);
                        ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
                    }

                    //ensure that a customer in the Vendors role has a vendor account associated.
                    //otherwise, he will have access to ALL products
                    if (customer.IsVendor() && customer.VendorId == 0)
                    {
                        var vendorRole = customer
                            .CustomerRoles
                            .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
                        customer.CustomerRoles.Remove(vendorRole);
                        _customerService.UpdateCustomer(customer);
                        ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
                    }

                    if (customer.Addresses.Count == 0)  //----Only create if there is no existing adresses---- [Earstore 2.0]
                    {
                        //------------------------------EDITED INSERT DEFAULT ADRESS CREATION--------------- [Earstore 2.0]
                        var defaultAddress = new Address()
                        {
                            FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                            LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                            Email = customer.Email,
                            Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                            CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                                (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                            StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                                (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                            City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City),
                            Address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
                            Address2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
                            ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                            PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                            FaxNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
                            CreatedOnUtc = customer.CreatedOnUtc
                        };

                        if (this._addressService.IsAddressValidWithoutEmail(defaultAddress))
                        {
                            //some validation
                            if (defaultAddress.CountryId == 0)
                                defaultAddress.CountryId = null;
                            if (defaultAddress.StateProvinceId == 0)
                                defaultAddress.StateProvinceId = null;
                            //set default address
                            customer.Addresses.Add(defaultAddress);
                            customer.BillingAddress = defaultAddress;
                            customer.ShippingAddress = defaultAddress;                         

                            _customerService.UpdateCustomer(customer);

                            var customerWithAdressId = _customerService.GetCustomerById(model.Id);

                            foreach (Address add in customerWithAdressId.Addresses)
                            {
                                customer.DefaultAdress = add.Id + ""; //Also set the flag indicating this is the customers default address [Earstore 2.0]
                                
                            }                 

                            _customerService.UpdateCustomer(customer);
                            SuccessNotification("Default address har skapats automatiskt");
                        }
                    }

                    //------------------------------EDITED INSERT DEFAULT ADRESS CREATION---------------    [Earstore 2.0]

                    //activity log
                    _customerActivityService.InsertActivity("EditCustomer", _localizationService.GetResource("ActivityLog.EditCustomer"), customer.Id);

                    SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Updated"));
                    return continueEditing ? RedirectToAction("Edit", customer.Id) : RedirectToAction("List");
                }
                catch (Exception exc)
                {
                    ErrorNotification(exc.Message, false);
                }
            }

            //If we got this far, something failed, redisplay form
            model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
            model.AllowUsersToChangeUsernames = _customerSettings.AllowUsersToChangeUsernames;
            model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
            foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
                model.AvailableTimeZones.Add(new SelectListItem() { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == model.TimeZoneId) });
            model.DisplayVatNumber = _taxSettings.EuVatEnabled;
            model.VatNumberStatusNote = ((VatNumberStatus)customer.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId))
                .GetLocalizedEnum(_localizationService, _workContext);
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc);
            model.LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc);
            model.LastIpAddress = model.LastIpAddress;
            model.LastVisitedPage = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastVisitedPage);
            //vendors
            PrepareVendorsModel(model);
            //form fields
            model.GenderEnabled = _customerSettings.GenderEnabled;
            model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
            model.CompanyEnabled = _customerSettings.CompanyEnabled;
            model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
            model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
            model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
            model.CityEnabled = _customerSettings.CityEnabled;
            model.CountryEnabled = _customerSettings.CountryEnabled;
            model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
            model.PhoneEnabled = _customerSettings.PhoneEnabled;
            model.FaxEnabled = _customerSettings.FaxEnabled;
            //countries and states
            if (_customerSettings.CountryEnabled)
            {
                model.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
                foreach (var c in _countryService.GetAllCountries())
                {
                    model.AvailableCountries.Add(new SelectListItem()
                    {
                        Text = c.Name,
                        Value = c.Id.ToString(),
                        Selected = c.Id == model.CountryId
                    });
                }

                if (_customerSettings.StateProvinceEnabled)
                {
                    //states
                    var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
                    if (states.Count > 0)
                    {
                        foreach (var s in states)
                            model.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.StateProvinceId) });
                    }
                    else
                        model.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });

                }
            }
            //customer roles
            model.AvailableCustomerRoles = _customerService
                .GetAllCustomerRoles(true)
                .Select(cr => cr.ToModel())
                .ToList();
            model.AllowManagingCustomerRoles = allowManagingCustomerRoles;
            //reward points gistory
            model.DisplayRewardPointsHistory = _rewardPointsSettings.Enabled;
            model.AddRewardPointsValue = 0;
            model.AddRewardPointsMessage = "Some comment here...";
            //external authentication records
            model.AssociatedExternalAuthRecords = GetAssociatedExternalAuthRecords(customer);
            return View(model);
        }
        
        [HttpPost, ActionName("Edit")]
        [FormValueRequired("changepassword")]
        public ActionResult ChangePassword(CustomerModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                var changePassRequest = new ChangePasswordRequest(model.Email,
                    false, _customerSettings.DefaultPasswordFormat, model.Password);
                var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
                if (changePassResult.Success)
                    SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.PasswordChanged"));
                else
                    foreach (var error in changePassResult.Errors)
                        ErrorNotification(error);
            }

            return RedirectToAction("Edit", customer.Id);
        }
        
        [HttpPost, ActionName("Edit")]
        [FormValueRequired("markVatNumberAsValid")]
        public ActionResult MarkVatNumberAsValid(CustomerModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            _genericAttributeService.SaveAttribute(customer, 
                SystemCustomerAttributeNames.VatNumberStatusId,
                (int)VatNumberStatus.Valid);

            return RedirectToAction("Edit", customer.Id);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("markVatNumberAsInvalid")]
        public ActionResult MarkVatNumberAsInvalid(CustomerModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            _genericAttributeService.SaveAttribute(customer,
                SystemCustomerAttributeNames.VatNumberStatusId,
                (int)VatNumberStatus.Invalid);
            
            return RedirectToAction("Edit", customer.Id);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            try
            {
                _customerService.DeleteCustomer(customer);

                //remove newsletter subscription (if exists)
                var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmail(customer.Email);
                if (subscription != null)
                    _newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);

                //activity log
                _customerActivityService.InsertActivity("DeleteCustomer", _localizationService.GetResource("ActivityLog.DeleteCustomer"), customer.Id);

                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
                return RedirectToAction("Edit", new { id = customer.Id });
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("impersonate")]
        public ActionResult Impersonate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AllowCustomerImpersonation))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            _genericAttributeService.SaveAttribute<int?>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.ImpersonatedCustomerId, customer.Id);

            return RedirectToAction("Index", "Home", new { area = "" });
        }

        public ActionResult SendEmail(CustomerModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            try
            {
                if (String.IsNullOrWhiteSpace(customer.Email))
                    throw new NopException("Customer email is empty");
                if (!CommonHelper.IsValidEmail(customer.Email))
                    throw new NopException("Customer email is not valid");
                if (String.IsNullOrWhiteSpace(model.SendEmail.Subject))
                    throw new NopException("Email subject is empty");
                if (String.IsNullOrWhiteSpace(model.SendEmail.Body))
                    throw new NopException("Email body is empty");

                var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                if (emailAccount == null)
                    emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                if (emailAccount == null)
                    throw new NopException("Email account can't be loaded");

                var email = new QueuedEmail()
                {
                    EmailAccountId = emailAccount.Id,
                    FromName = emailAccount.DisplayName,
                    From = emailAccount.Email,
                    ToName = customer.GetFullName(),
                    To = customer.Email,
                    Subject = model.SendEmail.Subject,
                    Body = model.SendEmail.Body,
                    CreatedOnUtc = DateTime.UtcNow,
                };
                _queuedEmailService.InsertQueuedEmail(email);
                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendEmail.Queued"));
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
            }

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        public ActionResult SendPm(CustomerModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            try
            {
                if (!_forumSettings.AllowPrivateMessages)
                    throw new NopException("Private messages are disabled");
                if (customer.IsGuest())
                    throw new NopException("Customer should be registered");
                if (String.IsNullOrWhiteSpace(model.SendPm.Subject))
                    throw new NopException("PM subject is empty");
                if (String.IsNullOrWhiteSpace(model.SendPm.Message))
                    throw new NopException("PM message is empty");


                var privateMessage = new PrivateMessage
                {
                    StoreId = _storeContext.CurrentStore.Id,
                    ToCustomerId = customer.Id,
                    FromCustomerId = _workContext.CurrentCustomer.Id,
                    Subject = model.SendPm.Subject,
                    Text = model.SendPm.Message,
                    IsDeletedByAuthor = false,
                    IsDeletedByRecipient = false,
                    IsRead = false,
                    CreatedOnUtc = DateTime.UtcNow
                };

                _forumService.InsertPrivateMessage(privateMessage);
                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendPM.Sent"));
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
            }

            return RedirectToAction("Edit", new { id = customer.Id });
        }
        
        #endregion
        
        #region Reward points history

        [GridAction]
        public ActionResult RewardPointsHistorySelect(int customerId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No customer found with the specified id");

            var model = new List<CustomerModel.RewardPointsHistoryModel>();
            foreach (var rph in customer.RewardPointsHistory.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id))
            {
                model.Add(new CustomerModel.RewardPointsHistoryModel()
                    {
                        Points = rph.Points,
                        PointsBalance = rph.PointsBalance,
                        Message = rph.Message,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(rph.CreatedOnUtc, DateTimeKind.Utc)
                    });
            } 
            var gridModel = new GridModel<CustomerModel.RewardPointsHistoryModel>
            {
                Data = model,
                Total = model.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        [ValidateInput(false)]
        public ActionResult RewardPointsHistoryAdd(int customerId, int addRewardPointsValue, string addRewardPointsMessage)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);

            customer.AddRewardPointsHistoryEntry(addRewardPointsValue, addRewardPointsMessage);
            _customerService.UpdateCustomer(customer);

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }
        
        #endregion
        
        #region Addresses

        /// <summary>
        /// Sets the list of a customers addresses. Modifications has been made to place the default-adress first in the list.
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [GridAction]
        public ActionResult AddressesSelect(int customerId, GridCommand command) 
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No customer found with the specified id", "customerId");

            var addresses = customer.Addresses.OrderByDescending(a => a.CreatedOnUtc).ThenByDescending(a => a.Id).ToList();


            // ---------------------Place defualt address at top------------------------ [Earstore 2.0]

            var oldArray = customer.Addresses.OrderByDescending(a => a.CreatedOnUtc).ThenByDescending(a => a.Id).ToArray();
            var newArray = customer.Addresses.OrderByDescending(a => a.CreatedOnUtc).ThenByDescending(a => a.Id).ToArray();

            if (oldArray.Length < 2)
            {
                //no swapping needed, 1 or 0 adresses.
            }
            else
            {
                if (oldArray[0].Id.ToString() == customer.DefaultAdress)
                {

                }
                if (oldArray[1].Id.ToString() == customer.DefaultAdress)
                {
                    newArray[0] = oldArray[1];
                    newArray[1] = oldArray[0];
                    addresses = newArray.ToList();
                }
            }
            // ---------------------Place defualt address at top------------------------ [Earstore 2.0]
            
            var gridModel = new GridModel<AddressModel>
            {
                Data = addresses.Select(x =>
                {
                    var model = x.ToModel();
                    var addressHtmlSb = new StringBuilder("<div>");
                    if (_addressSettings.CompanyEnabled && !String.IsNullOrEmpty(model.Company))
                        addressHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.Company));
                    if (_addressSettings.StreetAddressEnabled && !String.IsNullOrEmpty(model.Address1))
                        addressHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.Address1));
                    if (_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(model.Address2))
                        addressHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.Address2));
                    if (_addressSettings.CityEnabled && !String.IsNullOrEmpty(model.City))
                        addressHtmlSb.AppendFormat("{0},", Server.HtmlEncode(model.City));
                    if (_addressSettings.StateProvinceEnabled && !String.IsNullOrEmpty(model.StateProvinceName))
                        addressHtmlSb.AppendFormat("{0},", Server.HtmlEncode(model.StateProvinceName));
                    if (_addressSettings.ZipPostalCodeEnabled && !String.IsNullOrEmpty(model.ZipPostalCode))
                        addressHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.ZipPostalCode));
                    if (_addressSettings.CountryEnabled && !String.IsNullOrEmpty(model.CountryName))
                        addressHtmlSb.AppendFormat("{0}", Server.HtmlEncode(model.CountryName));
                    addressHtmlSb.Append("</div>");
                    model.AddressHtml = addressHtmlSb.ToString();
                    return model;
                }),
                Total = addresses.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        /// <summary>
        /// Not used, replaced by AddressDeleteWithPageReload [Earstore 2.0]
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [GridAction]
        public ActionResult AddressDelete(int customerId, int addressId, GridCommand command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No customer found with the specified id", "customerId");

            var address = customer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                //No customer found with the specified id
                return Content("No customer found with the specified id");

            if (customer.DefaultAdress == address.Id.ToString())
            {
                customer.DefaultAdress = "Borttagen";
            }

            customer.RemoveAddress(address);
            _customerService.UpdateCustomer(customer);
            //now delete the address record
            _addressService.DeleteAddress(address);

            return RedirectToAction("Edit", new { id = customer.Id });

            //return AddressesSelect(customerId, command);
        }


        /// <summary>
        /// The original method for removing addresses did not trigger a page reload. Since we use additional logics with default-adresses,
        /// we must trigger a page reload and possibly set a new default address if the current defualt-address is removed. If not reloaded the value displaying 
        /// the default address and the dropdown-list will not be updated according to the actual state of addresses.
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="addressId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult AddressDeleteWithPageReload(int addressId, int customerId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No customer found with the specified id", "customerId");

            var address = customer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                //No customer found with the specified id
                return Content("No customer found with the specified id");

            if (customer.DefaultAdress == address.Id.ToString())
            {
                customer.DefaultAdress = "Borttagen";
            }

            customer.RemoveAddress(address);
            _customerService.UpdateCustomer(customer);
            //now delete the address record
            _addressService.DeleteAddress(address);

            return RedirectToAction("Edit", new { id = customer.Id });

            //return AddressesSelect(customerId, command);
        }


        /// <summary>
        /// Creating addresses has been modified to not require an email-address. Therefore the view displaying the adress-model will not show errors if email is not entered.
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult AddressCreate(int customerId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();


            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            if (customer.Addresses.Count > 1)
            {
                ErrorNotification("Kunden har redan två adresser.");
                return RedirectToAction("Edit", new { id = customer.Id });
            }

            var model = new CustomerAddressModel();
            model.Address = new AddressModel();
            model.CustomerId = customerId;
            model.Address.FirstNameEnabled = true;
            model.Address.FirstNameRequired = true;
            model.Address.LastNameEnabled = true;
            model.Address.LastNameRequired = true;
            model.Address.EmailEnabled = true;
            //----------------------------------CHANGED-------------------- [Earstore 2.0]
           // model.Address.EmailRequired = true;
            model.Address.EmailRequired = false;
            model.Address.CompanyEnabled = _addressSettings.CompanyEnabled;
            model.Address.CompanyRequired = _addressSettings.CompanyRequired;
            model.Address.CountryEnabled = _addressSettings.CountryEnabled;
            model.Address.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
            model.Address.CityEnabled = _addressSettings.CityEnabled;
            model.Address.CityRequired = _addressSettings.CityRequired;
            model.Address.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
            model.Address.StreetAddressRequired = _addressSettings.StreetAddressRequired;
            model.Address.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
            model.Address.StreetAddress2Required = _addressSettings.StreetAddress2Required;
            model.Address.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
            model.Address.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
            model.Address.PhoneEnabled = _addressSettings.PhoneEnabled;
            model.Address.PhoneRequired = _addressSettings.PhoneRequired;
            model.Address.FaxEnabled = _addressSettings.FaxEnabled;
            model.Address.FaxRequired = _addressSettings.FaxRequired;
            //countries
            model.Address.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.Address.AvailableCountries.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == 73) });
            model.Address.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });

            return View(model);
        }

        /// <summary>
        /// The method for creating the customer-address has been modified to only allow a customer to have no more than 2 addresses.
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddressCreate(CustomerAddressModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                var address = model.Address.ToEntity();
                address.CreatedOnUtc = DateTime.UtcNow;
                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;

                //-----------------------EDITED MAXIMUM 2 ADDRESSES------------------------------ [Earstore 2.0]
                if (customer.Addresses.Count < 2)
                {
                    customer.Addresses.Add(address);
                    _customerService.UpdateCustomer(customer);
                }
                else
                {
                    ErrorNotification("Max 2 adresser");
                }
                //-----------------------EDITED MAXIMUM 2 ADDRESSES------------------------------ [Earstore 2.0]

                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.Added"));
                //return RedirectToAction("AddressEdit", new { addressId = address.Id, customerId = model.CustomerId });
                return RedirectToAction("Edit", new { id = customer.Id });
            }

            //If we got this far, something failed, redisplay form
            model.CustomerId = customer.Id;
            //countries
            model.Address.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.Address.AvailableCountries.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == model.Address.CountryId) });
            //states
            var states = model.Address.CountryId.HasValue ? _stateProvinceService.GetStateProvincesByCountryId(model.Address.CountryId.Value, true).ToList() : new List<StateProvince>();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.Address.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.Address.StateProvinceId) });
            }
            else
                model.Address.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });
            return View(model);
        }

        /// <summary>
        /// The view for updating an address will not require an email.
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="addressId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult AddressEdit(int addressId, int customerId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            var address = _addressService.GetAddressById(addressId);
            if (address == null)
                //No address found with the specified id
                return RedirectToAction("Edit", new { id = customer.Id });

            var model = new CustomerAddressModel();
            model.CustomerId = customerId;
            model.Address = address.ToModel();
            model.Address.FirstNameEnabled = true;
            model.Address.FirstNameRequired = true;
            model.Address.LastNameEnabled = true;
            model.Address.LastNameRequired = true;
            model.Address.EmailEnabled = true;
            //---------------------------------------CHANGED-------------------- [Earstore 2.0]
            // model.Address.EmailRequired = true;
            model.Address.EmailRequired = false;
            model.Address.CompanyEnabled = _addressSettings.CompanyEnabled;
            model.Address.CompanyRequired = _addressSettings.CompanyRequired;
            model.Address.CountryEnabled = _addressSettings.CountryEnabled;
            model.Address.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
            model.Address.CityEnabled = _addressSettings.CityEnabled;
            model.Address.CityRequired = _addressSettings.CityRequired;
            model.Address.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
            model.Address.StreetAddressRequired = _addressSettings.StreetAddressRequired;
            model.Address.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
            model.Address.StreetAddress2Required = _addressSettings.StreetAddress2Required;
            model.Address.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
            model.Address.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
            model.Address.PhoneEnabled = _addressSettings.PhoneEnabled;
            model.Address.PhoneRequired = _addressSettings.PhoneRequired;
            model.Address.FaxEnabled = _addressSettings.FaxEnabled;
            model.Address.FaxRequired = _addressSettings.FaxRequired;
            //countries
            model.Address.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.Address.AvailableCountries.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == address.CountryId) });
            //states
            var states = address.Country != null ? _stateProvinceService.GetStateProvincesByCountryId(address.Country.Id, true).ToList() : new List<StateProvince>();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.Address.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == address.StateProvinceId) });
            }
            else
                model.Address.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });

            return View(model);
        }

        [HttpPost]
        public ActionResult AddressEdit(CustomerAddressModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            var address = _addressService.GetAddressById(model.Address.Id);
            if (address == null)
                //No address found with the specified id
                return RedirectToAction("Edit", new { id = customer.Id });

            if (ModelState.IsValid)
            {
                address = model.Address.ToEntity(address);
                _addressService.UpdateAddress(address);

                //SECTION ADD UPDATED DATA TO CUSTOMER ADDRESS ADMIN
                if (model.Address.Id == Int32.Parse(customer.DefaultAdress))
                {
                    if (!customer.Email.Equals(model.Address.Email.Trim(), StringComparison.InvariantCultureIgnoreCase))
                    { 
                        //change email 
                        _customerRegistrationService.SetEmail(customer, model.Address.Email.Trim());
                        //re-authenticate (if usernames are disabled)
                        //if (!_customerSettings.UsernamesEnabled)
                        //{
                        //    _authenticationService.SignIn(customer, true);
                        //}
                    }

                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.Address.FirstName);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.Address.LastName);

                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Address.Company);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.Address.Address1);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.Address.Address2);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.Address.ZipPostalCode);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.Address.City);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.Address.CountryId);
                    if (_customerSettings.PhoneEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Address.PhoneNumber);

                }
                

                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.Updated"));
                return RedirectToAction("AddressEdit", new { addressId = model.Address.Id, customerId = model.CustomerId });
            }

            //If we got this far, something failed, redisplay form
            model.CustomerId = customer.Id;
            model.Address = address.ToModel();
            model.Address.FirstNameEnabled = true;
            model.Address.FirstNameRequired = true;
            model.Address.LastNameEnabled = true;
            model.Address.LastNameRequired = true;
            model.Address.EmailEnabled = true;
            model.Address.EmailRequired = true;
            model.Address.CompanyEnabled = _addressSettings.CompanyEnabled;
            model.Address.CompanyRequired = _addressSettings.CompanyRequired;
            model.Address.CountryEnabled = _addressSettings.CountryEnabled;
            model.Address.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
            model.Address.CityEnabled = _addressSettings.CityEnabled;
            model.Address.CityRequired = _addressSettings.CityRequired;
            model.Address.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
            model.Address.StreetAddressRequired = _addressSettings.StreetAddressRequired;
            model.Address.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
            model.Address.StreetAddress2Required = _addressSettings.StreetAddress2Required;
            model.Address.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
            model.Address.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
            model.Address.PhoneEnabled = _addressSettings.PhoneEnabled;
            model.Address.PhoneRequired = _addressSettings.PhoneRequired;
            model.Address.FaxEnabled = _addressSettings.FaxEnabled;
            model.Address.FaxRequired = _addressSettings.FaxRequired;
            //countries
            model.Address.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.Address.AvailableCountries.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == address.CountryId) });
            //states
            var states = address.Country != null ? _stateProvinceService.GetStateProvincesByCountryId(address.Country.Id, true).ToList() : new List<StateProvince>();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.Address.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == address.StateProvinceId) });
            }
            else
                model.Address.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });

            return View(model);
        }

        #endregion

        #region Orders
        
        /// <summary>
        /// Provides the list of orders for a chosen customer. Color marking support variables have been added, allowing the view to color the rows.
        /// [Earstore 2.0]
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult OrderList(int customerId, GridCommand command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var orders = _orderService.SearchOrders(0, 0, customerId,
                    null, null, null, null, null, null, null, 0, int.MaxValue, null, null, null);

        

            var model = new GridModel<CustomerModel.OrderModel>
            {
                Data = orders.PagedForCommand(command)
                    .Select(order =>
                    {
                        var store = _storeService.GetStoreById(order.StoreId);

                        //----------------added-------------------- [Earstore 2.0]
                        Boolean issCardPaymentAndUnpaid = false;
                        Boolean isNew = false;

                        if (order.Customer.IsInCustomerRole("Ny Kund"))
                        {
                            isNew = true;
                        }
                        if (order.PaymentMethodSystemName == "Payments.Manual" && order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext) == "Pending")
                        {
                            issCardPaymentAndUnpaid = true;
                        }
                        //----------------added-------------------- [Earstore 2.0]
                        var orderModel = new CustomerModel.OrderModel()
                        {
                            Id = order.Id,
                            IsExpiredInvoice = order.IsExpiredInvoice, //added [Earstore 2.0]
                            CustomizedStatus = order.CustomizedStatus, //added [Earstore 2.0]
                            OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                            PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
                            ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
                            OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false),
                            StoreName = store != null ? store.Name : "Unknown",
                            CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                            isCardPaymentAndUnpaid = issCardPaymentAndUnpaid,
                            isNewCustomer = isNew
                        };
                        return orderModel;
                    }),
                Total = orders.Count
            };

            return new JsonResult
            {
                Data = model
            };
        }
        
        #endregion

        #region Reports

        public ActionResult Reports()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var model = new CustomerReportsModel();
            //customers by number of orders
            model.BestCustomersByNumberOfOrders = new BestCustomersReportModel();
            model.BestCustomersByNumberOfOrders.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.BestCustomersByNumberOfOrders.AvailableOrderStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.BestCustomersByNumberOfOrders.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.BestCustomersByNumberOfOrders.AvailablePaymentStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.BestCustomersByNumberOfOrders.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.BestCustomersByNumberOfOrders.AvailableShippingStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //customers by order total
            model.BestCustomersByOrderTotal = new BestCustomersReportModel();
            model.BestCustomersByOrderTotal.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.BestCustomersByOrderTotal.AvailableOrderStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.BestCustomersByOrderTotal.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.BestCustomersByOrderTotal.AvailablePaymentStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.BestCustomersByOrderTotal.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.BestCustomersByOrderTotal.AvailableShippingStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            
            return View(model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult ReportBestCustomersByOrderTotalList(GridCommand command, BestCustomersReportModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return Content("");

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;


            var items = _customerReportService.GetBestCustomersReport(startDateValue, endDateValue,
                orderStatus, paymentStatus, shippingStatus, 1);
            var gridModel = new GridModel<BestCustomerReportLineModel>
            {
                Data = items.Select(x =>
                {
                    var m = new BestCustomerReportLineModel()
                    {
                        CustomerId = x.CustomerId,
                        OrderTotal = _priceFormatter.FormatPrice(x.OrderTotal, true, false),
                        OrderCount = x.OrderCount,
                    };
                    var customer = _customerService.GetCustomerById(x.CustomerId);
                    if (customer != null)
                    {
                        m.CustomerName = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
                    }
                    return m;
                }),
                Total = items.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult ReportBestCustomersByNumberOfOrdersList(GridCommand command, BestCustomersReportModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return Content("");

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;


            var items = _customerReportService.GetBestCustomersReport(startDateValue, endDateValue,
                orderStatus, paymentStatus, shippingStatus, 2);
            var gridModel = new GridModel<BestCustomerReportLineModel>
            {
                Data = items.Select(x =>
                {
                    var m = new BestCustomerReportLineModel()
                    {
                        CustomerId = x.CustomerId,
                        OrderTotal = _priceFormatter.FormatPrice(x.OrderTotal, true, false),
                        OrderCount = x.OrderCount,
                    };
                    var customer = _customerService.GetCustomerById(x.CustomerId);
                    if (customer != null)
                    {
                        m.CustomerName = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
                    }
                    return m;
                }),
                Total = items.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        [ChildActionOnly]
        public ActionResult ReportRegisteredCustomers()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return Content("");

            var model = GetReportRegisteredCustomersModel();
            return PartialView(model);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult ReportRegisteredCustomersList(GridCommand command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return Content("");

            var model = GetReportRegisteredCustomersModel();
            var gridModel = new GridModel<RegisteredCustomerReportLineModel>
            {
                Data = model,
                Total = model.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }
        
        #endregion

        #region Current shopping cart/ wishlist

        [GridAction(EnableCustomBinding = true)]
        public ActionResult GetCartList(int customerId, int cartTypeId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return Content("");

            var customer = _customerService.GetCustomerById(customerId);
            var cart = customer.ShoppingCartItems.Where(x => x.ShoppingCartTypeId == cartTypeId).ToList();

            var gridModel = new GridModel<ShoppingCartItemModel>()
            {
                Data = cart.Select(sci =>
                {
                    decimal taxRate;
                    var store = _storeService.GetStoreById(sci.StoreId); 
                    var sciModel = new ShoppingCartItemModel()
                    {
                        Id = sci.Id,
                        Store = store != null ? store.Name : "Unknown",
                        ProductId = sci.ProductId,
                        Quantity = sci.Quantity,
                        ProductName = sci.Product.Name,
                        UnitPrice = _priceFormatter.FormatPrice(_taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci, true), out taxRate)),
                        Total = _priceFormatter.FormatPrice(_taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, true), out taxRate)),
                        UpdatedOn = _dateTimeHelper.ConvertToUserTime(sci.UpdatedOnUtc, DateTimeKind.Utc)
                    };
                    return sciModel;
                }),
                Total = cart.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        #endregion

        #region Activity log

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult ListActivityLog(GridCommand command, int customerId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return Content("");

            var activityLog = _customerActivityService.GetAllActivities(null, null, customerId, 0, command.Page - 1, command.PageSize);
            var gridModel = new GridModel<CustomerModel.ActivityLogModel>
            {
                Data = activityLog.Select(x =>
                {
                    var m = new CustomerModel.ActivityLogModel()
                    {
                        Id = x.Id,
                        ActivityLogTypeName = x.ActivityLogType.Name,
                        Comment = x.Comment,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc)
                    };
                    return m;

                }),
                Total = activityLog.TotalCount
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        #endregion

        #region Export / Import

        public ActionResult ExportExcelAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            try
            {
                var customers = _customerService.GetAllCustomers();

                byte[] bytes = null;
                using (var stream = new MemoryStream())
                {
                    _exportManager.ExportCustomersToXlsx(stream, customers);
                    bytes = stream.ToArray();
                }
                return File(bytes, "text/xls", "customers.xlsx");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        public ActionResult ExportExcelSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customers = new List<Customer>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                customers.AddRange(_customerService.GetCustomersByIds(ids));
            }

            byte[] bytes = null;
            using (var stream = new MemoryStream())
            {
                _exportManager.ExportCustomersToXlsx(stream, customers);
                bytes = stream.ToArray();
            }
            return File(bytes, "text/xls", "customers.xlsx");
        }

        public ActionResult ExportXmlAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            try
            {
                var customers = _customerService.GetAllCustomers();
                
                var xml = _exportManager.ExportCustomersToXml(customers);
                return new XmlDownloadResult(xml, "customers.xml");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        public ActionResult ExportXmlSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var customers = new List<Customer>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                customers.AddRange(_customerService.GetCustomersByIds(ids));
            }

            var xml = _exportManager.ExportCustomersToXml(customers);
            return new XmlDownloadResult(xml, "customers.xml");
        }



        #endregion
    }
}
