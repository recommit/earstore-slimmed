﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Admin.Models.Messages;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using Telerik.Web.Mvc;
using Nop.Services.Customers;
using Nop.Core.Domain.Customers;
using Nop.Admin.Models.Customers;
using System.Collections.Generic;
using Nop.Services.Common;
using Nop.Core.Domain.Common;
using Nop.Services.Directory;
using Nop.Services.Logging;
using System.Diagnostics;

namespace Nop.Admin.Controllers
{
	[AdminAuthorize]
	public partial class NewsLetterSubscriptionController : BaseNopController
	{
		private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
		private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;

        private readonly ICustomerService _customerService;
        private readonly IAddressService _addressService;
        private readonly ICountryService _countryService;
        private readonly ILogger _logger;

        public NewsLetterSubscriptionController(ILogger logger, INewsLetterSubscriptionService newsLetterSubscriptionService,
			IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            ICustomerService customerService,
              IAddressService addressService,
            ICountryService countryService)
		{
			this._newsLetterSubscriptionService = newsLetterSubscriptionService;
			this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._customerService = customerService;
            this._addressService = addressService;
            this._countryService = countryService;
            this._logger = logger;
		}

		public ActionResult Index()
		{
			return RedirectToAction("List");
		}

		public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();

           

            var model = new NewsLetterSubscriptionListModel( );
            model.AvailableCustomerRoles = _customerService.GetAllCustomerRoles(true).Select(cr => cr.ToModel()).ToList();           
              
			return View(model);
		}

		[HttpPost, GridAction(EnableCustomBinding = true)]
		public ActionResult SubscriptionList(GridCommand command, NewsLetterSubscriptionListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();

            var newsletterSubscriptions = _newsLetterSubscriptionService.GetAllNewsLetterSubscriptions(model.SearchEmail, 
                command.Page - 1, command.PageSize, true);

            var gridModel = new GridModel<NewsLetterSubscriptionModel>
            {
                Data = newsletterSubscriptions.Select(x =>
				{
					var m = x.ToModel();
					m.CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc);
					return m;
				}),
                Total = newsletterSubscriptions.TotalCount
            };
            return new JsonResult
            {
                Data = gridModel
            };
		}

        [GridAction(EnableCustomBinding = true)]
        public ActionResult SubscriptionUpdate(NewsLetterSubscriptionModel model, GridCommand command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();
            
            if (!ModelState.IsValid)
            {
                //display the first model error
                var modelStateErrors = this.ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
                return Content(modelStateErrors.FirstOrDefault());
            }

            var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionById(model.Id);
            subscription.Email = model.Email;
            subscription.Active = model.Active;
            _newsLetterSubscriptionService.UpdateNewsLetterSubscription(subscription);

            return SubscriptionList(command, new NewsLetterSubscriptionListModel());
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult SubscriptionDelete(int id, GridCommand command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();

            var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionById(id);
            if (subscription == null)
                throw new ArgumentException("No subscription found with the specified id");
            _newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);

            return SubscriptionList(command, new NewsLetterSubscriptionListModel());
        }

        /// <summary>
        /// This method has been rebuilt to fit Earstore 2.0:s needs of customer newsletter subscription. 
        /// Flags saved on the customer model is used to determine which customers should be targets for newsletters,
        /// not actual objects of newsletterSubscriptions.
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
		public ActionResult ExportCsv(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();

            _logger.Debug("Started newsletter Export");

            //string email = form["searchEmail"];
           

            //--------Convert ID:s to a list of actual CustomerRole objects-----------
            string chosenRoles = form["SearchCustomerRoleIds"];
            List<CustomerRole> listOfChosenCustomerRoles = new List<CustomerRole>();
        
            Boolean getAll = false;
            if (chosenRoles == null) { getAll = true; }
            else
            {
                string[] array = chosenRoles.Split(',');
                int i = 0;
                while (array.Length > i)
                {
                    CustomerRole customerRole = _customerService.GetCustomerRoleById(Int32.Parse(array[i]));
                    listOfChosenCustomerRoles.Add(customerRole);
                    i++;
                }
            }

                           

			string fileName = String.Format("newsletter_emails_{0}_{1}.txt", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));

			var sb = new StringBuilder();
            _logger.Debug("Before Get all Customers from DB");
            var customers = _customerService.GetAllCustomers(getOnlyAcceptNewsletters: true);
            _logger.Debug("Number of customers retrieved from DB:" + customers.Count);

            List<Customer> theCustomers = new List<Customer>();

            foreach (Customer c in customers)
            {
                theCustomers.Add(c);
            }

            //while (theCustomers.Count < 25000)
            //{
            //    theCustomers.AddRange(theCustomers);
            //}

        
            int counter = 0;
            Boolean mathcingCustomerRole = false;

          
             
             _logger.Debug("Newsletter Export started.");




             foreach (Customer c in theCustomers)
             {
                 try
                 {
                     //Stopwatch totalTime = new Stopwatch();
                     //totalTime.Start();

                     //Stopwatch sw0 = new Stopwatch();
                     //Stopwatch sw1 = new Stopwatch();
                     //Stopwatch sw2 = new Stopwatch();
                     //Stopwatch sw3 = new Stopwatch();
                     //Stopwatch sw4 = new Stopwatch();



                     mathcingCustomerRole = false;
                     if (getAll == false)
                     {
                         foreach (CustomerRole chosenCustomerRole in listOfChosenCustomerRoles)
                         {
                             foreach (CustomerRole CrOfCustomer in c.CustomerRoles)
                             {
                                 if (CrOfCustomer.Id == chosenCustomerRole.Id)
                                 {
                                     mathcingCustomerRole = true; //The check is if a customer has ANY of the chosen roles, not ALL.
                                 }
                             }
                         }
                     }
                     else
                     {
                         mathcingCustomerRole = true;
                     }

                     if (mathcingCustomerRole)
                     {    //sw1.Start();                     
                         if (c.DefaultAdress != null && c.DefaultAdress != "")
                         {

                             //sw0.Start();
                             Address a = new Address();
                             foreach (Address address in c.Addresses)
                             {
                                 if (address.Id == Int32.Parse(c.DefaultAdress))
                                 {
                                     a = address;
                                 }
                             }

                             //sw0.Stop();
                             //sw2.Start();

                             sb.Append(a.FirstName);
                             sb.Append("$");
                             sb.Append(a.LastName);
                             sb.Append("$");
                             sb.Append(a.Address1);
                             sb.Append("$");
                             sb.Append(a.Address2);
                             sb.Append("$");
                             sb.Append(a.ZipPostalCode);
                             sb.Append("$");
                             sb.Append(a.City);
                             sb.Append("$");
                             sb.Append(a.Country.Name);
                             sb.Append("$");
                             sb.Append(a.PhoneNumber);
                             sb.Append("$");
                             sb.Append(c.Email);
                             sb.Append("$");

                             //sw2.Stop();

                         }
                         else //If the customer does not have a real adress bound, try to use any available fields on the customer-object.
                         {   //sw3.Start(); 
                             sb.Append(c.GetAttribute<string>(SystemCustomerAttributeNames.FirstName));
                             sb.Append("$");
                             sb.Append(c.GetAttribute<string>(SystemCustomerAttributeNames.LastName));
                             sb.Append("$");
                             sb.Append(c.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress));
                             sb.Append("$");
                             sb.Append(c.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2));
                             sb.Append("$");
                             sb.Append(c.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode)); ;
                             sb.Append("$");
                             sb.Append(c.GetAttribute<string>(SystemCustomerAttributeNames.City));
                             sb.Append("$");
                             sb.Append(_countryService.GetCountryById(c.GetAttribute<int>(SystemCustomerAttributeNames.CountryId)).Name); //FELHANTERA
                             sb.Append("$");
                             sb.Append(c.GetAttribute<string>(SystemCustomerAttributeNames.Phone));
                             sb.Append("$");
                             sb.Append(c.Email);
                             sb.Append("$");
                             //sw3.Stop(); 
                         }

                         sb.Append(Environment.NewLine);  //new line




                         counter++;

                         //sw1.Stop();
                         //totalTime.Stop();

                         //         _logger.Debug("    sw0: [" + sw0.Elapsed.Seconds + "]  [" + sw0.Elapsed.Milliseconds + "]" +
                         //"    sw1: [" + sw1.Elapsed.Seconds + "]  [" + sw1.Elapsed.Milliseconds + "]" +
                         //"    sw2: [" + sw2.Elapsed.Seconds + "]  [" + sw2.Elapsed.Milliseconds + "]" +
                         //"    sw3: [" + sw3.Elapsed.Seconds + "]  [" + sw3.Elapsed.Milliseconds + "]" +
                         //"    sw3: [" + sw4.Elapsed.Seconds + "]  [" + sw4.Elapsed.Milliseconds + "]" +
                         //         "    totalTime: [" + totalTime.Elapsed.Seconds + "]  [" + totalTime.Elapsed.Milliseconds + "]");


                         if (counter % 1000 == 0)
                         {
                             _logger.Debug("Row nr:" + counter);
                         }
                     }
                 }
                 catch (Exception e)
                 {
                     _logger.Debug("Unhandled error");
                 }
             }


           

            _logger.Debug("Finished filling the list");
			string result = sb.ToString();

            _logger.Debug("Returning file.");

			return File(Encoding.UTF8.GetBytes(result), "text/csv", fileName);
		}

        [HttpPost]
        public ActionResult ImportCsv(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();

            try
            {
                var file = Request.Files["importcsvfile"];
                if (file != null && file.ContentLength > 0)
                {
                    int count = 0;

                    using (var reader = new StreamReader(file.InputStream))
                    {
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            if (String.IsNullOrWhiteSpace(line))
                                continue;
                            string[] tmp = line.Split(',');

                            var email = "";
                            bool isActive = true;
                            //parse
                            if (tmp.Length == 1)
                            {
                                //"email" only
                                email = tmp[0].Trim();
                            }
                            else if (tmp.Length == 2)
                            {
                                //"email" and "active" fields specified
                                email = tmp[0].Trim();
                                isActive = Boolean.Parse(tmp[1].Trim());
                            }
                            else
                                throw new NopException("Wrong file format");

                            //import
                            var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmail(email);
                            if (subscription != null)
                            {
                                subscription.Email = email;
                                subscription.Active = isActive;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(subscription);
                            }
                            else
                            {
                                subscription = new NewsLetterSubscription()
                                {
                                    Active = isActive,
                                    CreatedOnUtc = DateTime.UtcNow,
                                    Email = email,
                                    NewsLetterSubscriptionGuid = Guid.NewGuid()
                                };
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(subscription);
                            }
                            count++;
                        }
                        SuccessNotification(String.Format(_localizationService.GetResource("Admin.Promotions.NewsLetterSubscriptions.ImportEmailsSuccess"), count));
                        return RedirectToAction("List");
                    }
                }
                ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }
	}
}
