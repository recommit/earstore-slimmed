﻿var menuTitleElementSelector = ".menu-title";
var menuElementSelector = ".top-menu";
var responsiveTables = ".order-summary-content .cart, .wishlist-page .cart, .downloadable-products-page .data-table, .return-request-page .data-table, .order-details-page .data-table, .compare-products-table-mobile";

function AntiSpam(emailName, emailDomain) {
    window.location.href = 'mailto:' + emailName + '@' + emailDomain;
}

function toggleBlocks() {

    $(".block").each(function () {
        var viewport = $.getSpikesViewPort();

        if (viewport.width >= 768) {
            $(this).children().eq(1).show();
        }
        else {
            $(this).children().eq(1).hide();
        }
    });
}

function configureHeaderMenu() {
    var documentDimensions = $.getSpikesViewPort();

    if (documentDimensions.width <= 980) {
        $(menuTitleElementSelector).show();
        $(menuElementSelector).hide();
    }
    else {
        $(menuTitleElementSelector).hide();
        $(menuElementSelector).show();
    }
}

$(document).ready(function () {
    var previousWidth = $.getSpikesViewPort().width;
    var checkWidth = function () {
        var viewport = $.getSpikesViewPort();
        if (viewport.width !== previousWidth) {
            previousWidth = viewport.width;

            toggleBlocks();

            configureHeaderMenu();
        }
    };

    $.addSpikesWindowEvent("resize", checkWidth);
    $.addSpikesWindowEvent("orientationchange", checkWidth);

    // (optional) Android doesn't always fire orientationChange on 180 degree turns
    setInterval(checkWidth, 2000);

    configureHeaderMenu();

    $(".block").each(function () {

        var that = this;

        $(this).find(".title strong").click(function () {

            var viewport = $.getSpikesViewPort();

            if (viewport.width <= 768) {
                //$(that).find(".listbox").slideToggle("slow"); //instead of taking the .listbox we can take the second child of the .block element
                // as the blocks in the filters do not have the .listbox class, but filtersGroupPanel instead
                $(that).children().eq(1).slideToggle("slow");
            }
        });
    });

    $(".nopAjaxFilters7Spikes .filtersTitlePanel ").click(function () {
        var viewport = $.getSpikesViewPort();
        if (viewport.width <= 768) {
            //$(that).find(".listbox").toggle("small"); instead of taking the .listbox we can take the second child of the .block element
            // as the blocks in the filters do not have the .listbox class, but filtersGroupPanel instead
            $(".nopAjaxFilters7Spikes").children().eq(1).slideToggle("slow");
        }

    });

    $(menuTitleElementSelector).click(function () {
        if ($(menuElementSelector).is(":hidden")) {
            $(menuElementSelector).show();
        } else {
            $(menuElementSelector).hide();
        }
    });

    // FOOTABLE.JS

    $(responsiveTables).footable();

})