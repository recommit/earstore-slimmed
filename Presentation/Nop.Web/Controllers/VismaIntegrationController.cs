﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.ExportImport;
using Nop.Services.Orders;
using Nop.Services.Tax;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Nop.Core.Domain.Shipping;
using Nop.Services.Shipping;
using Nop.Services.Logging;

namespace Nop.Web.Controllers
{
    public class VismaIntegrationController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IExportManager _exportManager;
        private readonly IProductService _productService;
        private readonly ICustomerService _customerService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly IImportManager _importManager;
        private readonly ITaxService _taxService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly IShipmentService _shipmentService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ILogger _logger;

        public VismaIntegrationController(ILogger logger, IOrderProcessingService orderProcessingService, IShipmentService shipmentService, ITaxCategoryService taxCategoryService, ITaxService taxService, RewardPointsSettings rewardPointsSettings, IExportManager exportManager, IOrderService orderService, IProductService productService, ICustomerService customerService, ICustomerRegistrationService customerRegistrationService, IImportManager importManager)
        {
            this._exportManager = exportManager;
            this._orderService = orderService;
            this._productService = productService;
            this._customerService = customerService;
            this._customerRegistrationService = customerRegistrationService;
            this._importManager = importManager;
            this._taxService = taxService;
            this._taxCategoryService = taxCategoryService;
             this._shipmentService = shipmentService;
             this._orderProcessingService = orderProcessingService;
             this._logger = logger;
        }
        //----------------------EDITED---------------------------

        /// <summary>
        /// This method is used In all Visma-integration methods below. These methods had to be placed as a controller not protected by the built in NC validation.
        /// So instead we add this authentication, to protect the methods, making them acessible only to the Visma integration program holding the correct credentials.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        protected String ValidateCredentials(string username, string password)
         {
             string result = "";

             CustomerLoginResults loginResult = _customerRegistrationService.ValidateCustomer(username, password);

             if (loginResult == CustomerLoginResults.Successful)
             {
                 Customer c = _customerService.GetCustomerByEmail(username);

                 Boolean isAdmin = false;
                 foreach (CustomerRole cr in c.CustomerRoles)
                 {
                     if (cr.SystemName == SystemCustomerRoleNames.Administrators)
                     {
                         isAdmin = true;
                     }
                 }
                 if (isAdmin == true)
                 {
                     result = "Successful";                     
                 }
                 else
                 {
                     result = "Användaren är ej administratör.";
                     _logger.Debug("Användaren är ej administratör. (Visma Auth)");
                 }          
             }
             else
             {
                 result = "Felkod: " + loginResult;
                 _logger.Debug("Visma Auth Failed: " + loginResult);
             }
             return result;
         }
        /// <summary>
        /// Returns a string containing information by all the products in Earstore 2.0. This information is used by the Visma-import to create/update the products in Visma.
        /// In original Earstore and its integration with Visma, this same export-method existed, but the information sent for each product contained less data-fields.
        /// The variable Volume in the string is calculated using width, length and height for the product in Earstore 2.0.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>String containing values: SKU;Name;Price;Weight;Volume;Taxrate; Separated by ";"</returns>
        public String VismaExportProducts(string username, string password)
        {

           string s = ValidateCredentials(username, password);
           if (s != "Successful")
           {
              return s;
           }

             String result = "";
               try
               {
                   //Get all orders ready for Visma Export
                   var products = _productService.SearchProducts(showHidden: true);
                   

                   foreach (Product p in products)
                   {
                       if (p.ProductTypeId != 10)
                       {
                           {
                               string taxrate = "";
                               if (p.TaxCategoryId == 0)
                               {
                                   taxrate = "25"; //if no other tax is set, use 25%
                               }
                               else
                               {
                                   TaxCategory tg = _taxCategoryService.GetTaxCategoryById(p.TaxCategoryId);

                                   if (tg.Name == "Varor 25%")
                                   {
                                       taxrate = "25";
                                   }
                                   if (tg.Name == "Böcker 6%")
                                   {
                                       taxrate = "6";
                                   }

                                   if (taxrate == "") { taxrate = "25"; }//if no other tax is set, use 25%
                               }
                               result = result + p.Sku + ";" + p.Name + ";" + (int)(p.Price) + ";" + ((double)p.Weight) + ";" + ((double)(p.Width * p.Length * p.Height)) + ";" + taxrate + ";";
                           }
                       }
                   }
                   return result;
               }
               catch (Exception exc)
               {
                   return null;
               }

           }      

        /// <summary>
        /// This method is to be called from the Visma-integration, for telling Earstore that a specifik order has been correctly imported.
        /// Doing this will set the order-status to Processing.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public String UpdateImportedVismaOrder(string username, string password, string ids)
        {
            string s = ValidateCredentials(username, password);
            if (s != "Successful")
            {
                return s;
            }

           Order o = _orderService.GetOrderById(Int32.Parse(ids));
           o.OrderStatus = OrderStatus.Processing;
           _orderService.UpdateOrder(o);
            return "hej";
        }
        /// <summary>
        /// Visma-integration uses this method to get a xml-file containing all the orders that are ready for export.
        /// An order is ready for export if orderStatus = Pending AND customizedStatus = "BESTÄLLD".
        /// It also ignores orders that are not paid while using card-payment
        /// This method searched the relevant orders, and passes them to a service for converting the orders to a xml-file.
        /// See exportManager.ExportOrdersToXmlVisma for further details on creating the xml-file.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>An xml-file containing all necessary data for the orders ready for Visma-export</returns>
        public string VismaExport(string username, string password, string ids)
        {
            string s = ValidateCredentials(username, password);
            if (s != "Successful")
            {
                return s;
            }
                      

            //----INSERT types filter on orders-----

            try
            {
                //Get all orders ready for Visma Export
                var orders = _orderService.SearchOrders(0, 0, 0, null, null, OrderStatus.Pending,
                    null, null, null, null, 0, int.MaxValue, "BESTÄLLD", null, null);


                //--------------FILTER on orderMethod----------------
                List<Order> theOrdersToExport = new List<Order>();
                string xml = "";
                if (ids == "Manuell order")
                {
                    ids = "Telefon";
                }
                if (ids == "Alla")  
                {
                    xml = _exportManager.ExportOrdersToXmlVisma(orders);
                }
                else
                {
                    foreach (Order o in orders) //--------------FILTER on orderMethod----------------
                    {
                        if (o.OrderMethod == ids)
                        {
                            theOrdersToExport.Add(o);
                        }
                    }
                     xml = _exportManager.ExportOrdersToXmlVisma(theOrdersToExport);
                }


               

                
                return xml;
            }
            catch (Exception exc)
            {                
                return null;
            }
        }


        public string UpdateAllCustomerUnpaidStatus(string username, string password)
        {
            string s = ValidateCredentials(username, password);
            if (s != "Successful")
            {
                return s;
            }

                var customers = _customerService.GetAllCustomers();
                Boolean custHasExpOrders = false;

                foreach (Customer c in customers)
                {
                    if (c.HasExpiredInvoices == true)
                    {
                        custHasExpOrders = false;
                        //Get all orders of this customer and check if any is expired

                        var orders = _orderService.SearchOrders(0, 0, c.Id,
                        null, null, null, null, null, null, null, 0, int.MaxValue, null, null, null);

                        foreach (Order ord in orders) 
                        {
                            if (ord.IsExpiredInvoice == true)
                            {
                                custHasExpOrders = true;
                            }
                        }

                        if (custHasExpOrders)
                        {
                            c.HasExpiredInvoices = true;
                        }
                        else
                        {
                            c.HasExpiredInvoices = false;
                        }
                        _customerService.UpdateCustomer(c);
                    }
                }
                    return "Uppdateringen lyckades";
        }




        /// <summary>
        /// This method is used by Visma-integration to get all orders which have not been payed yet.
        /// A list of all orders are passed to exportManager.ExportOrdersToXmlVismaUnpaid which returns an xml.file with orders having the paymentStatus = Pending.
        /// Visma uses the list of unpaid orders to detect any orders that might have been payed, comparing to the information in Visma. Visma then sends back the result of the
        /// comparison to another method in Earstore 2.0, which then finally updates the status in the website.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string VismaExportUnpaid(string username, string password)
        {
            string s = ValidateCredentials(username, password);
            if (s != "Successful")
            {
                return s;
            }

            try
            {
                //Get all orders ready for Visma Export
                var orders = _orderService.SearchOrders(0, 0, 0, null, null, null,
                    null, null, null, null, 0, int.MaxValue, null, null, null);                          


                string xml = _exportManager.ExportOrdersToXmlVismaUnpaid(orders);
                return xml;
            }
            catch (Exception exc)
            {
                return null;
            }
        }
        /// <summary>
        /// This method is called by Visma to update the payment-status of pending orders.
        /// Depending on different variables, Visma can direct this method to update the order in the proper way.
        /// This method can change the payment-status of the order, and additionally it can set or remove Expired-invoice flags on the customer and order.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ids">Split into 3 parts using ",". The first id is the Order-id. The remaining two are two different codes returned by Visma. These two codes are used to determine the proper update for the order.</param>
        /// <returns></returns>
        public string UpdatePaidOrders(string username, string password, string ids)
        {
            string orderId = ids;
            
            string s = ValidateCredentials(username, password);
            if (s != "Successful")
            {
                return s;
            }

            string paid = Request.QueryString["paid"];
            string checkPaid = Request.QueryString["checkPaid"];
            _logger.Debug("Attempting to update payment status for id: " + orderId + " Paid querystring: " + paid + " CheckPaid querystring: " + checkPaid);            
           
            Order order = _orderService.GetOrderById(Int32.Parse(orderId));
            
            Boolean custHasExpOrders = false;

            if (paid == "-1" && checkPaid == "-1")
            {
                //DOES NOT EXIST IN VISMA
            }
            if (paid == "-1" && checkPaid == "1")
            {
                //EXPIRED INVOICE
                order.Customer.HasExpiredInvoices = true;
                _customerService.UpdateCustomer(order.Customer);

                order.IsExpiredInvoice = true;
                _orderService.UpdateOrder(order);                
            }
            if (paid == "1" && checkPaid == "0")
            {
                //PAID
                order.PaymentStatus = Core.Domain.Payments.PaymentStatus.Paid;
                order.IsExpiredInvoice = false;
                _orderService.UpdateOrder(order);
                                
            }
            if (paid == "0" && checkPaid == "1")
            {
                //NOT PAID NOR EXPIRED
                order.IsExpiredInvoice = false;
                _orderService.UpdateOrder(order);                
            }


             //Check all orders if customer should go from hasExpiredOrders = true -> false

           
            
              Order latestOrder = null;
                var orders = _orderService.SearchOrders(0, 0, order.Customer.Id,
            null, null, null, null, null, null, null, 0, int.MaxValue, null, null, null);              

                foreach (Order ord in orders) //Get the last order
                {
                   if(ord.IsExpiredInvoice == true){
                        custHasExpOrders = true;
                    }
                }    
               
                if(custHasExpOrders){
                    order.Customer.HasExpiredInvoices = true;
                }
                else{
                    order.Customer.HasExpiredInvoices = false;
                }
                _customerService.UpdateCustomer(order.Customer);

            return "Hej";
        }
        /// <summary>
        /// This method is used by Visma to update the delivery-status of an order.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="id"></param>
        /// <param name="deliveryStatus"></param>
        /// <returns></returns>
        public string UpdateUnifaunStatus(string username, string password, string id, string trackID)
        {
            string s = ValidateCredentials(username, password);
            if (s != "Successful")
            {
                return s;
            }

            String returnStatus = "";

            Boolean validTrackId = false;

            if (trackID != "Send as letter")
            {

                string sPattern = "^[A-Za-z0-9]{5,25}$";
                if (System.Text.RegularExpressions.Regex.IsMatch(trackID, sPattern))
                {
                    validTrackId = true;
                }
                else
                {
                    returnStatus = "Orderkommentar (Unifaun kollinr) gick ej igenom validering, kollinummer har inte lagts till i Earstore.";
                }
            }
          
            Order order = _orderService.GetOrderById(Int32.Parse(id));

            //----If no shipment exists, create new-----
            if(order.Shipments.Count() == 0){

                if (1 == 1) //Create shipments for all orders
                {
                    var orderItems = order.OrderItems;

                    Shipment shipment = null;
                    decimal? totalWeight = null;
                    foreach (var orderItem in orderItems)
                    {

                        var orderItemTotalWeight = orderItem.ItemWeight.HasValue ? orderItem.ItemWeight * orderItem.Quantity : null;
                        if (orderItemTotalWeight.HasValue)
                        {
                            if (!totalWeight.HasValue)
                                totalWeight = 0;
                            totalWeight += orderItemTotalWeight.Value;
                        }
                        if (shipment == null)
                        {
                            var trackingNumber = "";
                            if (validTrackId)
                            {
                                 trackingNumber = trackID;
                            }
                          
                            shipment = new Shipment()
                            {
                                OrderId = order.Id,
                                TrackingNumber = trackingNumber,
                                TotalWeight = null,
                                ShippedDateUtc = null,
                                DeliveryDateUtc = null,
                                CreatedOnUtc = DateTime.UtcNow,
                            };
                        }
                        //create a shipment item
                        var shipmentItem = new ShipmentItem()
                        {
                            OrderItemId = orderItem.Id,
                            Quantity = orderItem.Quantity,
                        };
                        shipment.ShipmentItems.Add(shipmentItem);
                    }

                    //if we have at least one item in the shipment, then save it
                    if (shipment != null && shipment.ShipmentItems.Count > 0)
                    {
                        shipment.TotalWeight = totalWeight;
                        _shipmentService.InsertShipment(shipment);

                        try
                        {
                            _orderProcessingService.Ship(shipment, true);
                            order.OrderStatus = OrderStatus.Complete;                          
                            _orderService.UpdateOrder(order);
                        }
                        catch (Exception exc)
                        {
                            _logger.Debug("ERROR when creating shipment: " + exc.Message);
                            int error = 1;  //-----------UNDERSÖK DETTA---------------
                        }

                    }
                    else
                    {

                    }
                }
                else //------If a shipment already exists, atleast try to add tracking-number if existing.-----
                {
                    if (validTrackId)
                    {
                        foreach (Shipment shipm in order.Shipments) //Should only be 1 shipment really.
                        {
                            shipm.TrackingNumber = trackID;
                            _shipmentService.UpdateShipment(shipm);
                        }
                    }
                }
            }
           
             returnStatus = returnStatus + " Leveransstatus: [" + order.ShippingStatus.ToString() + "]";     
            
             returnStatus = returnStatus + " Antal shipments: ["+ order.Shipments.Count() + "]"; 
            

            return returnStatus;
        }

        public string VismaExportOrdersWithoutShipping(string username, string password)
        {
            string s = ValidateCredentials(username, password);
            if (s != "Successful")
            {
                return s;
            }

            try
            {
                //Get all orders which are awaiting a shipment
                var orders = _orderService.SearchOrders(0, 0, 0, null, null, null,
                    null, null, null, null, 0, int.MaxValue, null, null, null);


                string xml = _exportManager.ExportOrdersToXmlVismaWithoutShipping(orders);
                return xml;
            }
            catch (Exception exc)
            {
                return null;
            }
        }
    }
}
