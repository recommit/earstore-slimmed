﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Checkout;
using Nop.Web.Models.Common;
using Nop.Web.Models.ShoppingCart;
using Webpay.Integration.CSharp;
using Webpay.Integration.CSharp.WebpayWS;
using Webpay.Integration.CSharp.Order.Row;
using Webpay.Integration.CSharp.Util.Constant;
using Webpay.Integration.CSharp.Hosted.Helper;
using Webpay.Integration.CSharp.Order.Create;
using Webpay.Integration.CSharp.Config;
using Webpay.Integration.CSharp.Util.Security;
using System.Text;
using System.Net;
using System.IO;
using Webpay.Integration.CSharp.Response.Hosted;
using Nop.Services.Messages;
using Nop.Web.Models.Customer;
using Nop.Services.Authentication;


namespace Nop.Web.Controllers
{
    /// <summary>
    /// The order of different parts of the checkout has been changed to enable proper shipping method calculation required for Earstore 2.0. 
    /// The payment method part has been moved to occur before the shipping method, instead of after. 
    /// </summary>
    [NopHttpsRequirement(SslRequirement.Yes)]
    public partial class CheckoutController : BaseNopController
    {
		#region Fields

        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ILocalizationService _localizationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IShippingService _shippingService;
        private readonly IPaymentService _paymentService;
        private readonly IPluginFinder _pluginFinder;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly HttpContextBase _httpContext;
        private readonly IMobileDeviceHelper _mobileDeviceHelper;
        

        private readonly OrderSettings _orderSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PaymentSettings _paymentSettings;
        private readonly AddressSettings _addressSettings;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IAddressService _addressService;
        private readonly IAuthenticationService _authenticationService;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerRegistrationService _customerRegistrationService;

        #endregion

		#region Constructors

        public CheckoutController(IWorkContext workContext, IStoreContext storeContext,
            CustomerSettings customerSettings, IAuthenticationService authenticationService,
            IShoppingCartService shoppingCartService, ILocalizationService localizationService, 
            ITaxService taxService, ICurrencyService currencyService, 
            IPriceFormatter priceFormatter, IOrderProcessingService orderProcessingService,
            ICustomerService customerService,  IGenericAttributeService genericAttributeService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService, IShippingService shippingService, 
            IPaymentService paymentService, IPluginFinder pluginFinder,
            IOrderTotalCalculationService orderTotalCalculationService,
            ILogger logger, IOrderService orderService, IWebHelper webHelper,
            HttpContextBase httpContext, IMobileDeviceHelper mobileDeviceHelper,
            ICustomerRegistrationService customerRegistrationService,
            OrderSettings orderSettings, RewardPointsSettings rewardPointsSettings,
            PaymentSettings paymentSettings, AddressSettings addressSettings,IWorkflowMessageService workflowMessageService,IAddressService addressService)
        {
            this._customerRegistrationService = customerRegistrationService;
            this._customerSettings = customerSettings;
            this._authenticationService = authenticationService;
            this._workflowMessageService = workflowMessageService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._shoppingCartService = shoppingCartService;
            this._localizationService = localizationService;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._customerService = customerService;
            this._genericAttributeService = genericAttributeService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._shippingService = shippingService;
            this._paymentService = paymentService;
            this._pluginFinder = pluginFinder;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._logger = logger;
            this._orderService = orderService;
            this._webHelper = webHelper;
            this._httpContext = httpContext;
            this._mobileDeviceHelper = mobileDeviceHelper;

            this._orderSettings = orderSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._paymentSettings = paymentSettings;
            this._addressSettings = addressSettings;
            this._addressService = addressService;
        }

        #endregion





        private const string ExpectedResponseStart = "<html><head><SCRIPT LANGUAGE='JavaScript'>";



        public PaymentForm TriggerSveaPaymentRedirect(Order order)
        {
            MyConfigProd myConfig = new MyConfigProd();
            //MyConfigTest myConfig = new MyConfigTest();
          
            CountryCode countryCode = CountryCode.NONE;

            if (order.BillingAddress.Country.Name == "Sweden") { countryCode = CountryCode.SE; }
            if (order.BillingAddress.Country.Name == "Norway") { countryCode = CountryCode.NO; }
            if (order.BillingAddress.Country.Name == "Finland") { countryCode = CountryCode.FI; }
            if (order.BillingAddress.Country.Name == "Denmark") { countryCode = CountryCode.DK; }
            if (order.BillingAddress.Country.Name == "Germany") { countryCode = CountryCode.DE; }
            if (order.BillingAddress.Country.Name == "Netherlands") { countryCode = CountryCode.NL; }

            string auth = Request.Url.Authority;
            PaymentForm form = WebpayConnection.CreateOrder(myConfig)

                        .AddCustomerDetails(Item.IndividualCustomer()
                        .SetNationalIdNumber(order.Customer.SocialNumber)    //Required for individual customers in SE, NO, DK and FI
                //.SetBirthDate("19460509")               //Required for individual customers in NL and DE
                        .SetName(order.BillingAddress.FirstName, order.BillingAddress.LastName))             //Required for individual customers in NL and DE
                //.SetInitials("SB")                      //Required for individual customers in NL
                //.SetStreetAddress("Gatan", "23")            //Required in NL and DE
                //.SetCoAddress("c/o Eriksson")           //Optional
                //.SetZipCode("9999")                         //Required in NL and DE
                //.SetLocality("Stan")                    //Required in NL and DE
                //.SetPhoneNumber("999999")              //Optional
                // .SetEmail("test@svea.com")              //Optional but desirable
                // .SeCountryCodetIpAddress("123.123.123"))        //Optional but desirable
                .AddOrderRow(Item.OrderRow()
                .SetQuantity(1)                        //Required
                .SetAmountExVat(order.OrderTotal - order.OrderTax)               //Optional, see info above
                .SetAmountIncVat(order.OrderTotal)              //Optional, see info above
                //.SetVatPercent(0M)                 //Optional, see info above
                .SetArticleNumber("1")                 //Optional
                .SetDescription("Total kostnad för hela ordern")       //Optional
                .SetName("Hela ordern"))                   //Optional

                 .SetCountryCode(countryCode)                         //Required
                 .SetClientOrderNumber("" + order.Id)
                 .SetOrderDate(order.CreatedOnUtc)
                 .SetCurrency(Currency.SEK)
                 .UsePayPageCardOnly()

                
                .SetReturnUrl("http://" + auth + "/checkout/ReceiveXmlResponseFinishedPayment")
                .SetCallbackUrl("http://" + auth + "/checkout/CallbackUrl")
                .SetCancelUrl("http://" + auth + "/checkout/CancelUrl")
    
                    .GetPaymentForm();    

            return form;               
        }




        public ActionResult ReceiveXmlResponseFinishedPayment()
        {
            _logger.Debug("Response has been received from Svea (ReceiveXmlResponseFinishedPayment)");
            string result = "";
            string xmlString = "";          
            SveaResponse successfulObject = null;

            StreamReader reader = new StreamReader(Request.InputStream, Encoding.UTF8, true);
              
                
                string resp = Request.QueryString["response"];

                if (resp != "" && resp != null)
                {
                    _logger.Debug("querystring found for svea response");
                    xmlString = resp;
                }
                else
                {
                    _logger.Debug("querystring NOT found for svea response");
                }
                            

                //string xmlString2 = HttpUtility.UrlDecode(xmlString, Encoding.UTF8);
            string xmlString2 = xmlString;

            SveaResponse respObject = null;

            try
            {
                respObject = new SveaResponse(xmlString2);
                if (respObject != null)
                {
                    successfulObject = respObject;
                }
            }
            catch (Exception e)
            {
                _logger.Debug("Exception creating respObject");
            }

                xmlString2 = xmlString2.Replace("response=", "");

                string[] tokens = xmlString2.Split(new string[] { "&mac" }, StringSplitOptions.None);

                string s = "";
                string data = tokens[0];

                s = data.Trim().Replace(" ", "+");

                 respObject = null;

                try
                {
                    respObject = new SveaResponse(s);
                    if (respObject != null)
                    {
                        successfulObject = respObject;
                    }
                }
                catch (Exception e)
                {
                    _logger.Debug("Exception creating respObject");
                }


                if (s.Length % 4 > 0)
                    s = s.PadRight(s.Length + 4 - s.Length % 4, '=');
                //what = Encoding.UTF8.GetString(Convert.FromBase64String(s));

                 respObject = null;

                try
                {
                    respObject = new SveaResponse(s);
                    if (respObject != null)
                    {
                        successfulObject = respObject;
                    }
                }
                catch (Exception e) {
                    _logger.Debug("Exception creating respObject");
                }

            //-------------------------------------------------------------------------

                if (successfulObject == null)
                {

                    resp = Request.QueryString["response"];

                    if (resp != "" && resp != null)
                    {
                        _logger.Debug("querystring found for svea response");
                        xmlString = resp;
                    }
                    else
                    {
                        _logger.Debug("querystring NOT found for svea response");
                    }


                     xmlString2 = HttpUtility.UrlDecode(xmlString, Encoding.UTF8);
                    //string xmlString2 = xmlString;
                    xmlString2 = xmlString2.Replace("response=", "");

                   tokens = xmlString2.Split(new string[] { "&mac" }, StringSplitOptions.None);

                     s = "";
                     data = tokens[0];

                    s = data.Trim().Replace(" ", "+");
                    if (s.Length % 4 > 0)
                        s = s.PadRight(s.Length + 4 - s.Length % 4, '=');
                    //what = Encoding.UTF8.GetString(Convert.FromBase64String(s));

                     respObject = null;

                    try
                    {                        
                        respObject = new SveaResponse(s);
                        if (respObject != null)
                        {
                            successfulObject = respObject;
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.Debug("Exception creating respObject");
                    }
                }
            //-------------------------------------------------------------------------

                if (successfulObject == null)
                {
                    if (!reader.EndOfStream)
                    {
                        try
                        {
                            xmlString = reader.ReadToEnd();
                            _logger.Debug("Stream found for svea response");
                        }
                        catch (Exception e)
                        {
                            _logger.Debug("Exception reading the xml file.");
                        }
                    }

                     xmlString2 = HttpUtility.UrlDecode(xmlString, Encoding.UTF8);
                    //string xmlString2 = xmlString;
                    xmlString2 = xmlString2.Replace("response=", "");

                   tokens = xmlString2.Split(new string[] { "&mac" }, StringSplitOptions.None);

                     s = "";
                     data = tokens[0];

                    s = data.Trim().Replace(" ", "+");
                    if (s.Length % 4 > 0)
                        s = s.PadRight(s.Length + 4 - s.Length % 4, '=');
                    //what = Encoding.UTF8.GetString(Convert.FromBase64String(s));

                     respObject = null;

                    try
                    {                        
                        respObject = new SveaResponse(s);
                        if (respObject != null)
                        {
                            successfulObject = respObject;
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.Debug("Exception creating respObject");
                    }
                }               



                if (successfulObject != null)
                {
                    _logger.Debug("<---------restObject is not null----------->");
                    if (respObject.OrderAccepted == true)
                    {
                        _logger.Debug("Order was accepted. ClientOrderNumber: " + respObject.ClientOrderNumber + " TransactionId: " + respObject.TransactionId + " CardType" + respObject.CardType + " Amount" + respObject.Amount + " Currency" + respObject.Currency + " ResultCode" + respObject.ResultCode);
                        Order order = _orderService.GetOrderById(Int32.Parse(respObject.ClientOrderNumber));
                        _orderProcessingService.MarkOrderAsPaid(order);
                        _orderService.UpdateOrder(order);
                        removeNewCustomerStatus();

                        int orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId);
                        if (orderPlacedCustomerNotificationQueuedEmailId > 0)
                        {
                            order.OrderNotes.Add(new OrderNote()
                            {
                                Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
                                DisplayToCustomer = false,
                                CreatedOnUtc = DateTime.UtcNow
                            });
                            _orderService.UpdateOrder(order);
                        }
                        _logger.Debug("Order has been set as paid and confirmation mail has been sent to customer.");                                             
                        

                        return RedirectToRoute("CheckoutCompleted", new { orderId = Int32.Parse(respObject.ClientOrderNumber) });
                    }
                    else
                    {
                        _logger.Debug("Order was NOT accepted. ClientOrderNumber: " + respObject.ClientOrderNumber + " TransactionId: " + respObject.TransactionId + " CardType" + respObject.CardType + " Amount" + respObject.Amount + " Currency" + respObject.Currency + " ResultCode" + respObject.ResultCode + " ErrorMessage" + respObject.ErrorMessage);

                        return RedirectToRoute("OrderDetails", new { orderId = Int32.Parse(respObject.ClientOrderNumber) }); // Ändra till route
                    }
                }
                else
                {
                    _logger.Debug("Could not proces response, resoObject = NULL <-------------FATAL----------------->");
                    string auth = Request.Url.Authority;
                    return Redirect("http://" + auth + "/customer/OrdersWithMessage");
                    
                }
     

        }

        public void removeNewCustomerStatus()
        {
            var customer = new Customer();
            try
            {
                Boolean hasNewCustomerRole = false;

                customer = _workContext.CurrentCustomer;

                CustomerRole toRemove = new CustomerRole();

                foreach (CustomerRole cr in customer.CustomerRoles)
                {
                    if (cr.Name.ToLower() == "ny kund")
                    {
                        toRemove = cr;
                        hasNewCustomerRole = true;
                        break;
                    }
                }
                customer.CustomerRoles.Remove(toRemove);
                _customerService.UpdateCustomer(customer);

                if (hasNewCustomerRole)
                {
                    _logger.Debug("Successfully removed Ny Kund role on customer " + customer.Email);
                }
            }
            catch (Exception e)
            {
                _logger.Error("Something went wrong when trying to remove Ny Kund role on customer " + customer.Email,e,customer);
            }            
        }

        public void logSveaPaymentRedirectCheckout(int id)
        {
            Order theOrder = _orderService.GetOrderById(id);
            _logger.Debug("Svea Payment button has been clicked for order: " + id + " Amount: " + theOrder.OrderTotal);
        }
       
    //    public string TestDoCardPaymentRequest()
    //    {
    //        MyConfigTest myConfig = new MyConfigTest();

    ////        PaymentForm form = WebpayConnection.CreateOrder(myConfig)     //See Configuration chapt.3

    ////            //For all products and other items
    ////               .AddOrderRow(Item.OrderRow()
    ////        .SetQuantity(2)                        //Required
    ////        .SetAmountExVat(100.00M)               //Optional, see info above
    ////        .SetAmountIncVat(125.00M)              //Optional, see info above
    ////        .SetVatPercent(25.00M)                 //Optional, see info above
    ////        .SetArticleNumber("1")                 //Optional
    ////        .SetDescription("Specification")       //Optional
    ////        .SetName("Prod")                       //Optional
    ////        .SetUnit("st")                         //Optional              
    ////        .SetDiscountPercent(0))                //Optional   

    ////        //If shipping fee
    ////                .AddFee(Item.ShippingFee()
    ////        .SetAmountExVat(50)                    //Optional, see info above
    ////        .SetAmountIncVat(62.50M)                //Optional, see info above
    ////        .SetVatPercent(25.00M)                  //Optional, see info above
    ////        .SetShippingId("33")                   //Optional
    ////        .SetName("shipping")                   //Optional
    ////        .SetDescription("Specification")       //Optional
    ////        .SetUnit("st")                         //Optional             
    ////        .SetDiscountPercent(0))                //Optional
    ////            //If invoice with invoice fee

    ////                 .AddFee(Item.InvoiceFee()
    ////        .SetAmountExVat(50)                    //Optional, see info above
    ////        .SetAmountIncVat(62.50M)                //Optional, see info above
    ////        .SetVatPercent(25.00M)                 //Optional, see info above
    ////        .SetName("Svea fee")                   //Optional
    ////        .SetDescription("Fee for invoice")     //Optional       
    ////        .SetUnit("st")                         //Optional
    ////        .SetDiscountPercent(0))                //Optional  
    ////            //If discount or coupon with fixed amount
    ////        .AddDiscount(Item.FixedDiscount()
    ////        .SetAmountIncVat(100.00M))              //Required
    ////            //If discount or coupon with percent discount
    ////        .AddDiscount(Item.RelativeDiscount()
    ////        .SetDiscountPercent(50))              //Required
    ////            //Individual customer values
    ////        .AddCustomerDetails(Item.IndividualCustomer()
    ////        .SetNationalIdNumber("194605092222")    //Required for individual customers in SE, NO, DK and FI
    ////        .SetBirthDate("19460509")               //Required for individual customers in NL and DE
    ////        .SetName("Tess", "Testson")             //Required for individual customers in NL and DE
    ////        .SetInitials("SB")                      //Required for individual customers in NL
    ////        .SetStreetAddress("Gatan", "23")            //Required in NL and DE
    ////        .SetCoAddress("c/o Eriksson")           //Optional
    ////        .SetZipCode("9999")                         //Required in NL and DE
    ////        .SetLocality("Stan")                    //Required in NL and DE
    ////        .SetPhoneNumber("999999")              //Optional
    ////        .SetEmail("test@svea.com")              //Optional but desirable
    ////        .SetIpAddress("123.123.123"))        //Optional but desirable
    ////            //Company customer values

    ////        //Other values
    ////        .SetCountryCode(CountryCode.SE)             //Required
    ////        .SetCurrency(Currency.SEK)                      //Required for card payment, direct payment and PayPage payment.
    ////        .SetClientOrderNumber("33")                 //Required for card payment, direct payment, PaymentMethod payment and PayPage payments. Must be uniqe.
    ////        .SetOrderDate(new DateTime(2012, 12, 12))   //Required for synchronous payments
    ////        .SetCustomerReference("ref33")              //Optional

    ////                    //Continue by choosing one of the following paths
    ////            //Continue as a card payment
    ////        .UsePayPageCardOnly()
    ////       .SetReturnUrl("http://localhost:15536/checkout/ReceiveXmlResponse")                    //Required
    ////            .SetCallbackUrl("http://localhost:15536/checkout/ReceiveXmlResponse")                      //Optional
    ////            .SetCancelUrl("http://localhost:15536/checkout/ReceiveXmlResponse")                    //Optional
    ////.GetPaymentForm();




    //                PaymentForm form = WebpayConnection.CreateOrder(myConfig)
    //    .AddOrderRow(Item.OrderRow()
    //        .SetArticleNumber("1")
    //        .SetName("Prod")
    //        .SetDescription("Specification")
    //        .SetQuantity(2)
    //        .SetUnit("st")
    //        .SetAmountExVat(100.00M)
    //        .SetVatPercent(25.00M)
    //        .SetDiscountPercent(0))

    //    .SetCountryCode(CountryCode.SE)                         //Required
    //    .SetClientOrderNumber("50")
    //    .SetOrderDate(new DateTime(2012, 12, 12))
    //    .SetCurrency(Currency.SEK)
    //    .UsePayPageCardOnly()
    //        //.SetReturnUrl("http://localhost:15536/checkout/ReceiveXmlResponseFinishedPaymentTest") 
    //        //.SetCallbackUrl("http://localhost:15536/checkout/CallbackUrl")                      //Optional
    //        //            .SetCancelUrl("http://localhost:15536/checkout/CancelUrl")                    //Optional
            
    //                  .SetReturnUrl("http://testnopcommerce.recommit.se/checkout/ReceiveXmlResponseFinishedPayment")
    //        .SetCallbackUrl("http://testnopcommerce.recommit.se/checkout/CallbackUrl")                      //Optional
    //                    .SetCancelUrl("http://testnopcommerce.recommit.se/checkout/CancelUrl")                    //Optional
    //        .GetPaymentForm();

    //                return form.GetCompleteForm();


    //       // var postResponse = PostRequest(form);

    //        //Assert.That(postResponse.Item1, Is.EqualTo("OK"));
            
    //    }


        //public string ReceiveXmlResponseFinishedPaymentTest()
        //{
        //    string result = "";

        //    StreamReader reader = new StreamReader(Request.InputStream,Encoding.UTF8, true);

        //    string xmlString = "";

        //    if (!reader.EndOfStream)
        //    {
        //        try
        //        {
        //             xmlString = reader.ReadToEnd();
        //        }
        //        catch (Exception e) { }
        //    }
            
        //   string xmlString2 = HttpUtility.UrlDecode(xmlString, Encoding.UTF8);

        //  xmlString2 =  xmlString2.Replace("response=", "");

        //   string[] tokens = xmlString2.Split(new string[] { "&mac" }, StringSplitOptions.None);
                        
        //   string s = "";   
        //   string data = tokens[0];

        //   s = data.Trim().Replace(" ", "+");
        //   if (s.Length % 4 > 0)
        //       s = s.PadRight(s.Length + 4 - s.Length % 4, '=');
        //    //what = Encoding.UTF8.GetString(Convert.FromBase64String(s));

        //    SveaResponse respObject = null;

        //   try
        //   {
        //       respObject = new SveaResponse(s);               
        //       int i = 0;
        //   }
        //   catch (Exception e) { }

        //   if (respObject != null)
        //   {
        //       if (respObject.OrderAccepted == true)
        //       {
        //           result = "Payment Successful";
        //       }
        //       else
        //       {              
        //           result = "Payment failed";
        //       }
        //   }
        //   else
        //   {
        //       result = "Error, no response from payment site";
        //   }

        //   return result;
            
        //}

        public void CallbackUrl()
        {
            _logger.Debug("(CallbackUrl) Response has been received from Svea. This method should only be called if default response fails.");

            try
            {                
                string result = "";
              
                SveaResponse successfulObject = null;

                StreamReader reader = new StreamReader(Request.InputStream, Encoding.UTF8, true);


                string resp = Request.QueryString["response"];

                if (resp != "" && resp != null)
                {
                    _logger.Debug("querystring found for svea response: " + resp);                 
                }
                else
                {
                    _logger.Debug("querystring NOT found for svea response");
                }
            }
            catch (Exception e)
            {
                _logger.Debug("(CallbackUrl) Exception parsing querystring response");
            }
        }

        public ActionResult CancelUrl()        
        {
            string customerNo = Request.QueryString["customerrefno"];
            _logger.Debug("(CancelUrl) reached. Payment of order with ID: " + customerNo +" Has been cancelled");
            return RedirectToRoute("OrderDetails", new { orderId = customerNo });        
        }
                 
        #region Utilities

        [NonAction]
        protected bool IsPaymentWorkflowRequired(IList<ShoppingCartItem> cart, bool ignoreRewardPoints = false)
        {
            bool result = true;

            //check whether order total equals zero
            decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart, ignoreRewardPoints);
            if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value == decimal.Zero)
                result = false;
            return result;
        }

        [NonAction]
        protected CheckoutBillingAddressModel PrepareBillingAddressModel(int? selectedCountryId = null)
        {        
            var model = new CheckoutBillingAddressModel();
            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses.Where(a => a.Country == null || a.Country.AllowsBilling).ToList();
            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                addressModel.PrepareModel(address, 
                    false, 
                    _addressSettings);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            model.NewAddress.PrepareModel(null,
                false,
                _addressSettings,
                _localizationService,
                _stateProvinceService,
                () => _countryService.GetAllCountriesForBilling());
            return model;
        }

        [NonAction]
        protected CheckoutShippingAddressModel PrepareShippingAddressModel(int? selectedCountryId = null)
        {
            var model = new CheckoutShippingAddressModel();
            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses.Where(a => a.Country == null || a.Country.AllowsShipping).ToList();
            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                addressModel.PrepareModel(address,
                    false,
                    _addressSettings);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            model.NewAddress.PrepareModel(null,
                false,
                _addressSettings,
                _localizationService,
                _stateProvinceService,
                () => _countryService.GetAllCountriesForShipping());
            return model;
        }

        /// <summary>
        /// This method prepares the list of available shipping methods.
        /// It has been modified to send various parameters to the service for retrieving the shipping methods, to fit Earstore:s needs.
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        [NonAction]
        protected CheckoutShippingMethodModel PrepareShippingMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutShippingMethodModel();

            //--------------------------Edited--------------------------------
            //Modified this section to include totalprice of order in calculation of available shipping methods

            string selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
    SystemCustomerAttributeNames.SelectedPaymentMethod,
    _genericAttributeService, _storeContext.CurrentStore.Id);

            string totalPrice = _workContext.CurrentCustomer.GetAttribute<string>(
    "totalprice", _genericAttributeService, _storeContext.CurrentStore.Id);             

            var getShippingOptionResponse = _shippingService
                .GetShippingOptionsModified(cart, _workContext.CurrentCustomer.ShippingAddress,
                "", _storeContext.CurrentStore.Id, selectedPaymentMethodSystemName, totalPrice);

            //--------------------------Edited--------------------------------


            if (getShippingOptionResponse.Success)
            {
                //performance optimization. cache returned shipping options.
                //we'll use them later (after a customer has selected an option).
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, 
                    SystemCustomerAttributeNames.OfferedShippingOptions, 
                    getShippingOptionResponse.ShippingOptions,
                    _storeContext.CurrentStore.Id);
            
                foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                {
                    var soModel = new CheckoutShippingMethodModel.ShippingMethodModel()
                    {
                        Name = shippingOption.Name,
                        Description = shippingOption.Description,
                        ShippingRateComputationMethodSystemName = shippingOption.ShippingRateComputationMethodSystemName,
                    };

                    //adjust rate
                    Discount appliedDiscount = null;
                    var shippingTotal = _orderTotalCalculationService.AdjustShippingRate(
                        shippingOption.Rate, cart, out appliedDiscount);

                    decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                    decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                    soModel.Fee = _priceFormatter.FormatShippingPrice(rate, true);

                    model.ShippingMethods.Add(soModel);
                }

                //find a selected (previously) shipping method
                var selectedShippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                if (selectedShippingOption != null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.ToList()
                        .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedShippingOption.Name, StringComparison.InvariantCultureIgnoreCase) &&
                        !String.IsNullOrEmpty(so.ShippingRateComputationMethodSystemName) && so.ShippingRateComputationMethodSystemName.Equals(selectedShippingOption.ShippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
                //if no option has been selected, let's do it for the first one
                if (model.ShippingMethods.FirstOrDefault(so => so.Selected) == null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.FirstOrDefault();
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
            }
            else
                foreach (var error in getShippingOptionResponse.Errors)
                    model.Warnings.Add(error);

            return model;
        }

        /// <summary>
        /// This method has been modified to only allow card payment when delivery address is not swedish, or when a socialnumber is not set for the customer.
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        [NonAction]
        protected CheckoutPaymentMethodModel PreparePaymentMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutPaymentMethodModel();

            //reward points
            if (_rewardPointsSettings.Enabled && !cart.IsRecurring())
            {
                int rewardPointsBalance = _workContext.CurrentCustomer.GetRewardPointsBalance();
                decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
                decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
                if (rewardPointsAmount > decimal.Zero && 
                    _orderTotalCalculationService.CheckMinimumRewardPointsToUseRequirement(rewardPointsBalance))
                {
                    model.DisplayRewardPoints = true;
                    model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
                    model.RewardPointsBalance = rewardPointsBalance;
                }
            }

            var boundPaymentMethods = _paymentService
                .LoadActivePaymentMethods(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id)
                .Where(pm => pm.PaymentMethodType == Nop.Services.Payments.PaymentMethodType.Standard || pm.PaymentMethodType == Nop.Services.Payments.PaymentMethodType.Redirection)
                .ToList();
            foreach (var pm in boundPaymentMethods)
            {
                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                var pmModel = new CheckoutPaymentMethodModel.PaymentMethodModel()
                {
                    Name = pm.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id),
                    PaymentMethodSystemName = pm.PluginDescriptor.SystemName,
                };
                //payment method additional fee
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, pm.PluginDescriptor.SystemName);
                decimal rateBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, _workContext.CurrentCustomer);
                decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                if (rate > decimal.Zero)
                    pmModel.Fee = _priceFormatter.FormatPaymentMethodAdditionalFee(rate, true);

                model.PaymentMethods.Add(pmModel);

                //-----------------------------------EDITED--------------------------
                model.forceCardPayment = false;
                model.missingSocialNumber = false;
                model.notSwedenDelivery = false;

                if (_workContext.CurrentCustomer.BillingAddress.Country.Name != "Sweden")
                {
                    model.notSwedenDelivery = true;
                    model.forceCardPayment = true;
                }
               
                if ((_workContext.CurrentCustomer.SocialNumber == "" || _workContext.CurrentCustomer.SocialNumber == null))
                {
                    model.missingSocialNumber = true;
                    model.forceCardPayment = true; 
                }
                //-----------------------------------EDITED--------------------------
            }
            
            //find a selected (previously) payment method
            var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            if (!String.IsNullOrEmpty(selectedPaymentMethodSystemName))
            {
                var paymentMethodToSelect = model.PaymentMethods.ToList()
                    .Find(pm => pm.PaymentMethodSystemName.Equals(selectedPaymentMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }
            //if no option has been selected, let's do it for the first one
            if (model.PaymentMethods.FirstOrDefault(so => so.Selected) == null)
            {
                var paymentMethodToSelect = model.PaymentMethods.FirstOrDefault();
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }

            return model;
        }

        [NonAction]
        protected CheckoutPaymentInfoModel PreparePaymentInfoModel(IPaymentMethod paymentMethod)
        {
            var model = new CheckoutPaymentInfoModel();
            string actionName;
            string controllerName;
            RouteValueDictionary routeValues;
            paymentMethod.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);
            model.PaymentInfoActionName = actionName;
            model.PaymentInfoControllerName = controllerName;
            model.PaymentInfoRouteValues = routeValues;
            model.DisplayOrderTotals = _orderSettings.OnePageCheckoutDisplayOrderTotalsOnPaymentInfoTab;
            return model;
        }

        [NonAction]
        protected CheckoutConfirmModel PrepareConfirmOrderModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutConfirmModel();
            //min order amount validation
            bool minOrderTotalAmountOk = _orderProcessingService.ValidateMinOrderTotalAmount(cart);
            if (!minOrderTotalAmountOk)
            {
                decimal minOrderTotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderTotalAmount, _workContext.WorkingCurrency);
                model.MinOrderTotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), _priceFormatter.FormatPrice(minOrderTotalAmount, true, false));
            }
            return model;
        }

        [NonAction]
        protected bool UseOnePageCheckout()
        {
            bool useMobileDevice = _mobileDeviceHelper.IsMobileDevice(_httpContext)
                && _mobileDeviceHelper.MobileDevicesSupported()
                && !_mobileDeviceHelper.CustomerDontUseMobileVersion();

            //mobile version doesn't support one-page checkout
            if (useMobileDevice)
                return false;

            //check the appropriate setting
            return _orderSettings.OnePageCheckoutEnabled;
        }

        [NonAction]
        protected bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            //prevent 2 orders being placed within an X seconds time frame
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrder = _orderService.SearchOrders(_storeContext.CurrentStore.Id, 0, _workContext.CurrentCustomer.Id,
                null, null, null, null, null, null, null, 0, 1, null, null, null)
                .FirstOrDefault();
            if (lastOrder == null)
                return true;

            var interval = DateTime.UtcNow - lastOrder.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }

        #endregion

        #region Methods (common)

        public ActionResult Index()
        {            
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //reset checkout data
            _customerService.ResetCheckoutData(_workContext.CurrentCustomer, _storeContext.CurrentStore.Id);

            //validation (cart)
            var checkoutAttributesXml = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService);
            var scWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributesXml, true);
            if (scWarnings.Count > 0)
                return RedirectToRoute("ShoppingCart");
            //validation (each shopping cart item)
            foreach (ShoppingCartItem sci in cart)
            {
                var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(_workContext.CurrentCustomer,
                    sci.ShoppingCartType,
                    sci.Product,
                    sci.StoreId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.Quantity,
                    false);
                if (sciWarnings.Count > 0)
                    return RedirectToRoute("ShoppingCart");
            }

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");
            else
                return RedirectToRoute("CheckoutBillingAddress");
        }

        public ActionResult Completed(int? orderId)
        {
            //validation
            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            Order order = null;
            if (orderId.HasValue)
            {
                //load order by identifier (if provided)
                order = _orderService.GetOrderById(orderId.Value);
            }
            if (order == null)
            {
                order = _orderService.SearchOrders(_storeContext.CurrentStore.Id, 0, _workContext.CurrentCustomer.Id,
                    null, null, null, null, null, null, null, 0, 1, null, null, null)
                    .FirstOrDefault();
            }
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
            {
                return RedirectToRoute("HomePage");
            }

            //disable "order completed" page?
            if (_orderSettings.DisableOrderCompletedPage)
            {
                return RedirectToRoute("OrderDetails", new {orderId = order.Id});
            }

            //model
            var model = new CheckoutCompletedModel()
            {
                OrderId = order.Id,
                OnePageCheckoutEnabled = UseOnePageCheckout()
            };


            string paymentMethod = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);


            if (paymentMethod == "Payments.Manual")
            {
                var what = true;
            }   

            return View(model);
        }

        #endregion

        #region Methods (multistep checkout)

        public ActionResult UpdateBillingAddress(CustomerAddressEditModel model)
        {          
            var customer = _workContext.CurrentCustomer;   
            var address = customer.Addresses.FirstOrDefault(a => a.Id == model.Address.Id);
            if (address == null)           
                return RedirectToRoute("CheckoutBillingAddress");

            if (ModelState.IsValid)
            {
                address = model.Address.ToEntity(address);
                _addressService.UpdateAddress(address);

                //SECTION ADD UPDATED DATA TO CUSTOMER ADDRESS
                if (model.Address.Id == Int32.Parse(customer.DefaultAdress))
                {
                    if (!customer.Email.Equals(model.Address.Email.Trim(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        //change email
                        _customerRegistrationService.SetEmail(customer, model.Address.Email.Trim());
                        //re-authenticate (if usernames are disabled)
                        if (!_customerSettings.UsernamesEnabled)
                        {
                            _authenticationService.SignIn(customer, true);
                        }
                    }

                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.Address.FirstName);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.Address.LastName);

                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Address.Company);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.Address.Address1);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.Address.Address2);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.Address.ZipPostalCode);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.Address.City);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.Address.CountryId);
                    if (_customerSettings.PhoneEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Address.PhoneNumber);

                }


            }  
            return RedirectToRoute("CheckoutBillingAddress");       
        }

        public ActionResult UpdateShippingAddress(CustomerAddressEditModel model)
        {
            var customer = _workContext.CurrentCustomer;            
            var address = customer.Addresses.FirstOrDefault(a => a.Id == model.Address.Id);
            if (address == null)                
                return RedirectToRoute("CheckoutShippingAddress");

            if (ModelState.IsValid)
            {
                address = model.Address.ToEntity(address);
                _addressService.UpdateAddress(address);

                //SECTION ADD UPDATED DATA TO CUSTOMER ADDRESS
                if (model.Address.Id == Int32.Parse(customer.DefaultAdress))
                {
                    if (!customer.Email.Equals(model.Address.Email.Trim(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        //change email
                        _customerRegistrationService.SetEmail(customer, model.Address.Email.Trim());
                        //re-authenticate (if usernames are disabled)
                        if (!_customerSettings.UsernamesEnabled)
                        {
                            _authenticationService.SignIn(customer, true);
                        }
                    }

                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.Address.FirstName);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.Address.LastName);

                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Address.Company);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.Address.Address1);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.Address.Address2);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.Address.ZipPostalCode);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.Address.City);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.Address.CountryId);
                    if (_customerSettings.PhoneEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Address.PhoneNumber);

                }
            }
            return RedirectToRoute("CheckoutShippingAddress");
        }

        public ActionResult AddressDeleteCheckout(int addressId, int returnPage)
        {           

            var customer = _workContext.CurrentCustomer;

            //find address (ensure that it belongs to the current customer)
            var address = customer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address != null)
            {
                if (customer.DefaultAdress == address.Id + "")
                {
                    ErrorNotification("Detta är din standard-adress, du kan inte ta bort den. Du kan istället uppdatera adressinformationen för den här addressen.");
                }
                else
                {
                    int existingBilling = 0;
                    int existingShipping = 0;
                    string errorMessage = "Adressen borttagen.";   
                    string caseReturnPage = "";
                    
                    if (customer.BillingAddress != null)
                    {
                        existingBilling = customer.BillingAddress.Id;
                    }
                    if (customer.ShippingAddress != null)
                    {
                        existingShipping = customer.ShippingAddress.Id;
                    }
                    if (address.Id == existingShipping)
                    {
                        customer.ShippingAddress = null;
                        caseReturnPage = "shipping";
                        errorMessage = "Adressen användes som leveransadress för din order, var god välj eller skapa en ny leveransadress.";
                    }
                    if (address.Id == existingBilling)
                    {
                        customer.BillingAddress = null;
                        caseReturnPage = "billing";
                        errorMessage = "Adressen användes som faktura-adress för din order, var god välj eller skapa en ny faktura-adress.";
                    } 
                        if (address.Id == existingShipping && address.Id == existingBilling )
                    {
                        caseReturnPage = "billing";
                        customer.BillingAddress = null;
                        customer.ShippingAddress = null;
                        errorMessage = "Adressen användes som faktura och leverans-adress för din order, var god välj eller skapa nya adresser.";
                    }                                                
                        customer.RemoveAddress(address);
                        _customerService.UpdateCustomer(customer);
                        //now delete the address record
                        _addressService.DeleteAddress(address);
                        SuccessNotification(errorMessage);                         

                        if (caseReturnPage == "billing")
                        {
                            return RedirectToRoute("CheckoutBillingAddress");
                        }
                        if (caseReturnPage == "shipping")
                        {
                            return RedirectToRoute("CheckoutShippingAddress");
                        }                    
                }
            }
            if (returnPage == 1)
            {
                return RedirectToRoute("CheckoutBillingAddress");
            }
            else
            {
                return RedirectToRoute("CheckoutShippingAddress");
            }
        }


        public ActionResult BillingAddress()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareBillingAddressModel();
            return View(model);
        }
        public ActionResult SelectBillingAddress(int addressId)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutBillingAddress");

            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            return RedirectToRoute("CheckoutShippingAddress");
        }

        
        public ActionResult SelectBillingAddressAndShipping(int addressId)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutBillingAddress");

            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            return SelectShippingAddress(addressId);  //Edited workflow, skip a step if possible          
        }



         
        [HttpPost, ActionName("BillingAddress")]
        [FormValueRequired("nextstep")]
        public ActionResult NewBillingAddress(CheckoutBillingAddressModel model, FormCollection formColl)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (ModelState.IsValid)
            {
                var address = model.NewAddress.ToEntity();
                address.CreatedOnUtc = DateTime.UtcNow;
                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;
                _workContext.CurrentCustomer.Addresses.Add(address);
                _workContext.CurrentCustomer.BillingAddress = address;

                if (formColl.GetValue("useSameAddressCheckbox3") == null)  //Reverse logic
                {                  
                    return SelectShippingAddress(address.Id); //Edited workflow, skip a step if possible
                }

                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                return RedirectToRoute("CheckoutShippingAddress");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareBillingAddressModel(model.NewAddress.CountryId);
            return View(model);
        }

        public ActionResult ShippingAddress()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //model
            var model = PrepareShippingAddressModel();
            return View(model);
        }
        public ActionResult SelectShippingAddress(int addressId)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutShippingAddress");

            _workContext.CurrentCustomer.ShippingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            //return RedirectToRoute("CheckoutShippingMethod");
            return RedirectToRoute("CheckoutPaymentMethod");  //-----------------------------EDITED WORFLOW--------------------
        }
        [HttpPost, ActionName("ShippingAddress")]
        [FormValueRequired("nextstep")]
        public ActionResult NewShippingAddress(CheckoutShippingAddressModel model)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutPaymentMethod");  //-----------------------------EDITED WORFLOW--------------------
                //return RedirectToRoute("CheckoutShippingMethod");
            }

            if (ModelState.IsValid)
            {
                var address = model.NewAddress.ToEntity();
                address.CreatedOnUtc = DateTime.UtcNow;
                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;
                _workContext.CurrentCustomer.Addresses.Add(address);
                _workContext.CurrentCustomer.ShippingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                //return RedirectToRoute("CheckoutShippingMethod");               
                return RedirectToRoute("CheckoutPaymentMethod");  //-----------------------------EDITED WORFLOW--------------------
            }


            //If we got this far, something failed, redisplay form
            model = PrepareShippingAddressModel(model.NewAddress.CountryId);
            return View(model);
        }
        

        public ActionResult ShippingMethod()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentMethod");
            }
            
            
            //model
            var model = PrepareShippingMethodModel(cart);
            return View(model);
        }
        [HttpPost, ActionName("ShippingMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult SelectShippingMethod(string shippingoption)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //parse selected method 
            if (String.IsNullOrEmpty(shippingoption))
                return ShippingMethod();
            var splittedOption = shippingoption.Split(new string[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
            if (splittedOption.Length != 2)
                return ShippingMethod();
            string selectedName = splittedOption[0];
            string shippingRateComputationMethodSystemName = splittedOption[1];
            
            //find it
            //performance optimization. try cache first
            var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
            if (shippingOptions == null || shippingOptions.Count == 0)
            {
                //not found? let's load them using shipping service
                shippingOptions = _shippingService
                    .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id)
                    .ShippingOptions
                    .ToList();
            }
            else
            {
                //loaded cached results. let's filter result by a chosen shipping rate computation method
                shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    .ToList();
            }

            var shippingOption = shippingOptions
                .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
            if (shippingOption == null)
                return ShippingMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);

            return RedirectToRoute("CheckoutConfirm"); 
            //return RedirectToRoute("CheckoutPaymentMethod"); //------------EDITED-----------
        }
        [HttpPost, ActionName("AddSocialnumberCheckout")]
        public ActionResult AddSocialnumberCheckout(FormCollection form)
        {
            if (form["AddSocialNumber"] != null)
            {
                string sn = form["AddSocialNumber"];
                //_workContext.CurrentCustomer.SocialNumber = model.AddSocialNumber;
                Customer c = _customerService.GetCustomerById(_workContext.CurrentCustomer.Id);
                c.SocialNumber = sn;
                _customerService.UpdateCustomer(c);            
            }

            
            return RedirectToAction("PaymentMethod");
        }
        
        
        public ActionResult PaymentMethod()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            //model
            var paymentMethodModel = PreparePaymentMethodModel(cart);

            if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
            {
                //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                //so customer doesn't have to choose a payment method

                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, 
                    paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName,
                    _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            return View(paymentMethodModel);
        }
        [HttpPost, ActionName("PaymentMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult SelectPaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //reward points
            if (_rewardPointsSettings.Enabled)
            {
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, model.UseRewardPoints,
                    _storeContext.CurrentStore.Id);
            }



               

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired && paymentmethod != "Payments.Manual")
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }
            //payment method 
            if (String.IsNullOrEmpty(paymentmethod))
                return PaymentMethod();

            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
            if (paymentMethodInst == null || 
                !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                return PaymentMethod();

            //save
            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);

            //If card payment, we need to delay the entering of card information until the order has been placed.
            //The orderID received after order has been crated needs to be sent to SveaWebPay.
            if (paymentmethod == "Payments.Manual")
            {
                return RedirectToRoute("CheckoutShippingMethod");
            }      
            
            return RedirectToRoute("CheckoutPaymentInfo");
        }


        public ActionResult PaymentInfo()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutShippingMethod");   //--------------EDITED WORKFLOW------------------
                //return RedirectToRoute("CheckoutConfirm");
            }

            return RedirectToRoute("CheckoutShippingMethod");   //--------------EDITED WORKFLOW------------------

            //load payment method
            var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            //model
            var model = PreparePaymentInfoModel(paymentMethod);
            return View(model);
        }
        [HttpPost, ActionName("PaymentInfo")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult EnterPaymentInfo(FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            var paymentControllerType = paymentMethod.GetControllerType();
            var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BaseNopPaymentController;
            var warnings = paymentController.ValidatePaymentForm(form);
            foreach (var warning in warnings)
                ModelState.AddModelError("", warning);
            if (ModelState.IsValid)
            {
                //get payment info
                var paymentInfo = paymentController.GetPaymentInfo(form);
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;
                return RedirectToRoute("CheckoutShippingMethod");   //--------------EDITED WORKFLOW------------------
                //return RedirectToRoute("CheckoutConfirm");
            }

            //If we got this far, something failed, redisplay form
            //model
            var model = PreparePaymentInfoModel(paymentMethod);
            return View(model);
        }
        

        public ActionResult Confirm()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();


            //model
            var model = PrepareConfirmOrderModel(cart);

            
            string paymentMethod =  _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
            
            
            if (paymentMethod == "Payments.Manual")
            {
                model.IsSveaPayment = true;
            }   

            return View(model);
        }
        [HttpPost, ActionName("Confirm")]
        [ValidateInput(false)]
        public ActionResult ConfirmOrder()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();


            //model
            var model = new CheckoutConfirmModel();
            try
            {
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required ---EDITED always create order---
                    if (IsPaymentWorkflowRequired(cart))
                    { 
                        //return RedirectToRoute("CheckoutPaymentInfo");
                        processPaymentRequest = new ProcessPaymentRequest();
                    }
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }
                
                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest()
                    {
                        Order = placeOrderResult.PlacedOrder
                    };
                    _paymentService.PostProcessPayment(postProcessPaymentRequest);

                    string paymentMethod =  _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);

                    if (placeOrderResult.PlacedOrder.PaymentMethodSystemName == "Payments.Manual")
                    {
                        PaymentForm theForm = TriggerSveaPaymentRedirect(placeOrderResult.PlacedOrder);
                        model.SveaWebPayForm = theForm;
                        
                        logSveaPaymentRedirectCheckout(placeOrderResult.PlacedOrder.Id);
                        model.SveaWebPayFormIsSet = true;
                        return View(model);
                        
                        //return theForm;
                    }

                    if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    {
                        //redirection or POST has been done in PostProcessPayment
                        return Content("Redirected");
                    }
                    else
                    {
                        //return RedirectToRoute("OrderDetails", new { orderId = placeOrderResult.PlacedOrder.Id });
                        //return RedirectToRoute("HomePage");
                        return RedirectToRoute("CheckoutCompleted", new { orderId = placeOrderResult.PlacedOrder.Id });
                    }
                }
                else
                {
                    foreach (var error in placeOrderResult.Errors)
                        model.Warnings.Add(error);
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc);
                model.Warnings.Add(exc.Message);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        

        [ChildActionOnly]
        public ActionResult CheckoutProgress(CheckoutProgressStep step)
        {
            var model = new CheckoutProgressModel() {CheckoutProgressStep = step};
            return PartialView(model);
        }

        #endregion

        #region Methods (one page checkout)

        public ActionResult OnePageCheckout()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (!UseOnePageCheckout())
                return RedirectToRoute("Checkout");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            var model = new OnePageCheckoutModel()
            {
                ShippingRequired = cart.RequiresShipping()
            };
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult OpcBillingForm()
        {
            var billingAddressModel = PrepareBillingAddressModel();
            return PartialView("OpcBillingAddress", billingAddressModel);
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveBilling(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                int billingAddressId = 0;
                int.TryParse(form["billing_address_id"], out billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == billingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutBillingAddressModel();
                    TryUpdateModel(model.NewAddress, "BillingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = PrepareBillingAddressModel(model.NewAddress.CountryId);
                        billingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "billing",
                                html = this.RenderPartialViewToString("OpcBillingAddress", billingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId);
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                if (cart.RequiresShipping())
                {
                    //shipping is required
                    var shippingAddressModel = PrepareShippingAddressModel();
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "shipping",
                            html = this.RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                        },
                        goto_section = "shipping"
                    });
                }
                else
                {
                    //shipping is not required
                    _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);


                    //Check whether payment workflow is required
                    //we ignore reward points during cart total calculation
                    bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
                    if (isPaymentWorkflowRequired)
                    {
                        //payment is required
                        var paymentMethodModel = PreparePaymentMethodModel(cart);

                        if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                            paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                        {
                            //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                            //so customer doesn't have to choose a payment method

                            var selectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                                SystemCustomerAttributeNames.SelectedPaymentMethod,
                                selectedPaymentMethodSystemName, _storeContext.CurrentStore.Id);

                            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                            if (paymentMethodInst == null ||
                                !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                                !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                                throw new Exception("Selected payment method can't be parsed");


                            var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst);
                            return Json(new
                            {
                                update_section = new UpdateSectionJsonModel()
                                {
                                    name = "payment-info",
                                    html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                                },
                                goto_section = "payment_info"
                            });
                        }
                        else
                        {
                            //customer have to choose a payment method
                            return Json(new
                            {
                                update_section = new UpdateSectionJsonModel()
                                {
                                    name = "payment-method",
                                    html = this.RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                                },
                                goto_section = "payment_method"
                            });
                        }
                    }
                    else
                    {
                        //payment is not required
                        _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                            SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

                        var confirmOrderModel = PrepareConfirmOrderModel(cart);
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "confirm-order",
                                html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                            },
                            goto_section = "confirm_order"
                        });
                    }
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveShipping(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                int shippingAddressId = 0;
                int.TryParse(form["shipping_address_id"], out shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == shippingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutShippingAddressModel();
                    TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var shippingAddressModel = PrepareShippingAddressModel(model.NewAddress.CountryId);
                        shippingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "shipping",
                                html = this.RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId);
                    if (address == null)
                    {
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //little hack here (TODO: find a better solution)
                        //EF does not load navigation properties for newly created entities (such as this "Address").
                        //we have to load them manually 
                        //otherwise, "Country" property of "Address" entity will be null in shipping rate computation methods
                        if (address.CountryId.HasValue)
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        if (address.StateProvinceId.HasValue)
                            address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);

                        //other null validations
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                var shippingMethodModel = PrepareShippingMethodModel(cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "shipping-method",
                        html = this.RenderPartialViewToString("OpcShippingMethods", shippingMethodModel)
                    },
                    goto_section = "shipping_method"
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveShippingMethod(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");
                
                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                //parse selected method 
                string shippingoption = form["shippingoption"];
                if (String.IsNullOrEmpty(shippingoption))
                    throw new Exception("Selected shipping method can't be parsed");
                var splittedOption = shippingoption.Split(new string[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
                if (splittedOption.Length != 2)
                    throw new Exception("Selected shipping method can't be parsed");
                string selectedName = splittedOption[0];
                string shippingRateComputationMethodSystemName = splittedOption[1];
                
                //find it
                //performance optimization. try cache first
                var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
                if (shippingOptions == null || shippingOptions.Count == 0)
                {
                    //not found? let's load them using shipping service
                    shippingOptions = _shippingService
                        .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id)
                        .ShippingOptions
                        .ToList();
                }
                else
                {
                    //loaded cached results. let's filter result by a chosen shipping rate computation method
                    shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                        .ToList();
                }
                
                var shippingOption = shippingOptions
                    .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
                if (shippingOption == null)
                    throw new Exception("Selected shipping method can't be loaded");

                //save
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);


                //Check whether payment workflow is required
                //we ignore reward points during cart total calculation
                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
                if (isPaymentWorkflowRequired)
                {
                    //payment is required
                    var paymentMethodModel = PreparePaymentMethodModel(cart);

                    if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                        paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                    {
                        //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                        //so customer doesn't have to choose a payment method
                        var selectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                        _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                            SystemCustomerAttributeNames.SelectedPaymentMethod, selectedPaymentMethodSystemName, _storeContext.CurrentStore.Id);

                        var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                        if (paymentMethodInst == null ||
                            !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                            !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                            throw new Exception("Selected payment method can't be parsed");


                        var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst);
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "payment-info",
                                html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                            },
                            goto_section = "payment_info"
                        });
                    }
                    else
                    {
                        //customer have to choose a payment method
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "payment-method",
                                html = this.RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                            },
                            goto_section = "payment_method"
                        });
                    }
                }
                else
                {
                    //payment is not required
                    _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentMethod(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                string paymentmethod = form["paymentmethod"];
                //payment method 
                if (String.IsNullOrEmpty(paymentmethod))
                    throw new Exception("Selected payment method can't be parsed");


                var model = new CheckoutPaymentMethodModel();
                TryUpdateModel(model);

                //reward points
                if (_rewardPointsSettings.Enabled)
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, model.UseRewardPoints,
                        _storeContext.CurrentStore.Id);
                }

                //Check whether payment workflow is required
                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
                if (!isPaymentWorkflowRequired)
                {
                    //payment is not required
                    _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
                if (paymentMethodInst == null ||
                    !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                    !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                    throw new Exception("Selected payment method can't be parsed");

                //save
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);
                

                var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    },
                    goto_section = "payment_info"
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentInfo(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
                if (paymentMethod == null)
                    throw new Exception("Payment method is not selected");

                var paymentControllerType = paymentMethod.GetControllerType();
                var paymentController =
                    DependencyResolver.Current.GetService(paymentControllerType) as BaseNopPaymentController;
                var warnings = paymentController.ValidatePaymentForm(form);
                foreach (var warning in warnings)
                    ModelState.AddModelError("", warning);
                if (ModelState.IsValid)
                {
                    //get payment info
                    var paymentInfo = paymentController.GetPaymentInfo(form);
                    //session save
                    _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = PreparePaymentInfoModel(paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcConfirmOrder()
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                    {
                        throw new Exception("Payment information is not entered");
                    }
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }

                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest()
                    {
                        Order = placeOrderResult.PlacedOrder
                    };


                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);
                    if (paymentMethod != null)
                    {
                        if (paymentMethod.PaymentMethodType == Nop.Services.Payments.PaymentMethodType.Redirection)
                        {
                            //Redirection will not work because it's AJAX request.
                            //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

                            //redirect
                            return Json(new { redirect = string.Format("{0}checkout/OpcCompleteRedirectionPayment", _webHelper.GetStoreLocation()) });
                        }
                        else
                        {
                            _paymentService.PostProcessPayment(postProcessPaymentRequest);
                            //success
                            return Json(new { success = 1 });
                        }
                    }
                    else
                    {
                        //payment method could be null if order total is 0

                        //success
                        return Json(new { success = 1 });
                    }
                }
                else
                {
                    //error
                    var confirmOrderModel = new CheckoutConfirmModel();
                    foreach (var error in placeOrderResult.Errors)
                        confirmOrderModel.Warnings.Add(error); 
                    
                    return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "confirm-order",
                                html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                            },
                            goto_section = "confirm_order"
                        });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public ActionResult OpcCompleteRedirectionPayment()
        {
            try
            {
                //validation
                if (!UseOnePageCheckout())
                    return RedirectToRoute("HomePage");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    return new HttpUnauthorizedResult();

                //get the order
                var order = _orderService.SearchOrders(_storeContext.CurrentStore.Id, 0, _workContext.CurrentCustomer.Id,
                    null, null, null, null, null, null, null, 0, 1, null, null, null)
                    .FirstOrDefault();
                if (order == null)
                    return RedirectToRoute("HomePage");

                
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                if (paymentMethod == null)
                    return RedirectToRoute("HomePage");
                if (paymentMethod.PaymentMethodType != Nop.Services.Payments.PaymentMethodType.Redirection)
                    return RedirectToRoute("HomePage");

                //ensure that order has been just placed
                if ((DateTime.UtcNow - order.CreatedOnUtc).TotalMinutes > 3)
                    return RedirectToRoute("HomePage");


                //Redirection will not work on one page checkout page because it's AJAX request.
                //That's why we process it here
                var postProcessPaymentRequest = new PostProcessPaymentRequest()
                {
                    Order = order
                };

                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                {
                    //redirection or POST has been done in PostProcessPayment
                    return Content("Redirected");
                }
                else
                {
                    //if no redirection has been done (to a third-party payment page)
                    //theoretically it's not possible
                    return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Content(exc.Message);
            }
        }

        #endregion
    }
}
