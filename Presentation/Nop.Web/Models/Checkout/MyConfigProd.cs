﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webpay.Integration.CSharp.Config;
using Webpay.Integration.CSharp.Util.Constant;

namespace Nop.Web.Models.Checkout
{
    public class MyConfigProd : IConfigurationProvider
    {
        /// <summary>
        /// Get the return value from your database or likewise
        /// </summary>
        /// <param name="type"> eg. HOSTED, INVOICE or PAYMENTPLAN</param>
        /// <param name="country">country code</param>
        /// <returns>user name string</returns>
        public string GetUsername(PaymentType type, CountryCode country)
        {
            string myUserName = "1952DaBa";
            //DELBETALA? string myUserName = "STARKEY.SE";
            return myUserName;
        }

        /// <summary>
        /// Get the return value from your database or likewise
        /// </summary>
        /// <param name="type"> eg. HOSTED, INVOICE or PAYMENTPLAN</param>
        /// <param name="country">country code</param>
        /// <returns>password string</returns>
        public string GetPassword(PaymentType type, CountryCode country)
        {
            string myPassword = "5odJoe45jOkHod3";
            //DELBETALA? string myPassword = "mtuc5BtUq";
            return myPassword;
        }

        /// <summary>
        /// Get the return value from your database or likewise
        /// </summary>
        /// <param name="type"> eg. HOSTED, INVOICE or PAYMENTPLAN</param>
        /// <param name="country">country code</param>
        /// <returns>client number int</returns>
        public int GetClientNumber(PaymentType type, CountryCode country)
        {           
            int myClientNumber = 1952;
            return myClientNumber;
        }

        /// <summary>
        /// Get the return value from your database or likewise
        /// </summary>
        /// <param name="type"> eg. HOSTED, INVOICE or PAYMENTPLAN</param>
        /// <param name="country">country code</param>
        /// <returns>merchant id string</returns>
        public string GetMerchantId(PaymentType type, CountryCode country)
        {
            string myMerchantId = "1952";
            return myMerchantId;
        }

        /// <summary>
        /// Get the return value from your database or likewise
        /// </summary>
        /// <param name="type"> eg. HOSTED, INVOICE or PAYMENTPLAN</param>
        /// <param name="country">country code</param>
        /// <returns>secret word string</returns>
        public string GetSecretWord(PaymentType type, CountryCode country)
        {
            //Test
            //string mySecretWord = "5ec57a57af0ffc5075e57a8d2125d696a8719fd19d959e57992b08a40552325a7eddc97d168cbeae0c601e46a4cd22ec00c4bd0692f95330470784fb6d6ee3ac";
            
            
            //Prod
            string mySecretWord = "f5147005fadc8322e4b605c5a12b4b96103b0681edb517569aa436eb9d4587ab2c0a5cfbf5b731fe9047cc8f16c481b18affa5d964d9c1f18124c7d979908c7a";
            return mySecretWord;
        }

        /// <summary>
        /// Constants for the end point url found in the class SveaConfig
        /// </summary>
        /// <param name="type"> eg. HOSTED, INVOICE or PAYMENTPLAN</param>
        /// <returns>end point url</returns>
        public string GetEndPoint(PaymentType type)
        {
            //return type == PaymentType.HOSTED ? SveaConfig.GetTestPayPageUrl() : SveaConfig.GetTestWebserviceUrl();
            return type == PaymentType.HOSTED ? SveaConfig.GetProdPayPageUrl() : SveaConfig.GetProdWebserviceUrl();
        }
    }
}