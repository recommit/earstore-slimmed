﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutPaymentMethodModel : BaseNopModel
    {
        public CheckoutPaymentMethodModel()
        {
            PaymentMethods = new List<PaymentMethodModel>();
        }

        public IList<PaymentMethodModel> PaymentMethods { get; set; }

        public bool DisplayRewardPoints { get; set; }
        public int RewardPointsBalance { get; set; }
        public string RewardPointsAmount { get; set; }
        public bool UseRewardPoints { get; set; }

        public bool forceCardPayment { get; set; } //---------EDITED-------------
        public bool missingSocialNumber { get; set; }
        public bool notSwedenDelivery { get; set; }

        [RegularExpression(@"^((\d{6})[-]\d{4})$", ErrorMessage = "Detta är inte ett giltigt personnummer eller organisationsnummer. Vänligen använd formatet XXXXXX-XXXX")]
        public string AddSocialNumber { get; set; }

        #region Nested classes

        public partial class PaymentMethodModel : BaseNopModel
        {
            public string PaymentMethodSystemName { get; set; }
            public string Name { get; set; }
            public string Fee { get; set; }
            public bool Selected { get; set; }
        }
        #endregion
    }
}