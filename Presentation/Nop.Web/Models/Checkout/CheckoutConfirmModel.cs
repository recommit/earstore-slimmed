﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using System;
using Webpay.Integration.CSharp.Hosted.Helper;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutConfirmModel : BaseNopModel
    {
        public CheckoutConfirmModel()
        {
            Warnings = new List<string>();
        }

        public Boolean IsSveaPayment { get; set; }      

        public string MinOrderTotalWarning { get; set; }

        public PaymentForm SveaWebPayForm { get; set; }      

        public Boolean SveaWebPayFormIsSet { get; set; }              

        public IList<string> Warnings { get; set; }
    }
}